package yaml

import (
	"fmt"
	"os"
	"reflect"
	"testing"

	"eotl.supply/sextant"
	h "eotl.supply/sextant/helper"
)

func TestYamlService(t *testing.T) {
	joe := sextant.ContactAddress{
		Contact: &sextant.Contact{Name: "Joe", Amount: 1},
		Address: &sextant.Address{Label: "Sonnenallee 145 12053 Berlin"},
	}

	t.Run("LoadContactAddresses", func(t *testing.T) {
		fileAddressMissing := h.MkTempFile(fmt.Sprintf("---\n1:\n Name: %s", joe.Name))
		defer os.Remove(fileAddressMissing)
		fileOK := h.MkTempFile(fmt.Sprintf("---\n1:\n name: %s\n address: %s", joe.Name, joe.Label))
		defer os.Remove(fileOK)
		fileMissing := h.MkTempFile("")
		os.Remove(fileMissing)
		fileInvalid := h.MkTempFile(fmt.Sprintf("xxx\n1:\n name: %s\n address: %s", joe.Name, joe.Label))
		defer os.Remove(fileInvalid)

		decoder := NewContactAddressDecoder(false)

		tables := []struct {
			name     string
			input    string
			want     []sextant.ContactAddress
			want_err string
		}{
			{"FileMissing", "", []sextant.ContactAddress{}, ""},
			{"FileMissing2", fileMissing, nil, fmt.Sprintf("unable to read yaml file %s: open %s: no such file or directory\n", fileMissing, fileMissing)},
			{"AddressMissing", fileAddressMissing, nil, fmt.Sprintf("\nError: required key 'address' is missing in file %s", fileAddressMissing)},
			{"FileOk", fileOK, []sextant.ContactAddress{joe}, ""},
			{"FileInvalid", fileInvalid, nil, fmt.Sprintf("unable to unmarshal yaml data %s: yaml: line 2: mapping values are not allowed in this context\n", fileInvalid)},
		}

		for _, table := range tables {
			t.Run(table.name, func(t *testing.T) {
				got, got_err := decoder.DecodeContactAddresses(table.input)
				if got_err != nil {
					if got_err.Error() != table.want_err {
						t.Errorf("LoadContactAddresses(%s) -> want %+v got %+v", table.input, table.want_err, got_err)
					}
				} else {
					if !reflect.DeepEqual(got, table.want) {
						t.Errorf("LoadContactAddresses(%s) -> want %+v got %+v", table.input, table.want, got)
					}
				}
			})
		}
	})

}
