Sextant
=======

[![status-badge](https://ci.codeberg.org/api/badges/13137/status.svg)](https://ci.codeberg.org/repos/13137)

*Sextant* is an end-user application which is a collection of geo locational
services, tour building, and group management functions. It is made for
independent couriers, collectives, and small business who do their own
logistics fulfilment. *Sextant* consists of the command line tool `sextant` as
well as a HTTP server daemon `sextantd` and an intuitive Web UI.

Things *Sextant* can do:

- Fetches geo locations for a single address or list in a CSV
- Import CSV of deliveries and optional pickup and delivery locations and metadata (phone, email)
- Intelligently solves a "tour" finding optimal routes using the [Vroom engine](https://github.com/VROOM-Project/vroom)
- Converts lists of data to geojson format
- Create groups of multiple contacts / locations
- Transcoding of geo coordinates to [Where39](https://github.com/arcbtc/where39) words

## Documentation

- [Architecture](docs/architecture.md)
- [CLI](docs/cli.md)
- [REST API](docs/rest-api.md)


## Setup

**Web UI**

The first step is to build the Sextant Web UI, run the following:

```
$ ./install.sh theme
$ ./install.sh ui
```

**Building Binaries**

The following assumes a functioning [Go Lang](https://go.dev) environment (>=
1.17). Build the `sextant` and `sextantd` binaries and install in the default
directory `~/.local/bin/` or a custom path:

```
$ ./install.sh binaries
$ ./install.sh binaries /custom/path/bin/
```

Assuming that built correctly, you can now run the following:

```
$ sextant
$ sextantd
```

The config file `sextant.conf` is written in TOML format and copied to `$HOME`
of the user running `sextantd` server. It **must** contain these values for
basic functionality.

```
port             = 8081
be_verbose       = false
nominatim_url    = "https://nominatim.openstreetmap.org/search.php"
vroom_url        = "https://routing.eotl.supply/osmr"
dsn              = "~/.sextant.db"
ssrauth_filepath = ""
```

The default dB is located in `~/.sextant.db`. It can be changed via the
`dsn` key in the `sextant.conf` file.

For better error reporting log to stdout change `be_verbose = true` or run with
the `-v` flag

```
$ sextantd -v
```

**Install as systemd service**

Create a service unit for systemd at `/etc/systemd/system/sextantd.service`
with the following

```
[Unit]
Description=Sextant Daemon

[Service]
ExecStart=/home/user/.local/bin/sextantd
Restart=always

[Install]
WantedBy=multi-user.target
```

Don't forget to update the path to the `sextantd` binary.

**Defaults**

You can install default `groups` and `zones` data (**warning**: *Berlin only*) with:

```
$ ./install.sh defaults
```

**Test Data**

If you plan to help develop Sextant, you can install test data (customers,
riders, suppliers) with the following command

```
$ ./install.sh testing
```

## Docker

> [`codeberg.org/eotl/sextant`](https://codeberg.org/eotl/-/packages/container/sextant/latest)

### Manually

Where `latest` could be `$SomethingElse` (e.g. a specific version tag).

```
docker login codeberg.org
docker build -t codeberg.org/eotl/sextant:latest .
docker push codeberg.org/eotl/sextant:latest
```

## Notes

**What is the difference between Jobs and Shipments?**

From the [VROOM API](https://github.com/VROOM-Project/vroom/blob/master/docs/API.md)

> It is assumed that all delivery-related amounts for jobs
> are loaded at vehicle start, while all pickup-related
> amounts for jobs are brought back at vehicle end.

With Jobs, the solver assumes that a vehicle (rider) has all items to deliver
from its starting position. Capacity / delivery amount constraints seem to be
ignored (**TODO**: to be verified).

A Shipment consists of a pickup and a delivery location. A vehicle starts from
its location with load zero. The capacity restriction of a rider (maximum load)
is respected.
