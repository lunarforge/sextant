package sextant

type VroomService interface {
	CreateRequest(tour Tour) *VroomRequest

	Query(r *VroomRequest) (*VroomResponse, error)

	QueryWithFile(filepath string) (*VroomResponse, error)
}
