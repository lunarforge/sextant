package sextant

import (
	"context"
	"time"
)

// A ContactAddress references a Contact and an (generic) Address
// and stores the details of an address (e.g. third floor) uniqe to the contact
type ContactAddress struct {
	ID        int `json:"-"`
	*Address  `yaml:",inline"`
	AddressID int `json:"-"`
	*Contact  `yaml:",inline"`
	ContactID int       `json:"-"`
	Details   string    `json:"address_details" csv:"AddressDetails" yaml:"address_details"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
}

func (a *ContactAddress) Validate() error {
	return nil
}

type ContactAddressFilter struct {
	ID        *int    `json:"-"`
	AddressID *int    `json:"-"`
	ContactID *int    `json:"-"`
	Details   *string `json:"-"`
	Offset    int     `json:"offset"`
	Limit     int     `json:"limit"`
	// filters for other/joined tables
	GroupID          *int    `json:"-"`
	ContactName      *string `json:"-"`
	AddressLabel     *string `json:"-"`
	ZoneID           *int    `json:"-"`
	TourID           *int    `json:"-"`
	TourItemtypeName *string `json:"-"`
	TourIDs          []int   `json:"-"`
}

type ContactAddressService interface {
	FindContactAddresses(ctx context.Context, filter ContactAddressFilter) ([]ContactAddress, error)

	FindContactAddressByID(ctx context.Context, id int) (*ContactAddress, error)

	FindOrCreateContactAddress(ctx context.Context, ca *ContactAddress) error
}

type ContactAddressDecoder interface {
	DecodeContactAddresses(filepath string) ([]ContactAddress, error)
}
