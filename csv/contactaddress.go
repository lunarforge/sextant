package csv

import (
	"eotl.supply/sextant"
	"eotl.supply/sextant/inmem"
	"fmt"
	"os"

	"github.com/gocarina/gocsv"
)

var _ sextant.ContactAddressDecoder = (*ContactAddressDecoder)(nil)

type ContactAddressDecoder struct {
	parser          *inmem.AddressParser
	dateTimeService *inmem.DateTimeService
	logger          *sextant.Logger
}

func NewContactAddressDecoder(doLog bool) *ContactAddressDecoder {
	parser := inmem.NewAddressParser()
	dateTimeService := inmem.NewDateTimeService()
	logger := &sextant.Logger{doLog, os.Stderr}
	return &ContactAddressDecoder{parser, dateTimeService, logger}
}

func (dec *ContactAddressDecoder) DecodeContactAddresses(filepath string) (contacts []sextant.ContactAddress, err error) {
	dec.logger.Printf("Parsing CSV file '%s' ... ", filepath)
	csvFile, err := os.Open(filepath)
	if err != nil {
		err = fmt.Errorf("unable to open csv file %s: %v\n", filepath, err)
		return contacts, err
	}
	defer csvFile.Close()
	contacts = []sextant.ContactAddress{}
	if err := gocsv.UnmarshalFile(csvFile, &contacts); err != nil {
		err = fmt.Errorf("error unmarshaling csv file %s: %v\n", filepath, err)
		return contacts, err
	}
	// IMPROVE: iterate over all string fields and strings.TrimSpace
	contactsUpdated := []sextant.ContactAddress{}
	for _, contact := range contacts {
		if contact.Address == nil {
			err = fmt.Errorf("\nError: required column 'Address' is missing in file %s", filepath)
			return []sextant.ContactAddress{}, err
		}
		if contact.Label != "" {
			contact.Label = dec.parser.ParseAddress(contact.Label)
		}
		if contact.Amount < 1 {
			contact.Amount = 1
		}
		contactsUpdated = append(contactsUpdated, contact)
	}
	dec.logger.Printf("%s\n", "done")
	return contactsUpdated, nil
}
