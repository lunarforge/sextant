package sextant

import (
	"io"
	"net/http"
)

// A wrapper for net/http.Client
type HttpClient interface {
	Get(path string) (*http.Response, error)

	Post(url, contentType string, body io.Reader) (resp *http.Response, err error)
}
