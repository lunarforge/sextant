package mock

import (
	"context"
	"eotl.supply/sextant"
)

type GroupMembershipService struct {
	FindGroupMembershipByIDsFn    func(ctx context.Context, groupID int, contactAddressID int) (*sextant.GroupMembership, error)
	FindOrCreateGroupMembershipFn func(ctx context.Context, m *sextant.GroupMembership) error
}

func (s *GroupMembershipService) FindGroupMembershipByIDs(ctx context.Context, groupID int, contactAddressID int) (*sextant.GroupMembership, error) {
	return s.FindGroupMembershipByIDsFn(ctx, groupID, contactAddressID)
}

func (s *GroupMembershipService) FindOrCreateGroupMembership(ctx context.Context, m *sextant.GroupMembership) error {
	return s.FindOrCreateGroupMembershipFn(ctx, m)
}
