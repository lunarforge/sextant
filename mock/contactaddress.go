package mock

import (
	"context"
	"eotl.supply/sextant"
)

type ContactAddressService struct {
	FindContactAddressesFn       func(ctx context.Context, filter sextant.ContactAddressFilter) ([]sextant.ContactAddress, error)
	FindContactAddressByIDFn     func(ctx context.Context, id int) (*sextant.ContactAddress, error)
	FindOrCreateContactAddressFn func(ctx context.Context, ca *sextant.ContactAddress) error
}

func (s *ContactAddressService) FindContactAddresses(ctx context.Context, filter sextant.ContactAddressFilter) ([]sextant.ContactAddress, error) {
	return s.FindContactAddressesFn(ctx, filter)
}

func (s *ContactAddressService) FindContactAddressByID(ctx context.Context, id int) (*sextant.ContactAddress, error) {
	return s.FindContactAddressByIDFn(ctx, id)
}

func (s *ContactAddressService) FindOrCreateContactAddress(ctx context.Context, ca *sextant.ContactAddress) error {
	return s.FindOrCreateContactAddressFn(ctx, ca)
}

type ContactAddressDecoder struct {
	DecodeContactAddressesFn func(filepath string) ([]sextant.ContactAddress, error)
}

func (dec *ContactAddressDecoder) DecodeContactAddresses(filepath string) ([]sextant.ContactAddress, error) {
	return dec.DecodeContactAddressesFn(filepath)
}
