package mock

type FileService struct {
	ToJSONFn                   func(filename string, data interface{}, path string) (bool, error)
	GetFileNameWithoutSuffixFn func(path string) string
	SetFirstLineFn             func(filepath string, header string, action string) (string, error)
}

func (s *FileService) ToJSON(filename string, data interface{}, path string) (bool, error) {
	return s.ToJSONFn(filename, data, path)
}

func (s *FileService) GetFileNameWithoutSuffix(path string) string {
	return s.GetFileNameWithoutSuffixFn(path)
}

func (s *FileService) SetFirstLine(filepath string, header string, action string) (string, error) {
	return s.SetFirstLineFn(filepath, header, action)
}
