package mock

import (
	"eotl.supply/sextant"
)

type VroomService struct {
	CreateRequestFn func(tour sextant.Tour) *sextant.VroomRequest
	QueryFn         func(request *sextant.VroomRequest) (*sextant.VroomResponse, error)
	QueryWithFileFn func(filepath string) (*sextant.VroomResponse, error)
}

func (s *VroomService) CreateRequest(tour sextant.Tour) *sextant.VroomRequest {
	return s.CreateRequestFn(tour)
}

func (s *VroomService) Query(request *sextant.VroomRequest) (*sextant.VroomResponse, error) {
	return s.QueryFn(request)
}

func (s *VroomService) QueryWithFile(filepath string) (*sextant.VroomResponse, error) {
	return s.QueryWithFileFn(filepath)
}
