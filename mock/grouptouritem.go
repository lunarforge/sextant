package mock

import (
	"context"
	"eotl.supply/sextant"
)

type GroupTourItemService struct {
	FindGroupTourItemByIDsFn    func(ctx context.Context, groupID int, tourID int) (*sextant.GroupTourItem, error)
	FindOrCreateGroupTourItemFn func(ctx context.Context, m *sextant.GroupTourItem) error
}

func (s *GroupTourItemService) FindGroupTourItemByIDs(ctx context.Context, groupID int, tourID int) (*sextant.GroupTourItem, error) {
	return s.FindGroupTourItemByIDsFn(ctx, groupID, tourID)
}

func (s *GroupTourItemService) FindOrCreateGroupTourItem(ctx context.Context, m *sextant.GroupTourItem) error {
	return s.FindOrCreateGroupTourItemFn(ctx, m)
}
