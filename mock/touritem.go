package mock

import (
	"context"
	"eotl.supply/sextant"
)

type TourItemService struct {
	CreateTourItemFn        func(ctx context.Context, item *sextant.TourItem) error
	DeleteTourItemFn        func(ctx context.Context, id int) error
	FindTourItemByIDFn      func(ctx context.Context, id int) (*sextant.TourItem, error)
	FindTourItemsByTourIDFn func(ctx context.Context, tourID int) ([]sextant.TourItem, error)
	UpdateTourItemFn        func(ctx context.Context, id int, upd sextant.TourItemUpdate) (*sextant.TourItem, error)
}

func (s *TourItemService) CreateTourItem(ctx context.Context, tour *sextant.TourItem) error {
	return s.CreateTourItemFn(ctx, tour)
}

func (s *TourItemService) DeleteTourItem(ctx context.Context, id int) error {
	return s.DeleteTourItemFn(ctx, id)
}

func (s *TourItemService) FindTourItemByID(ctx context.Context, id int) (*sextant.TourItem, error) {
	return s.FindTourItemByIDFn(ctx, id)
}

func (s *TourItemService) FindTourItemsByTourID(ctx context.Context, tourID int) ([]sextant.TourItem, error) {
	return s.FindTourItemsByTourIDFn(ctx, tourID)
}

func (s *TourItemService) UpdateTourItem(ctx context.Context, id int, upd sextant.TourItemUpdate) (*sextant.TourItem, error) {
	return s.UpdateTourItemFn(ctx, id, upd)
}
