package mock

import (
	"context"
	"eotl.supply/sextant"
)

type ContactService struct {
	FindContactByIDFn     func(ctx context.Context, id int) (*sextant.Contact, error)
	FindOrCreateContactFn func(ctx context.Context, contact *sextant.Contact) error
}

func (s *ContactService) FindContactByID(ctx context.Context, id int) (*sextant.Contact, error) {
	return s.FindContactByIDFn(ctx, id)
}

func (s *ContactService) FindOrCreateContact(ctx context.Context, contact *sextant.Contact) error {
	return s.FindOrCreateContactFn(ctx, contact)
}
