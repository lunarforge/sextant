package sextant

import (
	"gopkg.in/yaml.v3"
	"time"
)

type DateTime struct {
	time.Time
}

func (date *DateTime) UnmarshalCSV(csv string) (err error) {
	if len(csv) > 0 {
		date.Time, err = time.Parse(TimeLayout, csv)
	}
	return err
}

func (date *DateTime) UnmarshalYAML(n *yaml.Node) (err error) {
	if len(n.Value) > 0 {
		date.Time, err = time.Parse(TimeLayout, n.Value)
	}
	return err
}

type DateTimeService interface {
	Create(s string) (*DateTime, error)
}
