package sextant

import (
	"context"
	"encoding/json"
	"time"
)

type Tour struct {
	ID         int        `yaml:"-" json:"id"`
	Name       string     `yaml:"-" json:"name"`
	Slug       string     `yaml:"-" json:"slug"`
	OnDate     time.Time  `yaml:"-" json:"date"`
	Groups     []Group    `yaml:"-" json:"-"`
	Deliveries []TourItem `json:"deliveries"`
	Pickups    []TourItem `json:"pickups"`
	Riders     []TourItem `json:"riders"`
	CreatedAt  time.Time  `yaml:"-" json:"-"`
	UpdatedAt  time.Time  `yaml:"-" json:"-"`
}

type TourFilter struct {
	ID        *int    `json:"id"`
	Slug      *string `json:"slug"`
	Name      *string `json:"name"`
	Offset    int     `json:"offset"`
	Limit     int     `json:"limit"`
	GroupID   *int    `json:"group_id"`
	ContactID *int    `json:"contact_id"`
}

func (t *Tour) Validate() error {
	return nil
}

func (t *Tour) ContactAddresses() (cas []ContactAddress) {
	for _, item := range t.Deliveries {
		cas = append(cas, *item.ContactAddress)
	}
	for _, item := range t.Pickups {
		cas = append(cas, *item.ContactAddress)
	}
	for _, item := range t.Riders {
		cas = append(cas, *item.ContactAddress)
	}
	return cas
}

func (t *Tour) StopsCount() int {
	return len(t.Deliveries) + len(t.Pickups)
}

// Adds additional fields to json output
// that are not part of the struct
type FakeTour Tour

func (t Tour) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		FakeTour
		StopsCount int `json:"stops_count"`
	}{
		FakeTour:   FakeTour(t),
		StopsCount: t.StopsCount(),
	})
}

type TourService interface {
	CreateTour(ctx context.Context, tour *Tour) error

	DeleteTour(ctx context.Context, id int) error

	FindTours(ctx context.Context, filter TourFilter) ([]Tour, error)

	FindTourByIDorSlug(ctx context.Context, idOrSlug string) (*Tour, error)
}
