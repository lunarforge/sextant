package sextant

import (
	"fmt"
	"os"
)

type Logger struct {
	DoLog bool
	Dev   *os.File
}

func (l *Logger) Printf(fmts string, args ...interface{}) {
	if l.DoLog {
		fmt.Fprintf(l.Dev, fmts, args...)
	}
}

func (l *Logger) Println(s string) {
	if l.DoLog {
		fmt.Fprintln(l.Dev, s)
	}
}
