package sextant

import (
	"context"
	"time"
)

type ZoneMembership struct {
	ID         int
	ZoneID     int
	LocationID int
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

type ZoneMembershipFilter struct {
	ID         *int
	ZoneID     *int
	LocationID *int
	Offset     int
	Limit      int
}

func (zm *ZoneMembership) Validate() error {
	return nil
}

type ZoneMembershipService interface {
	FindZoneMembershipByIDs(ctx context.Context, zoneID int, locationID int) (*ZoneMembership, error)

	FindOrCreateZoneMembership(ctx context.Context, zm *ZoneMembership) error
}
