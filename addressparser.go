package sextant

type AddressParser interface {
	ParseAddress(address string) string
}
