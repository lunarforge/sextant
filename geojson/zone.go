package geojson

import (
	"eotl.supply/sextant"
	"fmt"
	"github.com/paulmach/orb/geojson"
	"io/ioutil"
	"os"
)

var _ sextant.ZoneDecoder = (*ZoneDecoder)(nil)

type ZoneDecoder struct {
	logger *sextant.Logger
}

func NewZoneDecoder(doLog bool) *ZoneDecoder {
	logger := &sextant.Logger{doLog, os.Stderr}
	return &ZoneDecoder{logger}
}

func (dec *ZoneDecoder) DecodeZones(filepath string, nameKey string) ([]sextant.Zone, error) {
	if filepath == "" {
		return []sextant.Zone{}, nil
	}
	dec.logger.Printf("Parsing GEOJSON file '%s' ... ", filepath)
	file, err := ioutil.ReadFile(filepath)
	if err != nil {
		err = fmt.Errorf("unable to read geojson file %s: %v\n", filepath, err)
		return nil, err
	}

	featureCollection, err := geojson.UnmarshalFeatureCollection([]byte(file))
	if err != nil {
		err = fmt.Errorf("unable to unmarshal geojson data %s: %v\n", filepath, err)
		return nil, err
	}

	var zones []sextant.Zone
	if len(nameKey) == 0 {
		nameKey = "name"
	}
	for _, feature := range featureCollection.Features {
		// Extract zone name
		v := feature.Properties[nameKey]
		if v == nil {
			err = fmt.Errorf("No value for namekey '%s' found", nameKey)
			return nil, err
		}
		name, ok := v.(string)
		if !ok {
			err = fmt.Errorf("Value for namekey '%s' is not a string: %v", nameKey, v)
			return nil, err
		}
		// Build zone
		zone := sextant.Zone{
			Name:     name,
			Geometry: feature.Geometry,
		}
		zones = append(zones, zone)
	}

	dec.logger.Printf("%s\n", "done")
	return zones, nil
}
