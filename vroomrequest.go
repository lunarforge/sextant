package sextant

type VroomRequest struct {
	Vehicles  []*VroomVehicle  `json:"vehicles"`
	Jobs      []*VroomJob      `json:"jobs"`
	Shipments []*VroomShipment `json:"shipments"`
	Options   *VroomOptions    `json:"options,omitempty"`
}

type VroomVehicle struct {
	ID          int         `json:"id"`
	Description string      `json:"description,omitempty"`
	Profile     string      `json:"profile,omitempty"`
	Capacity    []int       `json:"capacity,omitempty"`
	Start       *[2]float64 `json:"start,omitempty"`
	End         *[2]float64 `json:"end,omitempty"`
	TimeWindow  *TimeWindow `json:"time_window,omitempty"`
}

type VroomShipment struct {
	Pickup   VroomShipmentStep `json:"pickup"`
	Delivery VroomShipmentStep `json:"delivery"`
	Amount   []int             `json:"amount,omitempty"`
}

type VroomShipmentStep struct {
	ID          int            `json:"id"`
	Location    *[2]float64    `json:"location,omitempty"`
	Description string         `json:"description,omitempty"`
	TimeWindows *[]*TimeWindow `json:"time_windows,omitempty"`
}

type VroomJob struct {
	ID          int            `json:"id"`
	Location    *[2]float64    `json:"location"`
	Description string         `json:"description"`
	Delivery    []int          `json:"delivery,omitempty"`
	TimeWindows *[]*TimeWindow `json:"time_windows,omitempty"`
}

type VroomOptions struct {
	G bool `json:"g"`
}
