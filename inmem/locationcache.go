package inmem

import (
	"crypto/sha256"
	"encoding/json"
	"eotl.supply/sextant"
	"errors"
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	"strings"
	"sync"
)

const PS = string(os.PathSeparator)

// Ensure type implements interface
var _ sextant.LocationCache = (*LocationCache)(nil)

// Implements a very simple file cache for locations
// Cache entries (files) never expire unless removed from filesystem
type LocationCache struct {
	dir       string
	BaseDir   string
	Namespace []string
	lock      sync.Mutex
}

func NewLocationCache(baseDir string, namespace []string) *LocationCache {
	return &LocationCache{BaseDir: baseDir, Namespace: namespace}
}

func (c *LocationCache) Store(key string, value *sextant.Location) (err error) {
	c.lock.Lock()
	defer c.lock.Unlock()
	if len(c.dir) < 1 {
		c.setDir(c.BaseDir, c.Namespace...)
	}
	err = os.MkdirAll(c.dir, 0755)
	if err != nil {
		return err
	}

	path := c.dir + PS + key
	data, err := json.Marshal(&value)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(path, data, 0644)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}

func (c *LocationCache) Load(key string) (*sextant.Location, error) {
	c.lock.Lock()
	defer c.lock.Unlock()
	if len(c.dir) < 1 {
		c.setDir(c.BaseDir, c.Namespace...)
	}
	path := c.dir + PS + key
	_, err := os.Stat(path)
	if errors.Is(err, fs.ErrNotExist) {
		return nil, nil
	}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	location := sextant.Location{}
	err = json.Unmarshal(data, &location)
	if err != nil {
		return nil, err
	}
	return &location, nil
}

func (c *LocationCache) setDir(path string, ns ...string) (err error) {
	if path == "" {
		path, err = os.UserCacheDir()
		if err != nil {
			return err
		}
	}
	s := []string{path}
	s = append(s, ns...)
	c.dir = strings.Join(s[:], string(os.PathSeparator))
	return nil
}

func (c *LocationCache) GetKey(s string) string {
	sum := sha256.Sum256([]byte(s))
	out := fmt.Sprintf("%x", sum)
	return out
}
