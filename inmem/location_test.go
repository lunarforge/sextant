package inmem

import (
	"eotl.supply/sextant"
	"eotl.supply/sextant/mock"
	"io/ioutil"
	"os"
	"reflect"
	"regexp"
	"testing"
)

func TestLocationService(t *testing.T) {
	address_joe := "Alexanderplatz Berlin"
	address_sam := "Hermannplatz Berlin"
	json_location_found := `[{"lat":"52.5170365","lon":"13.3888599"}]`
	json_location_missing := `[]`
	json_invalid := `[{"invalid"}]`

	t.Run("GetLocationCached", func(t *testing.T) {

		// Create tmp cache folder
		tmpDir, _ := ioutil.TempDir("", "sextant-locationcache")
		defer os.RemoveAll(tmpDir)

		tcs := []struct {
			name             string
			input            string
			mockStatusCode   int
			mockResponseData string
			want_err         string
			want             *sextant.Location
		}{
			{"LocationFound", address_joe, 200, json_location_found, "", &sextant.Location{Lon: 13.3888599, Lat: 52.5170365, Words: "small pledge view useful region"}},
			{"LocationFoundCached", address_joe, 404, "", "", &sextant.Location{Lon: 13.3888599, Lat: 52.5170365, Words: "small pledge view useful region"}},
		}
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {

				// Init HttpClient that mocks its server
				client, server := mock.NewHttpClient(tc.mockStatusCode, tc.mockResponseData)
				defer server.Close()

				// Init Cache
				cache := LocationCache{BaseDir: tmpDir}

				// Init Service
				service := NewLocationService(client, "", &cache, false)

				got, got_err := service.getLocationCached(tc.input)
				if got_err != nil {
					if ok, _ := regexp.MatchString(tc.want_err, got_err.Error()); !ok {
						t.Errorf("FetchLocation() returned unexpected error: %v", got_err)
					}
				}
				if !reflect.DeepEqual(got, tc.want) {
					t.Errorf("FetchLocation() -> want %v got %v", tc.want, got)
				}
			})
		}
	})

	t.Run("GetLocation", func(t *testing.T) {
		tcs := []struct {
			name             string
			input            string
			mockStatusCode   int
			mockResponseData string
			want_err         string
			want             *sextant.Location
		}{
			{"LocationFound", address_joe, 200, json_location_found, "", &sextant.Location{Lon: 13.3888599, Lat: 52.5170365, Words: "small pledge view useful region"}},
			{"LocationNotFound", address_sam, 200, json_location_missing, `returned no locations$`, nil},
			{"JsonResponseInvalid", address_sam, 200, json_invalid, `parsing json response.*with invalid character`, nil},
			{"StatusCodeNot200", address_sam, 404, "", `with status code 404 Not Found`, nil},
			{"ServerDown", address_sam, 200, "", `http get:.*failed with.*connection refused`, nil},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {

				// Init HttpClient that mocks its server
				client, server := mock.NewHttpClient(tc.mockStatusCode, tc.mockResponseData)
				defer server.Close()

				// Create LocationService
				service := NewLocationService(client, "", nil, false)

				if tc.name == "ServerDown" {
					// Simulate remote server down
					server.Close()
				}

				// Get Location
				got, got_err := service.getLocation(tc.input)
				if got_err != nil {
					if ok, _ := regexp.MatchString(tc.want_err, got_err.Error()); !ok {
						t.Errorf("FetchLocation() returned unexpected error: %v", got_err)
					}
				}
				if !reflect.DeepEqual(got, tc.want) {
					t.Errorf("FetchLocation() -> want %v got %v", tc.want, got)
				}
			})
		}
	})

	t.Run("UpdateLocations", func(t *testing.T) {
		joe := sextant.ContactAddress{
			Contact: &sextant.Contact{Name: "Joe", Amount: 1},
			Address: &sextant.Address{Label: "Hermannplatz 1 12053 Berlin"},
		}
		sam := sextant.ContactAddress{
			Contact: &sextant.Contact{Name: "Joe", Amount: 1},
			Address: &sextant.Address{Label: "Sonnenallee 33 12053 Berlin"},
		}

		tcs := []struct {
			name             string
			input            []sextant.ContactAddress
			mockStatusCode   int
			mockResponseData string
			want             []sextant.ContactAddress
			want_location    *sextant.Location
		}{
			{"locationFound", []sextant.ContactAddress{joe}, 200, json_location_found, []sextant.ContactAddress{}, &sextant.Location{Lon: 13.3888599, Lat: 52.5170365, Words: "small pledge view useful region"}},
			{"locationMissing", []sextant.ContactAddress{sam}, 200, json_location_missing, []sextant.ContactAddress{sam}, nil},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {

				// Create tmp cache folder
				tmpDir, _ := ioutil.TempDir("", "sextant-locationcache")
				defer os.RemoveAll(tmpDir)

				// Init Cache
				cache := LocationCache{BaseDir: tmpDir}

				// Init HttpClient that mocks its server
				client, server := mock.NewHttpClient(tc.mockStatusCode, tc.mockResponseData)
				defer server.Close()

				// Create LocationService
				service := NewLocationService(client, "", &cache, false)

				got := service.UpdateLocations(tc.input)

				if !reflect.DeepEqual(got, tc.want) {
					t.Errorf("UpdateLocations() -> got %T want %v", got, tc.want)
				}
				got_location := tc.input[0].Location
				if !reflect.DeepEqual(got_location, tc.want_location) {
					t.Errorf("UpdateLocations() -> got %v want %v", got_location, tc.want_location)
				}
			})
		}
	})
}
