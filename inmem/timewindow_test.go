package inmem

import (
	"encoding/json"
	"eotl.supply/sextant"
	"reflect"
	"testing"
	"time"
)

func TestTimeWindow(t *testing.T) {
	t.Run("json.Marshal()", func(t *testing.T) {
		input_ts, err := time.Parse(sextant.TimeLayout, "12-Dec-21 15:00")
		if err != nil {
			t.Error(`Unable to parse time`)
		}
		tcs := []struct {
			input sextant.TimeWindow
			want  string
		}{
			{sextant.TimeWindow{}, "[-62135596800,-62135596800]"},
			{sextant.TimeWindow{input_ts, input_ts}, "[1639321200,1639321200]"},
		}
		for _, tc := range tcs {
			data, err := tc.input.MarshalJSON()
			if err != nil {
				t.Errorf("TimeWindow.MarshalJSON(): an unexpected error occured %v", err)
			}
			got := string(data)
			if string(got) != tc.want {
				t.Errorf("TimeWindow.MarshalJSON(): input: %+v, got: %+v want: %+v", tc.input, got, tc.want)
			}
		}
	})
	t.Run("json.Unmarshal()", func(t *testing.T) {
		service := NewTimeWindowService()
		tw, err := service.Create("12-Dec-21 15:00", "12-Dec-21 16:00")
		if err != nil {
			t.Errorf("An unexpected error occured: %+v", err)
		}
		tcs := []struct {
			input [2]int64
			want  *sextant.TimeWindow
		}{
			{[2]int64{tw.Start.Unix(), tw.End.Unix()}, tw},
		}

		for _, tc := range tcs {
			var got sextant.TimeWindow
			data, _ := json.Marshal(tc.input)
			err := got.UnmarshalJSON(data)
			if err != nil {
				t.Errorf("TimeWindow.UnmarshalJSON(): an unexpected error occured: %+v", err)
			}
			if !reflect.DeepEqual(got, *tc.want) {
				t.Errorf("TimeWindow.UnmarshalJSON():\ninput:\t%+v\ngot:\t%+v\nwant:\t%+v", tc.input, got, *tc.want)
			}
		}
	})
}

/*
func TestTimeWindowUnmarshalJSON(t *testing.T) {
	tw := TimeWindow{}
	tw.Init("12-Dec-21 15:00", "12-Dec-21 16:00")

}
*/

func TestTimeWindowServiceCreate(t *testing.T) {
	service := NewTimeWindowService()

	tcs := []struct {
		input [2]string
		want  map[string]string
	}{
		{[2]string{"04-Dec-21 10:00", "04-Dec-21 11:00"}, map[string]string{"Start": "2021-12-04 10:00:00 +0000 UTC", "End": "2021-12-04 11:00:00 +0000 UTC"}},
	}
	for _, tc := range tcs {
		got, err := service.Create(tc.input[0], tc.input[1])
		if err != nil {
			t.Errorf("TimeWindowService.Create() failed: %+v", err)
		}
		if got.Start.String() != tc.want["Start"] || got.End.String() != tc.want["End"] {
			t.Errorf("TimeWindowService.Create() -> got: %v want: %v", got, tc.want)
		}
	}
}
