package inmem

import (
	"errors"
	"io"
	"net/http"
)

const NominatimDefaultUrl = "http://localhost/nominatim"

type NominatimHttpClient struct {
	client *http.Client
	url    string
}

func NewNominatimHttpClient(url string) *NominatimHttpClient {
	if url == "" {
		url = NominatimDefaultUrl
	}
	return &NominatimHttpClient{http.DefaultClient, url}
}

func (c *NominatimHttpClient) Get(path string) (*http.Response, error) {
	url := c.url + path
	return c.client.Get(url)
}

func (c *NominatimHttpClient) Post(url, contentType string, body io.Reader) (*http.Response, error) {
	return nil, errors.New("Implemente me!")
}
