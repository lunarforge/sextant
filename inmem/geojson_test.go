package inmem

import (
	"fmt"
	"reflect"
	"testing"

	"eotl.supply/sextant"
)

func TestGeojson(t *testing.T) {
	service := NewGeojsonService()
	location := &sextant.Location{Lon: 13.44575, Lat: 52.47865, Words: "this is fake"}

	contactAddress := sextant.ContactAddress{
		Contact: &sextant.Contact{},
		Address: &sextant.Address{
			Label:    "Sonnenallee 148",
			Location: location,
		},
	}

	f_want_contact := &sextant.GeojsonFeature{
		Type: "Feature",
		Properties: map[string]string{
			"address":         "Sonnenallee 148",
			"address_details": "",
			"name":            "",
			"email":           "",
			"phone":           "",
			"location_words":  "this is fake",
		},
		Geometry: sextant.GeojsonGeometry{
			Type:        "Point",
			Coordinates: location.ToArray(),
		},
	}

	route := sextant.VroomRoute{
		VehicleID: 1,
		Steps: []sextant.VroomRoutingStep{
			sextant.VroomRoutingStep{
				Location: &[2]float64{13.44575, 52.47865},
			},
		},
	}

	f_want_route := &sextant.GeojsonFeature{
		Type:       "Feature",
		Properties: map[string]string{},
		Geometry: sextant.GeojsonGeometry{
			Type:        "LineString",
			Coordinates: []*[2]float64{&[2]float64{13.44575, 52.47865}},
		},
	}

	t.Run("CreateFeature", func(t *testing.T) {
		tcs := []struct {
			input interface{}
			want  interface{}
		}{
			{contactAddress, f_want_contact},
			{route, f_want_route},
		}
		for _, tc := range tcs {
			tc_name := fmt.Sprintf("%v", reflect.TypeOf(tc.input))
			t.Run(tc_name, func(t *testing.T) {
				got := service.CreateFeature(tc.input)
				if !reflect.DeepEqual(got, tc.want) {
					t.Errorf("GeojsonFeatureService.Create() -> \n  got:\t%v\n want:\t%v", got, tc.want)
				}
			})
		}
	})

	t.Run("CreateFeatureCollection", func(t *testing.T) {
		contact_no_location := sextant.ContactAddress{
			Address: &sextant.Address{Label: "Hermannplatz 1"},
		}
		fc_want_contact := &sextant.GeojsonFeatureCollection{
			Type:     "FeatureCollection",
			Features: []*sextant.GeojsonFeature{f_want_contact},
		}
		fc_want_route := &sextant.GeojsonFeatureCollection{
			Type:     "FeatureCollection",
			Features: []*sextant.GeojsonFeature{f_want_route},
		}
		tcs := []struct {
			input interface{}
			want  interface{}
		}{
			{[]sextant.ContactAddress{contactAddress, contact_no_location}, fc_want_contact},
			{[]sextant.VroomRoute{route}, fc_want_route},
		}
		for _, tc := range tcs {
			tc_name := fmt.Sprintf("%v", reflect.TypeOf(tc.input))
			t.Run(tc_name, func(t *testing.T) {
				got := service.CreateFeatureCollection(tc.input)
				if !reflect.DeepEqual(got, tc.want) {
					t.Errorf("FeatureCollection.Init() -> \n  got:\t%v\n want:\t%v", got, tc.want)
				}
			})
		}
	})
}
