package inmem

import (
	"eotl.supply/sextant"
	"time"
)

var _ sextant.TimeWindowService = (*TimeWindowService)(nil)

type TimeWindowService struct {
}

func NewTimeWindowService() *TimeWindowService {
	return &TimeWindowService{}
}

func (service *TimeWindowService) Create(start string, end string) (timewindow *sextant.TimeWindow, err error) {
	timewindow = &sextant.TimeWindow{}
	timewindow.Start, err = time.Parse(sextant.TimeLayout, start)
	if err != nil {
		return nil, err
	}
	timewindow.End, err = time.Parse(sextant.TimeLayout, end)
	if err != nil {
		return nil, err
	}
	return timewindow, nil
}
