package inmem

import (
	"encoding/json"
	"eotl.supply/sextant"
	"reflect"
	"testing"
)

func TestJsonEncoder(t *testing.T) {
	joe := sextant.Contact{Name: "Joe"}
	encoder := JsonEncoder{}

	t.Run("Encode", func(t *testing.T) {
		tcs := []struct {
			name       string
			input      interface{}
			wantStruct interface{}
			wantErr    error
		}{
			{"Contact", joe, JsonEnvelope{Type: "sextant.Contact", Msg: joe}, nil},
			{"[]Contact", []sextant.Contact{joe}, JsonEnvelope{Type: "[]sextant.Contact", Msg: []sextant.Contact{joe}}, nil},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				want, err := json.Marshal(tc.wantStruct)
				if err != nil {
					t.Fatal(err)
				}
				got, got_err := encoder.EncodeToJson(tc.input)
				if got_err != tc.wantErr {
					t.Errorf("Decode unexpted error: got %v want %v", got_err, tc.wantErr)
				}
				if !reflect.DeepEqual(got, want) {
					t.Errorf("Encode failed: got\n%v\nwant\n%v", string(got), string(want))
				}
			})
		}
	})
	t.Run("Decode", func(t *testing.T) {
		tcs := []struct {
			name        string
			inputStruct interface{}
			want        interface{}
			wantErr     error
		}{
			{"Contact", JsonEnvelope{Type: "sextant.Contact", Msg: joe}, joe, nil},
			{"Contact", JsonEnvelope{Type: "[]sextant.Contact", Msg: []sextant.Contact{joe}}, []sextant.Contact{joe}, nil},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				input, err := json.Marshal(tc.inputStruct)
				if err != nil {
					t.Fatal(err)
				}
				got, got_err := encoder.DecodeFromJson(input)
				if got_err != tc.wantErr {
					t.Errorf("Decode unexpted error: got %v want %v", got_err, tc.wantErr)
				}
				if !reflect.DeepEqual(got, tc.want) {
					t.Errorf("Encode failed: got\n%v\nwant\n%v", got, tc.want)
				}
			})
		}
	})
}
