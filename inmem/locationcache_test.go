package inmem

import (
	"eotl.supply/sextant"
	"io/ioutil"
	"os"
	"os/exec"
	"reflect"
	"regexp"
	"strings"
	"testing"
)

func TestLocationCache(t *testing.T) {

	t.Run("Load", func(t *testing.T) {
		cache := LocationCache{BaseDir: "testdata", Namespace: []string{"input", "locationcache"}}
		location := sextant.Location{Lon: 1, Lat: 2, Words: ""}
		tcs := []struct {
			name      string
			input_key string
			want      *sextant.Location
			want_err  interface{}
		}{
			{"CacheFileExists", "1", &location, nil},
			{"CacheFileMissing", "2", nil, nil},
		}
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				got, got_err := cache.Load(tc.input_key)
				if got_err != nil {
					if ok, _ := regexp.MatchString(tc.want_err.(string), got_err.Error()); !ok {
						t.Errorf("Load() -> an unexpected error occurred: got %v want %v", got_err, tc.want_err)
					}
				} else if !reflect.DeepEqual(got, tc.want) {
					t.Errorf("Load() -> unexpected file output: input %s, got %v, want %v", tc.input_key, got, tc.want)
				}
			})
		}
	})

	t.Run("Store", func(t *testing.T) {
		tmpDir, _ := ioutil.TempDir("", "test-locationcache")
		defer os.RemoveAll(tmpDir)
		cache := LocationCache{BaseDir: tmpDir}
		location := sextant.Location{Lon: 1, Lat: 2, Words: ""}
		tcs := []struct {
			name        string
			input_key   string
			input_value *sextant.Location
			want_err    string
		}{
			{"validInput", "1", &location, ""},
			{"fileAlreadyExists", "2", &location, ""},
			{"toInvalidPath", "3", &location, ""},
		}
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				if tc.name == "toInvalidPath" {
					t.Skip("Implement me")
				}
				if tc.name == "fileAlreadyExists" {
					t.Skip("Implement me")
				}
				got_err := cache.Store(tc.input_key, tc.input_value)
				if tc.want_err != "" || got_err != nil {
					var got_err_msg string
					if got_err != nil {
						got_err_msg = got_err.Error()
					}
					if ok, _ := regexp.MatchString(tc.want_err, got_err_msg); !ok {
						t.Errorf("Store() -> an unexpected error occurred: got %v want '%v'", got_err, tc.want_err)
					}
				}
				want_file := "testdata" + PS + "want" + PS + "location_file_cache" + PS + tc.input_key
				got_file := tmpDir + PS + tc.input_key
				cmd := exec.Command("diff", got_file, want_file)
				out, err := cmd.CombinedOutput()
				if err != nil {
					t.Errorf("Store() -> unexpected file output: got %s, want %s\n%s\n", got_file, want_file, out)
				}
			})
		}
	})

	t.Run("GetKey", func(t *testing.T) {
		cache := LocationCache{}
		got := cache.GetKey("test")
		want := "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"
		if got != want {
			t.Errorf("GetKey() -> got %v, want %s", got, want)
		}
	})

	t.Run("setDir", func(t *testing.T) {
		cache := LocationCache{}
		tcs := []struct {
			name       string
			input_path string
			input_ns   []string
		}{
			{"validWithNoPath", "", []string{"eotl", "locations"}},
			{"validWithPath", "/tmp", []string{"eotl", "locations"}},
		}
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				path := tc.input_path
				if len(tc.input_path) < 1 {
					path, _ = os.UserCacheDir()
				}
				want := path + PS + strings.Join(tc.input_ns[:], PS)
				cache.setDir(tc.input_path, tc.input_ns...)
				got := cache.dir
				if got != want {
					t.Errorf("getCacheDir() -> got: %v\nwant:%v\n", got, want)
				}
			})
		}
	})
}

func BenchmarkLocationCache(b *testing.B) {
	cache := LocationCache{BaseDir: "testdata", Namespace: []string{"input", "locationcache"}}
	b.Run("Load", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			cache.Load("1")
		}
	})

	b.Run("GetKey", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			cache.GetKey("test")
		}
	})
}
