package inmem

import (
	"encoding/json"
	"eotl.supply/sextant"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

const pathSep = string(os.PathSeparator)

var _ sextant.FileService = (*FileService)(nil)

type FileService struct {
	logger *sextant.Logger
}

func NewFileService(doLog bool) *FileService {
	logger := &sextant.Logger{DoLog: doLog, Dev: os.Stderr}
	return &FileService{logger}
}

func (service *FileService) ToJSON(filename string, data interface{}, path string) (bool, error) {
	if len(path) > 0 {
		filename = service.makePath(path, filename)
	}
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0655)
	if err != nil {
		err = fmt.Errorf("can not open file %s for writing: %v", filename, err)
		return false, err
	}
	defer file.Close()
	encoder := json.NewEncoder(file)
	encoder.SetIndent("", "  ")
	encoder.Encode(data)
	service.logger.Printf("File written -> %s\n", filename)
	return true, nil
}

func (service *FileService) makePath(args ...string) string {
	if len(args) == 1 {
		return args[0] + pathSep
	}
	return strings.Join(args, pathSep)
}

func (service *FileService) GetFileNameWithoutSuffix(path string) string {
	filename := filepath.Base(path)
	out := strings.Split(filename, ".")[0]
	return out
}

// Please remove returned filepath after usage
func (service *FileService) SetFirstLine(filepath string, header string, action string) (string, error) {
	var doReplace bool
	switch action {
	case "prepend":
		doReplace = false
	case "replace":
		doReplace = true
	default:
		err := errors.New(fmt.Sprintf("Unknown csv header action: %s", action))
		return "", err
	}
	if action == "replace" {
		doReplace = true
	}
	input, err := ioutil.ReadFile(filepath)
	if err != nil {
		return "", err
	}
	// Assemble output
	var outputLines []string
	inputLines := strings.Split(string(input), "\n")
	if doReplace {
		outputLines = inputLines[:]
		outputLines[0] = header
	} else {
		size := len(inputLines) + 1
		outputLines = make([]string, size, size)
		outputLines[0] = header
		for i, line := range inputLines {
			outputLines[i+1] = line
		}
	}
	output := strings.Join(outputLines, "\n")
	// Create temporary file
	outputFile, err := os.CreateTemp("", "eotl-csv-")
	if err != nil {
		return "", err
	}
	outputFile.Close()
	// Write output
	outputFilepath := outputFile.Name()
	if err = ioutil.WriteFile(outputFilepath, []byte(output), 0640); err != nil {
		return "", err
	}

	return outputFilepath, nil
}
