package inmem

import (
	"eotl.supply/sextant"
	"testing"
)

func TestZoneService(t *testing.T) {
	s := NewZoneService()

	t.Run("SetZoneAbbr", func(t *testing.T) {
		zones := []sextant.Zone{
			sextant.Zone{Name: "Mitte"},
		}

		if got, got_err := s.SetZoneAbbr(zones); got_err != nil {
			t.Errorf("Unexpected error: %v", got_err)
		} else if got, want := got[0].Abbr, "MTT"; got != want {
			t.Errorf("Mismatch got %s want %s", got, want)
		}
	})
}
