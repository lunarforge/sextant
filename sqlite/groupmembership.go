package sqlite

import (
	"context"
	_ "database/sql"
	"errors"
	"strings"

	"eotl.supply/sextant"
)

var _ sextant.GroupMembershipService = (*GroupMembershipService)(nil)

type GroupMembershipService struct {
	db *DB
}

func NewGroupMembershipService(db *DB) *GroupMembershipService {
	return &GroupMembershipService{db: db}
}

func (s *GroupMembershipService) FindOrCreateGroupMembership(ctx context.Context, membership *sextant.GroupMembership) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := findOrCreateGroupMembership(ctx, tx, membership); err != nil {
		return err
	}
	return tx.Commit()
}

func findOrCreateGroupMembership(ctx context.Context, tx *Tx, membership *sextant.GroupMembership) error {
	membershipFound, err := findGroupMembershipByIDs(ctx, tx, membership.GroupID, membership.ContactAddressID)
	if err != nil {
		if err := createGroupMembership(ctx, tx, membership); err != nil {
			return err
		}
		return nil
	}
	*membership = *membershipFound

	return nil

}

func createGroupMembership(ctx context.Context, tx *Tx, membership *sextant.GroupMembership) error {
	membership.CreatedAt = tx.now
	membership.UpdatedAt = membership.CreatedAt

	if err := membership.Validate(); err != nil {
		return err
	}

	result, err := tx.ExecContext(ctx, `
		INSERT INTO group_memberships (
			group_id,
			contact_address_id,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?)
	`,
		membership.GroupID,
		membership.ContactAddressID,
		(*NullTime)(&membership.CreatedAt),
		(*NullTime)(&membership.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	membership.ID = int(id)

	return nil
}

func (s *GroupMembershipService) FindGroupMembershipByIDs(ctx context.Context, groupID int, contactAddressID int) (*sextant.GroupMembership, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	membership, err := findGroupMembershipByIDs(ctx, tx, groupID, contactAddressID)
	if err != nil {
		return nil, err
	} else if err := attachGroupMembershipAssociations(ctx, tx, membership); err != nil {
		return nil, err
	}

	return membership, nil
}

func findGroupMembershipByIDs(ctx context.Context, tx *Tx, groupID int, contactAddressID int) (*sextant.GroupMembership, error) {
	filter := sextant.GroupMembershipFilter{GroupID: &groupID, ContactAddressID: &contactAddressID}
	memberships, _, err := findGroupMemberships(ctx, tx, filter)
	if err != nil {
		return nil, err
	} else if len(memberships) == 0 {
		return nil, errors.New("GroupMembership not found")
	}
	return memberships[0], nil
}

func findGroupMemberships(ctx context.Context, tx *Tx, filter sextant.GroupMembershipFilter) (_ []*sextant.GroupMembership, n int, err error) {
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.GroupID; v != nil {
		where, args = append(where, "group_id = ?"), append(args, *v)
	}
	if v2 := filter.ContactAddressID; v2 != nil {
		where, args = append(where, "contact_address_id = ?"), append(args, *v2)
	}

	rows, err := tx.QueryContext(ctx, `
		SELECT 
		    id,
				group_id,
				contact_address_id,
		    created_at,
		    updated_at,
		    COUNT(*) OVER()
		FROM group_memberships
		WHERE `+strings.Join(where, " AND ")+`
		ORDER BY id ASC
		`+FormatLimitOffset(filter.Limit, filter.Offset),
		args...,
	)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	memberships := make([]*sextant.GroupMembership, 0)
	for rows.Next() {
		var membership sextant.GroupMembership
		if err := rows.Scan(
			&membership.ID,
			&membership.GroupID,
			&membership.ContactAddressID,
			(*NullTime)(&membership.CreatedAt),
			(*NullTime)(&membership.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		memberships = append(memberships, &membership)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return memberships, n, nil
}

func attachGroupMembershipAssociations(ctx context.Context, tx *Tx, membership *sextant.GroupMembership) (err error) {
	return nil
}
