CREATE TABLE addresses (
	id          INTEGER PRIMARY KEY AUTOINCREMENT,
	location_id INTEGER NOT NULL REFERENCES locations (id),
	label       TEXT NOT NULL UNIQUE,
	created_at  TEXT NOT NULL,
	updated_at  TEXT NOT NULL
);

CREATE TABLE locations (
	id         INTEGER PRIMARY KEY AUTOINCREMENT,
	lat        TEXT NOT NULL,
	lon        TEXT NOT NULL,
	words      TEXT,
	created_at TEXT NOT NULL,
	updated_at TEXT NOT NULL,

	UNIQUE(lat, lon)
);

CREATE TABLE contacts (
	id                 INTEGER PRIMARY KEY AUTOINCREMENT,
	name               TEXT NOT NULL,
	email              TEXT,
	phone              TEXT,
	created_at         TEXT NOT NULL,
	updated_at         TEXT NOT NULL
);

CREATE TABLE contact_addresses (
	id          INTEGER PRIMARY KEY AUTOINCREMENT,
	address_id  INTEGER NOT NULL REFERENCES addresses (id) ON DELETE CASCADE,
	contact_id  INTEGER NOT NULL REFERENCES contacts (id) ON DELETE CASCADE,
	details TEXT,
	created_at  TEXT NOT NULL,
	updated_at  TEXT NOT NULL,

	UNIQUE(address_id, contact_id, details)
);
