CREATE TABLE tours (
  id          INTEGER PRIMARY KEY AUTOINCREMENT,
  name        TEXT NOT NULL UNIQUE,
	slug			  TEXT NO NULL UNIQUE,
	ondate			TEXT NO NULL,
  created_at  TEXT NOT NULL,
  updated_at  TEXT NOT NULL
);

CREATE TABLE tour_items (
  id          INTEGER PRIMARY KEY AUTOINCREMENT,
  tour_id     INTEGER NOT NULL REFERENCES tours (id) ON DELETE CASCADE,
  contactaddress_id INTEGER NOT NULL REFERENCES contact_addresses (id) ON DELETE CASCADE,
  touritemtype_id     INTEGER NOT NULL REFERENCES tour_itemtypes (id) ON DELETE CASCADE,
  created_at  TEXT NOT NULL,
  updated_at  TEXT NOT NULL
);

CREATE TABLE tour_itemtypes (
  id          INTEGER PRIMARY KEY AUTOINCREMENT,
  name        TEXT NOT NULL UNIQUE,
  created_at  TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at  TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO tour_itemtypes (name) VALUES('delivery');
INSERT INTO tour_itemtypes (name) VALUES('pickup');
INSERT INTO tour_itemtypes (name) VALUES('rider');
