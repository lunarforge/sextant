package sqlite

import (
	"context"
	_ "database/sql"
	"errors"
	"fmt"
	"strings"

	"eotl.supply/sextant"
)

var _ sextant.AddressService = (*AddressService)(nil)

type AddressService struct {
	db *DB
}

func NewAddressService(db *DB) *AddressService {
	return &AddressService{db: db}
}

func (s *AddressService) FindOrCreateAddress(ctx context.Context, address *sextant.Address) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := findOrCreateAddress(ctx, tx, address); err != nil {
		return err
	}
	return tx.Commit()

}

func (s *AddressService) FindAddressByID(ctx context.Context, id int) (*sextant.Address, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	// Fetch address object and attach owner user.
	address, err := findAddressByID(ctx, tx, id)
	if err != nil {
		return nil, err
	} else if err := attachAddressAssociations(ctx, tx, address); err != nil {
		return nil, err
	}

	return address, nil
}

func findOrCreateAddress(ctx context.Context, tx *Tx, address *sextant.Address) error {
	addressFound, err := findAddressByLocation(ctx, tx, address.Location)
	// Address does not exist, create it
	if err != nil {
		if err := createAddress(ctx, tx, address); err != nil {
			return err
		}
		return nil
	}
	*address = *addressFound

	return nil
}

func createAddress(ctx context.Context, tx *Tx, address *sextant.Address) error {
	address.CreatedAt = tx.now
	address.UpdatedAt = address.CreatedAt

	if err := createLocation(ctx, tx, address.Location); err != nil {
		return err
	}
	address.LocationID = address.Location.ID

	if err := address.Validate(); err != nil {
		return err
	}

	// Insert row into database.
	result, err := tx.ExecContext(ctx, `
		INSERT INTO addresses (
			location_id,
			label,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?)
	`,
		address.LocationID,
		address.Label,
		(*NullTime)(&address.CreatedAt),
		(*NullTime)(&address.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}

	// Read back new address ID into caller argument.
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	address.ID = int(id)
	return nil
}

func findAddressByLocation(ctx context.Context, tx *Tx, location *sextant.Location) (*sextant.Address, error) {
	locationFound, err := findLocationByLatLon(ctx, tx, location.Lat, location.Lon)
	if err != nil {
		return nil, errors.New("Location not found")
	}
	address, err := findAddressByLocationID(ctx, tx, locationFound.ID)
	if err != nil {
		return nil, errors.New("Address not found")
	}
	address.Location = locationFound
	return address, nil
}

func findAddressByID(ctx context.Context, tx *Tx, id int) (*sextant.Address, error) {
	addresses, _, err := findAddresses(ctx, tx, sextant.AddressFilter{ID: &id})
	if err != nil {
		return nil, err
	} else if len(addresses) == 0 {
		return nil, errors.New("Address not found")
		// TODO: implement sextant.ENOTFOUND etc.
		//return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Address not found."}
	}
	return addresses[0], nil
}

func findAddressByLocationID(ctx context.Context, tx *Tx, id int) (*sextant.Address, error) {
	addresses, _, err := findAddresses(ctx, tx, sextant.AddressFilter{LocationID: &id})
	if err != nil {
		return nil, err
	} else if len(addresses) == 0 {
		return nil, errors.New("Address not found")
		// TODO: implement sextant.ENOTFOUND etc.
		//return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Address not found."}
	}
	return addresses[0], nil
}

// findAddresses retrieves a list of matching addresses. Also returns a total matching
// count which may different from the number of results if filter.Limit is set.
func findAddresses(ctx context.Context, tx *Tx, filter sextant.AddressFilter) (_ []*sextant.Address, n int, err error) {
	// Build WHERE clause. Each part of the WHERE clause is AND-ed together.
	// Values are appended to an arg list to avoid SQL injection.
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.ID; v != nil {
		where, args = append(where, "id = ?"), append(args, *v)
	}
	if locationId := filter.LocationID; locationId != nil {
		where, args = append(where, "id = ?"), append(args, *locationId)
	}

	// Execue query with limiting WHERE clause and LIMIT/OFFSET injected.
	rows, err := tx.QueryContext(ctx, `
		SELECT 
		    id,
				location_id,
		    label,
		    created_at,
		    updated_at,
		    COUNT(*) OVER()
		FROM addresses
		WHERE `+strings.Join(where, " AND ")+`
		ORDER BY id ASC
		`+FormatLimitOffset(filter.Limit, filter.Offset),
		args...,
	)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	// Iterate over rows and deserialize into Address objects.
	addresses := make([]*sextant.Address, 0)
	for rows.Next() {
		var address sextant.Address
		if err := rows.Scan(
			&address.ID,
			&address.LocationID,
			&address.Label,
			(*NullTime)(&address.CreatedAt),
			(*NullTime)(&address.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		addresses = append(addresses, &address)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return addresses, n, nil
}

func attachAddressAssociations(ctx context.Context, tx *Tx, address *sextant.Address) (err error) {
	if address.Location, err = findLocationByID(ctx, tx, address.LocationID); err != nil {
		return fmt.Errorf("attach address location: %w", err)
	}
	return nil
}
