package sqlite

import (
	"context"
	_ "database/sql"
	"fmt"
	"strings"

	"eotl.supply/sextant"
)

var _ sextant.LocationService = (*LocationService)(nil)

type LocationService struct {
	db *DB
}

func NewLocationService(db *DB) *LocationService {
	return &LocationService{db: db}
}

func (s *LocationService) FetchLocation(address string) (*sextant.Location, error) {
	return nil, sextant.Errorf(sextant.ENOTIMPLEMENTED, "Not implemented.")
}

func (s *LocationService) UpdateLocations(people ...[]sextant.ContactAddress) []sextant.ContactAddress {
	return nil
}

func (s *LocationService) FindOrCreateLocation(ctx context.Context, location *sextant.Location) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := findOrCreateLocation(ctx, tx, location); err != nil {
		return err
	}
	return tx.Commit()
}

func (s *LocationService) FindLocationByID(ctx context.Context, id int) (*sextant.Location, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	// Fetch location object and attach owner user.
	location, err := findLocationByID(ctx, tx, id)
	if err != nil {
		return nil, err
	}

	return location, nil
}

func findLocationByID(ctx context.Context, tx *Tx, id int) (*sextant.Location, error) {
	locations, _, err := findLocations(ctx, tx, sextant.LocationFilter{ID: &id})
	if err != nil {
		return nil, err
	} else if len(locations) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Location not found."}
	}
	return locations[0], nil
}

func findLocationByLatLon(ctx context.Context, tx *Tx, lat float64, lon float64) (*sextant.Location, error) {
	locations, _, err := findLocations(ctx, tx, sextant.LocationFilter{Lat: &lat, Lon: &lon})
	if err != nil {
		return nil, err
	} else if len(locations) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Location not found."}
	}
	return locations[0], nil
}

// findLocations retrieves a list of matching locations. Also returns a total matching
// count which may different from the number of results if filter.Limit is set.
func findLocations(ctx context.Context, tx *Tx, filter sextant.LocationFilter) (_ []*sextant.Location, n int, err error) {
	// Build WHERE clause. Each part of the WHERE clause is AND-ed together.
	// Values are appended to an arg list to avoid SQL injection.
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.ID; v != nil {
		where, args = append(where, "id = ?"), append(args, *v)
	}
	if v2 := filter.Lat; v2 != nil {
		where, args = append(where, "lat = ?"), append(args, fmt.Sprintf("%g", *v2))
	}
	if v3 := filter.Lon; v3 != nil {
		where, args = append(where, "lon = ?"), append(args, fmt.Sprintf("%g", *v3))
	}
	if v4 := filter.ZoneID; v4 != nil {
		where, args = append(where, "zm.zone_id = ?"), append(args, *v4)
	}

	sql := `
		SELECT 
			l.id,
			l.lat,
			l.lon,
			l.words,
			l.created_at,
			l.updated_at,
			COUNT(*) OVER()
	 	FROM locations AS l`

	if filter.ZoneID != nil {
		sql = sql + `
		INNER JOIN zone_memberships AS zm
		ON zm.location_id = l.id`
	}

	sql = sql + ` 
		WHERE ` + strings.Join(where, " AND ")

	// Preserve zone polygone location order
	if filter.ZoneID != nil {
		sql = sql + `
		ORDER BY zm.id ASC`
	} else {
		sql = sql + `
		ORDER BY l.id ASC`
	}

	sql = sql + FormatLimitOffset(filter.Limit, filter.Offset)

	// Execue query with limiting WHERE clause and LIMIT/OFFSET injected.
	rows, err := tx.QueryContext(ctx, sql, args...)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	// Iterate over rows and deserialize into Location objects.
	locations := make([]*sextant.Location, 0)
	for rows.Next() {
		var location sextant.Location
		if err := rows.Scan(
			&location.ID,
			&location.Lat,
			&location.Lon,
			&location.Words,
			(*NullTime)(&location.CreatedAt),
			(*NullTime)(&location.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		locations = append(locations, &location)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return locations, n, nil
}

func findOrCreateLocation(ctx context.Context, tx *Tx, location *sextant.Location) error {
	locationFound, err := findLocationByLatLon(ctx, tx, location.Lat, location.Lon)
	if err != nil {
		if err := createLocation(ctx, tx, location); err != nil {
			return err
		}
		return nil
	}
	*location = *locationFound

	return nil
}

func createLocation(ctx context.Context, tx *Tx, location *sextant.Location) error {
	location.CreatedAt = tx.now
	location.UpdatedAt = location.CreatedAt

	if err := location.Validate(); err != nil {
		return err
	}

	// Insert row into database.
	result, err := tx.ExecContext(ctx, `
		INSERT INTO locations (
			lat,
			lon,
			words,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?, ?)
	`,
		fmt.Sprintf("%g", location.Lat),
		fmt.Sprintf("%g", location.Lon),
		location.Words,
		(*NullTime)(&location.CreatedAt),
		(*NullTime)(&location.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}

	// Read back new location ID into caller argument.
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	location.ID = int(id)
	// Create ZoneMemberships for location
	if err := createZoneMembershipsForLocation(ctx, tx, location, nil); err != nil {
		return err
	}
	return nil
}
