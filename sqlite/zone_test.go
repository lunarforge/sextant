package sqlite_test

import (
	"context"
	"reflect"
	"testing"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
	"github.com/paulmach/orb"
)

func TestZoneService(t *testing.T) {
	t.Run("CreateZone", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()

		locationInsideZone := &sextant.Location{Lon: 1, Lat: 1}
		locationOutsideZone := &sextant.Location{Lon: 3, Lat: 3}

		MustCreateLocation(t, ctx, db, locationInsideZone)
		MustCreateLocation(t, ctx, db, locationOutsideZone)

		s := sqlite.NewZoneService(db)

		zone := &sextant.Zone{
			Abbr:     "BLN",
			Name:     "Berlin",
			Geometry: orb.Polygon{{{0, 0}, {0, 2}, {2, 2}, {2, 0}, {0, 0}}},
		}

		if err := s.CreateZone(ctx, zone); err != nil {
			t.Fatal(err)
		} else if got, want := zone.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, zone)
		} else if zone.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if zone.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}

		other, err := s.FindZoneByID(ctx, 1)
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(zone, other) {
			t.Fatalf("mismatch: %#v != %#v", zone, other)
		}

		s2 := sqlite.NewZoneMembershipService(db)
		if zm, err := s2.FindZoneMembershipByIDs(ctx, zone.ID, locationInsideZone.ID); err != nil {
			t.Fatal(err)
		} else if zm == nil {
			t.Fatal("ZoneMembership was no found")
		} else if _, err := s2.FindZoneMembershipByIDs(ctx, zone.ID, locationOutsideZone.ID); sextant.ErrorCode(err) != sextant.ENOTFOUND {
			t.Fatalf("unexpected error: %#v", err)
		}

		t.Run("AsChild", func(t *testing.T) {
			zone2 := &sextant.Zone{
				ParentID: &zone.ID,
				Abbr:     "NKN",
				Name:     "Neukölln",
				Geometry: orb.Polygon{{{0, 0}, {0, 2}, {2, 2}, {2, 0}, {0, 0}}},
			}

			if err := s.CreateZone(ctx, zone2); err != nil {
				t.Fatal(err)
			} else if got, want := zone2.ID, 2; got != want {
				t.Fatalf("ID=%v, want %v %+v", got, want, zone)
			} else if zone2.CreatedAt.IsZero() {
				t.Fatal("expected created at")
			} else if zone2.UpdatedAt.IsZero() {
				t.Fatal("expected updated at")
			}

			other, err := s.FindZoneByID(ctx, 2)
			if err != nil {
				t.Fatal(err)
			} else if !reflect.DeepEqual(zone2, other) {
				t.Fatalf("mismatch: %#v != %#v", zone2, other)
			}

			if zm3, err := s2.FindZoneMembershipByIDs(ctx, zone2.ID, locationInsideZone.ID); err != nil {
				t.Fatal(err)
			} else if zm3 == nil {
				t.Fatal("Expected ZoneMembership was not found")
			} else if _, err := s2.FindZoneMembershipByIDs(ctx, zone2.ID, locationOutsideZone.ID); sextant.ErrorCode(err) != sextant.ENOTFOUND {
				t.Fatalf("unexpected error: %#v", err)
			}
		})

	})
	t.Run("DeleteZone", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewZoneService(db)

		zone := MustCreateZone(t, ctx, db, &sextant.Zone{Name: "BER"})

		if err := s.DeleteZone(ctx, zone.ID); err != nil {
			t.Fatal(err)
		} else if _, err := s.FindZoneByID(ctx, zone.ID); err != nil && sextant.ErrorCode(err) != sextant.ENOTFOUND {
			t.Fatalf("unexpected error: %#v", err)
		}
	})
}

func MustCreateZone(tb testing.TB, ctx context.Context, db *sqlite.DB, zone *sextant.Zone) *sextant.Zone {
	tb.Helper()
	if err := sqlite.NewZoneService(db).CreateZone(ctx, zone); err != nil {
		tb.Fatal(err)
	}
	return zone
}
