package sqlite_test

import (
	"context"
	_ "fmt"
	"reflect"
	_ "strings"
	"testing"
	_ "time"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
)

func TestContactService(t *testing.T) {
	t.Run("CreateContact", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)

		ctx := context.Background()

		s := sqlite.NewContactService(db)
		contact := &sextant.Contact{Name: "Harry"}

		if err := s.FindOrCreateContact(ctx, contact); err != nil {
			t.Fatal(err)
		} else if got, want := contact.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, contact)
		} else if contact.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if contact.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}

		other, err := s.FindContactByID(ctx, 1)
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(contact, other) {
			t.Fatalf("mismatch: %#v != %#v", contact, other)
		}

		t.Run("AsDuplicate", func(t *testing.T) {
			contact_dup := &sextant.Contact{Name: "Harry"}

			if err := s.FindOrCreateContact(ctx, contact_dup); err != nil {
				t.Fatal(err)
			} else if got, want := contact_dup.ID, 1; got != want {
				t.Fatalf("ID=%v, want %v %+v", got, want, contact_dup)
			}
		})
	})
}

func MustCreateContact(tb testing.TB, ctx context.Context, db *sqlite.DB, contact *sextant.Contact) *sextant.Contact {
	tb.Helper()
	if err := sqlite.NewContactService(db).FindOrCreateContact(ctx, contact); err != nil {
		tb.Fatal(err)
	}
	return contact
}
