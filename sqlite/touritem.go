package sqlite

import (
	"context"
	_ "database/sql"
	"fmt"
	"strings"

	"eotl.supply/sextant"
)

var _ sextant.TourItemService = (*TourItemService)(nil)

type TourItemService struct {
	db *DB
}

func NewTourItemService(db *DB) *TourItemService {
	return &TourItemService{db: db}
}

func (s *TourItemService) CreateTourItem(ctx context.Context, item *sextant.TourItem) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := createTourItem(ctx, tx, item); err != nil {
		return err
	}
	return tx.Commit()
}

// Deletes a TourItem
func (s *TourItemService) DeleteTourItem(ctx context.Context, id int) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := deleteTourItem(ctx, tx, id); err != nil {
		return err
	}
	return tx.Commit()
}

func (s *TourItemService) FindTourItemByID(ctx context.Context, id int) (*sextant.TourItem, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	item, err := findTourItemByID(ctx, tx, id)
	if err != nil {
		return nil, err
	} else if err := attachTourItemAssociations(ctx, tx, item); err != nil {
		return nil, err
	}

	return item, nil
}

func (s *TourItemService) FindTourItemsByTourID(ctx context.Context, tourID int) ([]sextant.TourItem, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	filter := sextant.TourItemFilter{TourID: &tourID}
	items, _, err := findTourItems(ctx, tx, filter)
	if err != nil {
		return nil, err
	}
	// Attach associations
	for i, _ := range items {
		if err := attachTourItemAssociations(ctx, tx, &items[i]); err != nil {
			return nil, err
		}
	}

	return items, nil
}

// UpdateTourItem updates the state of a touritem.
func (s *TourItemService) UpdateTourItem(ctx context.Context, id int, upd sextant.TourItemUpdate) (*sextant.TourItem, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	item, err := updateTourItem(ctx, tx, id, upd)
	if err != nil {
		return item, err
	}
	return item, tx.Commit()
}

func createTourItem(ctx context.Context, tx *Tx, item *sextant.TourItem) error {
	item.CreatedAt = tx.now
	item.UpdatedAt = item.CreatedAt

	if err := item.Validate(); err != nil {
		return err
	}

	result, err := tx.ExecContext(ctx, `
		INSERT INTO tour_items (
			tour_id,
			contactaddress_id,
			touritemtype_id,
			state,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?, ?, ?)
	`,
		item.TourID,
		item.ContactAddressID,
		item.TourItemtypeID,
		item.State,
		(*NullTime)(&item.CreatedAt),
		(*NullTime)(&item.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	item.ID = int(id)

	return nil
}

func findTourItemByID(ctx context.Context, tx *Tx, id int) (*sextant.TourItem, error) {
	items, _, err := findTourItems(ctx, tx, sextant.TourItemFilter{ID: &id})
	if err != nil {
		return nil, err
	} else if len(items) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "TourItem not found."}
	}
	return &items[0], nil
}

func findTourItems(ctx context.Context, tx *Tx, filter sextant.TourItemFilter) (_ []sextant.TourItem, n int, err error) {
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.ID; v != nil {
		where, args = append(where, "id = ?"), append(args, *v)
	}
	if v2 := filter.TourID; v2 != nil {
		where, args = append(where, "tour_id = ?"), append(args, *v2)
	}
	if v3 := filter.TourItemtypeID; v3 != nil {
		where, args = append(where, "touritemtype_id = ?"), append(args, *v3)
	}

	rows, err := tx.QueryContext(ctx, `
		SELECT 
		    id,
				tour_id,
				contactaddress_id,
				touritemtype_id,
				state,
		    created_at,
		    updated_at,
		    COUNT(*) OVER()
		FROM tour_items
		WHERE `+strings.Join(where, " AND ")+`
		ORDER BY id ASC
		`+FormatLimitOffset(filter.Limit, filter.Offset),
		args...,
	)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	items := make([]sextant.TourItem, 0)
	for rows.Next() {
		var item sextant.TourItem
		if err := rows.Scan(
			&item.ID,
			&item.TourID,
			&item.ContactAddressID,
			&item.TourItemtypeID,
			&item.State,
			(*NullTime)(&item.CreatedAt),
			(*NullTime)(&item.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		items = append(items, item)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return items, n, nil
}

func deleteTourItem(ctx context.Context, tx *Tx, id int) error {
	item, err := findTourItemByID(ctx, tx, id)
	if err != nil {
		return err
	}
	if _, err := tx.ExecContext(ctx, `DELETE FROM tour_items WHERE id = ?`, item.ID); err != nil {
		return FormatError(err)
	}
	return nil
}

func updateTourItem(ctx context.Context, tx *Tx, id int, upd sextant.TourItemUpdate) (*sextant.TourItem, error) {
	item, err := findTourItemByID(ctx, tx, id)
	if err != nil {
		return item, err
	}

	// Save state of item to compare later in the function.
	prev := *item

	// Update fields.
	if v := upd.State; v != nil {
		item.State = *v
	}

	// Exit if item did not change.
	if prev.State == item.State {
		return item, nil
	}

	// Set last updated date to current time.
	item.UpdatedAt = tx.now

	// Perform basic field validation.
	if err := item.Validate(); err != nil {
		return item, err
	}

	// Execute query to update item value.
	if _, err := tx.ExecContext(ctx, `
		UPDATE tour_items
		SET state = ?,
		    updated_at = ?
		WHERE id = ?
	`,
		item.State,
		(*NullTime)(&item.UpdatedAt),
		id,
	); err != nil {
		return item, FormatError(err)
	}

	return item, nil
}

func attachTourItemAssociations(ctx context.Context, tx *Tx, item *sextant.TourItem) (err error) {
	if item.ContactAddress, err = findContactAddressByID(ctx, tx, item.ContactAddressID); err != nil {
		return fmt.Errorf("attach contact address: %w", err)
	} else if err = attachContactAddressAssociations(ctx, tx, item.ContactAddress); err != nil {
		return fmt.Errorf("attach contact address associations: %w", err)
	}
	return nil
}
