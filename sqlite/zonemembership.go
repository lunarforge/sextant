package sqlite

import (
	"context"
	_ "database/sql"
	"strings"

	"eotl.supply/sextant"
)

var _ sextant.ZoneMembershipService = (*ZoneMembershipService)(nil)

type ZoneMembershipService struct {
	db *DB
}

func NewZoneMembershipService(db *DB) *ZoneMembershipService {
	return &ZoneMembershipService{db: db}
}

func (s *ZoneMembershipService) FindOrCreateZoneMembership(ctx context.Context, membership *sextant.ZoneMembership) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := findOrCreateZoneMembership(ctx, tx, membership); err != nil {
		return err
	}
	return tx.Commit()
}

func findOrCreateZoneMembership(ctx context.Context, tx *Tx, membership *sextant.ZoneMembership) error {
	membershipFound, err := findZoneMembershipByIDs(ctx, tx, membership.ZoneID, membership.LocationID)
	if err != nil {
		if err := createZoneMembership(ctx, tx, membership); err != nil {
			return err
		}
		return nil
	}
	*membership = *membershipFound

	return nil

}

func createZoneMembership(ctx context.Context, tx *Tx, membership *sextant.ZoneMembership) error {
	membership.CreatedAt = tx.now
	membership.UpdatedAt = membership.CreatedAt

	if err := membership.Validate(); err != nil {
		return err
	}

	result, err := tx.ExecContext(ctx, `
		INSERT INTO zone_memberships (
			zone_id,
			location_id,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?)
	`,
		membership.ZoneID,
		membership.LocationID,
		(*NullTime)(&membership.CreatedAt),
		(*NullTime)(&membership.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	membership.ID = int(id)

	return nil
}

func (s *ZoneMembershipService) FindZoneMembershipByIDs(ctx context.Context, zoneID int, contactAddressID int) (*sextant.ZoneMembership, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	membership, err := findZoneMembershipByIDs(ctx, tx, zoneID, contactAddressID)
	if err != nil {
		return nil, err
	} else if err := attachZoneMembershipAssociations(ctx, tx, membership); err != nil {
		return nil, err
	}

	return membership, nil
}

func findZoneMembershipByIDs(ctx context.Context, tx *Tx, zoneID int, contactAddressID int) (*sextant.ZoneMembership, error) {
	filter := sextant.ZoneMembershipFilter{ZoneID: &zoneID, LocationID: &contactAddressID}
	memberships, _, err := findZoneMemberships(ctx, tx, filter)
	if err != nil {
		return nil, err
	} else if len(memberships) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "ZoneMembership not found."}
	}
	return memberships[0], nil
}

func findZoneMemberships(ctx context.Context, tx *Tx, filter sextant.ZoneMembershipFilter) (_ []*sextant.ZoneMembership, n int, err error) {
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.ZoneID; v != nil {
		where, args = append(where, "zone_id = ?"), append(args, *v)
	}
	if v2 := filter.LocationID; v2 != nil {
		where, args = append(where, "location_id = ?"), append(args, *v2)
	}

	rows, err := tx.QueryContext(ctx, `
		SELECT 
		    id,
				zone_id,
				location_id,
		    created_at,
		    updated_at,
		    COUNT(*) OVER()
		FROM zone_memberships
		WHERE `+strings.Join(where, " AND ")+`
		ORDER BY id ASC
		`+FormatLimitOffset(filter.Limit, filter.Offset),
		args...,
	)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	memberships := make([]*sextant.ZoneMembership, 0)
	for rows.Next() {
		var membership sextant.ZoneMembership
		if err := rows.Scan(
			&membership.ID,
			&membership.ZoneID,
			&membership.LocationID,
			(*NullTime)(&membership.CreatedAt),
			(*NullTime)(&membership.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		memberships = append(memberships, &membership)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return memberships, n, nil
}

// Recursively create ZoneMemberships for a Location
func createZoneMembershipsForLocation(ctx context.Context, tx *Tx, location *sextant.Location, parentZoneID *int) error {
	filter := sextant.ZoneFilter{ParentID: parentZoneID}
	zones, _, err := findZones(ctx, tx, filter)
	if err != nil {
		return err
	}
	for _, zone := range zones {
		if zone.Contains(*location) {
			membership := &sextant.ZoneMembership{ZoneID: zone.ID, LocationID: location.ID}
			if err := findOrCreateZoneMembership(ctx, tx, membership); err != nil {
				return err
			}
			nextParentZoneID := &zone.ID
			err := createZoneMembershipsForLocation(ctx, tx, location, nextParentZoneID)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// Recursively create ZoneMemberships for a Zone
func createZoneMembershipsForZone(ctx context.Context, tx *Tx, zone *sextant.Zone) error {
	var filter sextant.LocationFilter
	if zone.ParentID == nil {
		// check all existing locations
		filter = sextant.LocationFilter{}
	} else {
		// only check locations cotained by parent zone
		filter = sextant.LocationFilter{ZoneID: zone.ParentID}
	}
	locations, _, err := findLocations(ctx, tx, filter)
	if err != nil {
		return err
	}
	for _, location := range locations {
		err := createZoneMembershipsForLocation(ctx, tx, location, zone.ParentID)
		if err != nil {
			return err
		}
	}
	return nil
}

func attachZoneMembershipAssociations(ctx context.Context, tx *Tx, membership *sextant.ZoneMembership) (err error) {
	return nil
}
