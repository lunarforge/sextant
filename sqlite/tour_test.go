package sqlite_test

import (
	"context"
	"reflect"
	"testing"
	"time"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
)

func TestTourService(t *testing.T) {
	delivery := newContactAddress("Harry", "Times Square 1", "Apartment 404", 1, 1)
	pickup := newContactAddress("Joe", "Boulevard 2", "", 2, 2)
	rider := newContactAddress("Sam", "Main Street 1", "", 3, 3)
	tDate, err := time.Parse(time.RFC3339, "2022-05-22T00:00:00.00Z")
	if err != nil {
		t.Fatal(err)
	}
	group := &sextant.Group{Name: "Tasty Tacos"}

	tour := &sextant.Tour{
		Name:       "Northern Tour",
		OnDate:     tDate,
		Groups:     []sextant.Group{},
		Deliveries: []sextant.TourItem{sextant.TourItem{ContactAddress: delivery, State: sextant.ValidTourItemStates[0]}},
		Pickups:    []sextant.TourItem{sextant.TourItem{ContactAddress: pickup, State: sextant.ValidTourItemStates[0]}},
		Riders:     []sextant.TourItem{sextant.TourItem{ContactAddress: rider, State: sextant.ValidTourItemStates[0]}},
	}

	t.Run("CreateTour", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewTourService(db)
		MustCreateGroup(t, ctx, db, group)
		tour.Groups = []sextant.Group{*group}
		// This tests existing ContactAddress works
		MustCreateContactAddress(t, ctx, db, rider)

		if err := s.CreateTour(ctx, tour); err != nil {
			t.Fatal(err)
		} else if got, want := tour.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, tour)
		} else if got, want := tour.Slug, "northern-tour"; got != want {
			t.Fatalf("Slug=%v, want %v %+v", got, want, tour)
		} else if tour.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if tour.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}

		/*
			other, err := s.FindTourByIDorSlug(ctx, "1")
			if err != nil {
				t.Fatal(err)
			} else if !reflect.DeepEqual(tour, other) {
				t.Fatalf("mismatch: %#v != %#v", tour, other)
			}
		*/
	})

	t.Run("FindTours", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewTourService(db)

		MustCreateGroup(t, ctx, db, group)
		tour.Groups = []sextant.Group{*group}
		tour := MustCreateTour(t, ctx, db, tour)

		t.Run("NoFilter", func(t *testing.T) {
			filter := sextant.TourFilter{}
			if tours, err := s.FindTours(ctx, filter); err != nil {
				t.Fatal(err)
			} else if got, want := len(tours), 1; got != want {
				t.Fatalf("len(tours)=%v, want %v %+v", got, want, tour)
			} else if got, want := &tours[0], tour; !reflect.DeepEqual(got, want) {
				t.Fatalf("mismatch: \n%#v \n!= \n%#v", got, want)
			}
		})

		t.Run("FilterGroupID", func(t *testing.T) {
			MustCreateTour(t, ctx, db, &sextant.Tour{Name: "Ungrouped Tour"})

			filter := sextant.TourFilter{GroupID: &group.ID}
			tours, err := s.FindTours(ctx, filter)
			if err != nil {
				t.Fatal(err)
			} else if got, want := len(tours), 1; got != want {
				t.Fatalf("COUNT: %v, want %v %+v", got, want, tours)
			}
			if got, want := tours[0], *tour; !reflect.DeepEqual(got, want) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, want)
			}
		})

		t.Run("FilterContactID", func(t *testing.T) {
			MustCreateTour(t, ctx, db, &sextant.Tour{Name: "Tour without items"})
			filter := sextant.TourFilter{ContactID: &(tour.Riders[0].ContactID)}
			tours, err := s.FindTours(ctx, filter)
			if err != nil {
				t.Fatal(err)
			} else if got, want := len(tours), 1; got != want {
				t.Fatalf("COUNT: %v, want %v %+v", got, want, tours)
			}
			if got, want := tours[0], *tour; !reflect.DeepEqual(got, want) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, want)
			}
		})
	})

	t.Run("FindToursByIDorSlug", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewTourService(db)
		MustCreateGroup(t, ctx, db, group)
		tour := &sextant.Tour{
			Name:       "Northern Tour",
			Groups:     []sextant.Group{*group},
			Deliveries: []sextant.TourItem{},
			Riders:     []sextant.TourItem{},
			Pickups:    []sextant.TourItem{},
		}
		want := MustCreateTour(t, ctx, db, tour)

		t.Run("WithID", func(t *testing.T) {
			got, err := s.FindTourByIDorSlug(ctx, "1")
			if err != nil {
				t.Fatal(err)
			} else if !reflect.DeepEqual(got, want) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, want)
			}
		})

		t.Run("WithSlug", func(t *testing.T) {
			got, err := s.FindTourByIDorSlug(ctx, "northern-tour")
			if err != nil {
				t.Fatal(err)
			} else if !reflect.DeepEqual(got, want) {
				t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, want)
			}
		})
	})

	t.Run("DeleteTour", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewTourService(db)

		tour := MustCreateTour(t, ctx, db, &sextant.Tour{Name: "Northern Tour"})

		if err := s.DeleteTour(ctx, tour.ID); err != nil {
			t.Fatal(err)
		} else if _, err := s.FindTourByIDorSlug(ctx, tour.Name); err != nil && sextant.ErrorCode(err) != sextant.ENOTFOUND {
			t.Fatalf("unexpected error: %#v", err)
		}
	})
}

func MustCreateTour(tb testing.TB, ctx context.Context, db *sqlite.DB, tour *sextant.Tour) *sextant.Tour {
	tb.Helper()
	if err := sqlite.NewTourService(db).CreateTour(ctx, tour); err != nil {
		tb.Fatal(err)
	}
	return tour
}

func newTour(name string) *sextant.Tour {
	timeValue := "2022-05-22T00:00:00.00Z"
	onDate, err := time.Parse(time.RFC3339, timeValue)
	if err != nil {
		// This should never happen
		//t.Fatal(err)
	}
	tour := &sextant.Tour{
		Name:       name,
		OnDate:     onDate,
		Groups:     []sextant.Group{},
		Deliveries: []sextant.TourItem{},
		Pickups:    []sextant.TourItem{},
		Riders:     []sextant.TourItem{},
	}
	return tour
}
