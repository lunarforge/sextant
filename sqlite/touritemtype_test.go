package sqlite_test

import (
	"context"
	"testing"

	//"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
)

func TestTourItemtypeService(t *testing.T) {
	// We do not need create dedicated item types
	// Let's use the ones created by migration files

	t.Run("FindToursByName", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewTourItemtypeService(db)

		found, err := s.FindTourItemtypeByName(ctx, "delivery")
		if err != nil {
			t.Fatal(err)
		} else if got, want := found.ID, 1; got != want {
			t.Fatalf("mismatch:\n%#v\n!=\n%#v\n", got, want)
		}
	})
}
