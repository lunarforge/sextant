package sqlite_test

import (
	"context"
	"reflect"
	"testing"

	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
)

func TestGroupService(t *testing.T) {
	t.Run("CreateGroup", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewGroupService(db)
		group := &sextant.Group{Name: "Tasty Taco"}

		if err := s.CreateGroup(ctx, group); err != nil {
			t.Fatal(err)
		} else if got, want := group.ID, 1; got != want {
			t.Fatalf("ID=%v, want %v %+v", got, want, group)
		} else if got, want := group.Slug, "tasty-taco"; got != want {
			t.Fatalf("Slug=%v, want %v %+v", got, want, group)
		} else if group.CreatedAt.IsZero() {
			t.Fatal("expected created at")
		} else if group.UpdatedAt.IsZero() {
			t.Fatal("expected updated at")
		}

		other, err := s.FindGroupByID(ctx, 1)
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(group, other) {
			t.Fatalf("mismatch: %#v != %#v", group, other)
		}

		other2, err := s.FindGroupByName(ctx, "Tasty Taco")
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(group, other2) {
			t.Fatalf("mismatch: %#v != %#v", group, other2)
		}

		other3, err := s.FindGroupBySlug(ctx, "tasty-taco")
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(group, other3) {
			t.Fatalf("mismatch: %#v != %#v", group, other3)
		}

		other4, err := s.FindGroupByIDorSlug(ctx, "1")
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(group, other4) {
			t.Fatalf("mismatch: %#v != %#v", group, other4)
		}

		other5, err := s.FindGroupByIDorSlug(ctx, "tasty-taco")
		if err != nil {
			t.Fatal(err)
		} else if !reflect.DeepEqual(group, other5) {
			t.Fatalf("mismatch: %#v != %#v", group, other5)
		}
	})

	t.Run("FindGroup", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewGroupService(db)
		group := MustCreateGroup(t, ctx, db, &sextant.Group{Name: "Tasty Tacos"})
		tour := MustCreateTour(t, ctx, db, &sextant.Tour{Name: "Northern Tour"})
		item := &sextant.GroupTourItem{GroupID: group.ID, TourID: tour.ID}
		MustCreateGroupTourItem(t, ctx, db, item)

		found, err := s.FindGroupByID(ctx, 1)
		if err != nil {
			t.Fatal(err)
		} else if got, want := len(found.Tours), 1; got != want {
			t.Fatalf("len(Tours): %#v != %#v", got, want)
		} else if got, want := &found.Tours[0], tour; !reflect.DeepEqual(got, want) {
			t.Fatalf("Tours:\n%#v\n!=\n%#v", got, want)
		}
	})

	t.Run("DeleteGroup", func(t *testing.T) {
		db := MustOpenDB(t)
		defer MustCloseDB(t, db)
		ctx := context.Background()
		s := sqlite.NewGroupService(db)

		group := MustCreateGroup(t, ctx, db, &sextant.Group{Name: "Tasty Carrots"})

		if err := s.DeleteGroup(ctx, group.ID); err != nil {
			t.Fatal(err)
		} else if _, err := s.FindGroupByID(ctx, group.ID); err != nil && sextant.ErrorCode(err) != sextant.ENOTFOUND {
			t.Fatalf("unexpected error: %#v", err)
		}
	})
}

func MustCreateGroup(tb testing.TB, ctx context.Context, db *sqlite.DB, group *sextant.Group) *sextant.Group {
	tb.Helper()
	if err := sqlite.NewGroupService(db).CreateGroup(ctx, group); err != nil {
		tb.Fatal(err)
	}
	return group
}
