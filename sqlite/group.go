package sqlite

import (
	"context"
	_ "database/sql"
	"strconv"
	"strings"

	"eotl.supply/sextant"
	"github.com/gosimple/slug"
)

var _ sextant.GroupService = (*GroupService)(nil)

type GroupService struct {
	db *DB
}

func NewGroupService(db *DB) *GroupService {
	return &GroupService{db: db}
}

func (s *GroupService) CreateGroup(ctx context.Context, group *sextant.Group) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := createGroup(ctx, tx, group); err != nil {
		return err
	}
	return tx.Commit()
}

func (s *GroupService) DeleteGroup(ctx context.Context, id int) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := deleteGroup(ctx, tx, id); err != nil {
		return err
	}
	return tx.Commit()
}

func (s *GroupService) FindAllGroups(ctx context.Context) ([]sextant.Group, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()
	groups, _, err := findGroups(ctx, tx, sextant.GroupFilter{})
	if err != nil {
		return nil, err
	}
	for i, _ := range groups {
		if err := attachGroupAssociations(ctx, tx, &groups[i]); err != nil {
			return nil, err
		}
	}
	return groups, nil
}

func (s *GroupService) FindGroupByIDorSlug(ctx context.Context, idOrSlug string) (*sextant.Group, error) {
	var id int
	var slug string
	var group *sextant.Group

	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	// Parse ID or slug
	if id, err = strconv.Atoi(idOrSlug); err != nil {
		slug = idOrSlug
	}
	// Fetch group object and attach owner user.
	if id > 0 {
		group, err = findGroupByID(ctx, tx, id)
	} else {
		group, err = findGroupBySlug(ctx, tx, slug)
	}
	if err != nil {
		return nil, err
	} else if err := attachGroupAssociations(ctx, tx, group); err != nil {
		return nil, err
	}

	return group, nil
}

func (s *GroupService) FindGroupByID(ctx context.Context, id int) (*sextant.Group, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()

	// Fetch group object and attach owner user.
	group, err := findGroupByID(ctx, tx, id)
	if err != nil {
		return nil, err
	} else if err := attachGroupAssociations(ctx, tx, group); err != nil {
		return nil, err
	}

	return group, nil
}

func (s *GroupService) FindGroupByName(ctx context.Context, name string) (*sextant.Group, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()
	group, err := findGroupByName(ctx, tx, name)
	if err != nil {
		return nil, err
	} else if err := attachGroupAssociations(ctx, tx, group); err != nil {
		return nil, err
	}

	return group, nil
}

func (s *GroupService) FindGroupBySlug(ctx context.Context, slug string) (*sextant.Group, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()
	group, err := findGroupBySlug(ctx, tx, slug)
	if err != nil {
		return nil, err
	} else if err := attachGroupAssociations(ctx, tx, group); err != nil {
		return nil, err
	}

	return group, nil
}

func createGroup(ctx context.Context, tx *Tx, group *sextant.Group) error {
	group.CreatedAt = tx.now
	group.UpdatedAt = group.CreatedAt
	group.Slug = slug.Make(group.Name)

	if err := group.Validate(); err != nil {
		return err
	}
	result, err := tx.ExecContext(ctx, `
		INSERT INTO groups (
			name,
			slug,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?)
	`,
		group.Name,
		group.Slug,
		(*NullTime)(&group.CreatedAt),
		(*NullTime)(&group.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	group.ID = int(id)

	return nil
}

func findGroupByID(ctx context.Context, tx *Tx, id int) (*sextant.Group, error) {
	groups, _, err := findGroups(ctx, tx, sextant.GroupFilter{ID: &id})
	if err != nil {
		return nil, err
	} else if len(groups) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Group not found."}
	}
	return &groups[0], nil
}

func findGroupByName(ctx context.Context, tx *Tx, name string) (*sextant.Group, error) {
	groups, _, err := findGroups(ctx, tx, sextant.GroupFilter{Name: &name})
	if err != nil {
		return nil, err
	} else if len(groups) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Group not found."}
	}
	return &groups[0], nil
}

func findGroupBySlug(ctx context.Context, tx *Tx, slug string) (*sextant.Group, error) {
	groups, _, err := findGroups(ctx, tx, sextant.GroupFilter{Slug: &slug})
	if err != nil {
		return nil, err
	} else if len(groups) == 0 {
		return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Group not found."}
	}
	return &groups[0], nil
}

func findGroups(ctx context.Context, tx *Tx, filter sextant.GroupFilter) (_ []sextant.Group, n int, err error) {
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.ID; v != nil {
		where, args = append(where, "id = ?"), append(args, *v)
	}
	if v2 := filter.Name; v2 != nil {
		where, args = append(where, "name = ?"), append(args, *v2)
	}
	if v3 := filter.Slug; v3 != nil {
		where, args = append(where, "slug = ?"), append(args, *v3)
	}
	if v4 := filter.TourID; v4 != nil {
		where, args = append(where, "gti.tour_id = ?"), append(args, *v4)
	}

	sql := `
		SELECT 
		    g.id,
		    g.name,
				g.slug,
		    g.created_at,
		    g.updated_at,
		    COUNT(*) OVER()
		FROM groups As g`

	if filter.TourID != nil {
		sql = sql + `
		INNER JOIN group_tour_items AS gti 
		ON gti.group_id = g.id`
	}

	sql = sql + `
		WHERE ` + strings.Join(where, " AND ")

	sql = sql + `
		ORDER BY g.id ASC ` + FormatLimitOffset(filter.Limit, filter.Offset)

	rows, err := tx.QueryContext(ctx, sql, args...)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	// Iterate over rows and deserialize into Group objects.
	groups := make([]sextant.Group, 0)
	for rows.Next() {
		var group sextant.Group
		if err := rows.Scan(
			&group.ID,
			&group.Name,
			&group.Slug,
			(*NullTime)(&group.CreatedAt),
			(*NullTime)(&group.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		groups = append(groups, group)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return groups, n, nil
}

func deleteGroup(ctx context.Context, tx *Tx, id int) error {
	group, err := findGroupByID(ctx, tx, id)
	if err != nil {
		return err
	}
	if _, err := tx.ExecContext(ctx, `DELETE FROM groups WHERE id = ?`, group.ID); err != nil {
		return FormatError(err)
	}
	return nil
}

func attachGroupAssociations(ctx context.Context, tx *Tx, group *sextant.Group) (err error) {
	filter := sextant.TourFilter{GroupID: &group.ID}
	tours, _, err := findTours(ctx, tx, filter)
	if err != nil {
		return err
	}
	for _, tour := range tours {
		group.Tours = append(group.Tours, tour)
	}
	return nil
}
