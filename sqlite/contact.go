package sqlite

import (
	"context"
	_ "database/sql"
	"errors"
	_ "fmt"
	"strings"

	"eotl.supply/sextant"
)

var _ sextant.ContactService = (*ContactService)(nil)

type ContactService struct {
	db *DB
}

func NewContactService(db *DB) *ContactService {
	return &ContactService{db: db}
}

func (s *ContactService) FindOrCreateContact(ctx context.Context, contact *sextant.Contact) error {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := findOrCreateContact(ctx, tx, contact); err != nil {
		return err
	}
	return tx.Commit()
}

func (s *ContactService) FindContactByID(ctx context.Context, id int) (*sextant.Contact, error) {
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback()
	contact, err := findContactByID(ctx, tx, id)
	if err != nil {
		return nil, err
	} else if err := attachContactAssociations(ctx, tx, contact); err != nil {
		return nil, err
	}
	return contact, nil
}

func findOrCreateContact(ctx context.Context, tx *Tx, contact *sextant.Contact) error {
	contactFound, err := findContactByEmailNamePhone(ctx, tx, contact.Email, contact.Name, contact.Phone)
	if err != nil {
		if err := createContact(ctx, tx, contact); err != nil {
			return err
		}
		return nil
	}
	*contact = *contactFound

	return nil
}

func createContact(ctx context.Context, tx *Tx, contact *sextant.Contact) error {
	contact.CreatedAt = tx.now
	contact.UpdatedAt = contact.CreatedAt
	// Create Contact
	if err := contact.Validate(); err != nil {
		return err
	}
	result, err := tx.ExecContext(ctx, `
		INSERT INTO contacts (
			name,
			phone,
			email,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?, ?)
	`,
		contact.Name,
		contact.Phone,
		contact.Email,
		(*NullTime)(&contact.CreatedAt),
		(*NullTime)(&contact.UpdatedAt),
	)
	if err != nil {
		return FormatError(err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return err
	}
	contact.ID = int(id)
	return nil
}

func findContactByEmailNamePhone(ctx context.Context, tx *Tx, email string, name string, phone string) (*sextant.Contact, error) {
	contacts, _, err := findContacts(ctx, tx, sextant.ContactFilter{Email: &email, Name: &name, Phone: &phone})
	if err != nil {
		return nil, err
	} else if len(contacts) == 0 {
		return nil, errors.New("Contact not found")
		// TODO: implement sextant.ENOTFOUND etc.
		//return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Contact not found."}
	}
	return contacts[0], nil
}

func findContactByID(ctx context.Context, tx *Tx, id int) (*sextant.Contact, error) {
	contacts, _, err := findContacts(ctx, tx, sextant.ContactFilter{ID: &id})
	if err != nil {
		return nil, err
	} else if len(contacts) == 0 {
		return nil, errors.New("Contact not found")
		// TODO: implement sextant.ENOTFOUND etc.
		//return nil, &sextant.Error{Code: sextant.ENOTFOUND, Message: "Contact not found."}
	}
	return contacts[0], nil
}

func findContacts(ctx context.Context, tx *Tx, filter sextant.ContactFilter) (_ []*sextant.Contact, n int, err error) {
	where, args := []string{"1 = 1"}, []interface{}{}
	if v := filter.ID; v != nil {
		where, args = append(where, "id = ?"), append(args, *v)
	}
	if v2 := filter.Email; v2 != nil {
		where, args = append(where, "email = ?"), append(args, *v2)
	}
	if v3 := filter.Name; v3 != nil {
		where, args = append(where, "name = ?"), append(args, *v3)
	}
	if v4 := filter.Phone; v4 != nil {
		where, args = append(where, "phone = ?"), append(args, *v4)
	}

	rows, err := tx.QueryContext(ctx, `
		SELECT 
		    id,
		    name,
				phone,
				email,
		    created_at,
		    updated_at,
		    COUNT(*) OVER()
		FROM contacts
		WHERE `+strings.Join(where, " AND ")+`
		ORDER BY id ASC
		`+FormatLimitOffset(filter.Limit, filter.Offset),
		args...,
	)
	if err != nil {
		return nil, n, FormatError(err)
	}
	defer rows.Close()

	// Iterate over rows and deserialize into Contact objects.
	contacts := make([]*sextant.Contact, 0)
	for rows.Next() {
		var contact sextant.Contact
		if err := rows.Scan(
			&contact.ID,
			&contact.Name,
			&contact.Phone,
			&contact.Email,
			(*NullTime)(&contact.CreatedAt),
			(*NullTime)(&contact.UpdatedAt),
			&n,
		); err != nil {
			return nil, 0, err
		}
		contacts = append(contacts, &contact)
	}
	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return contacts, n, nil
}

func attachContactAssociations(ctx context.Context, tx *Tx, contact *sextant.Contact) (err error) {
	return nil
}
