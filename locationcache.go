package sextant

type LocationCache interface {
	Store(key string, value *Location) (err error)

	Load(key string) (value *Location, err error)

	GetKey(str string) string
}
