package sextant

import (
	"context"
	"errors"
	"time"
)

type Address struct {
	ID         int    `json:"-" yaml:"-"`
	Label      string `json:"address" csv:"Address" yaml:"address"`
	*Location  `json:"location,omitempty"`
	LocationID int       `json:"-" yaml:"-"`
	CreatedAt  time.Time `json:"-" yaml:"-"`
	UpdatedAt  time.Time `json:"-" yaml:"-"`
}

func (a *Address) Validate() error {
	if a.Label == "" {
		return errors.New("Address label required.")
	}
	return nil
}

type AddressFilter struct {
	ID         *int    `json:"id"`
	LocationID *int    `json:"location_id"`
	Label      *string `json:"string"`
	Offset     int     `json:"offset"`
	Limit      int     `json:"limit"`
}

type AddressService interface {
	FindOrCreateAddress(ctx context.Context, address *Address) error

	FindAddressByID(ctx context.Context, id int) (*Address, error)
}
