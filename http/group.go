package http

import (
	"fmt"

	"eotl.supply/sextant"
	"github.com/kataras/iris/v12"
)

func (s *Server) registerGroupRoutes(app *iris.Application) {
	api := app.Party("/api/groups")
	{
		api.Use(iris.Compression)
		api.Get("/", s.handleGroupIndex)
		api.Post("/", s.handleGroupCreate)
		api.Get("/{group_id_or_slug}", s.handleGroupView)
		api.Delete("/{group_id_or_slug}", s.handleGroupDelete)
		// Internal redirect to contact resource
		api.Get("/{group_id_or_slug}/contacts", func(ctx iris.Context) {
			r := ctx.Request()
			r.URL.Path = "/api/contacts"
			r.URL.RawQuery = fmt.Sprintf("group=%s", ctx.Params().Get("group_id_or_slug"))
			s.handleContactAddressIndex(ctx)
		})
		// Internal redirect  to tour resource
		api.Get("/{group_id_or_slug}/tours", func(ctx iris.Context) {
			r := ctx.Request()
			r.URL.Path = "/api/tours"
			r.URL.RawQuery = fmt.Sprintf("group=%s", ctx.Params().Get("group_id_or_slug"))
			s.handleTourIndex(ctx)
		})
		api.Post("/{group_id_or_slug}/tours", func(ctx iris.Context) {
			r := ctx.Request()
			r.URL.Path = "/api/tours"
			r.URL.RawQuery = fmt.Sprintf("group=%s", ctx.Params().Get("group_id_or_slug"))
			s.handleTourCreate(ctx)
		})
		// Internal redirect to touritem resource
		api.Post("/{group_id_or_slug}/tours/{tour_id_or_slug}/items", func(ctx iris.Context) {
			r := ctx.Request()
			r.URL.Path = "/api/touritems"
			s.handleTourItemCreate(ctx)
		})
		api.Delete("/{group_id_or_slug}/tours/{tour_id_or_slug}/items/{touritem_id:int}", func(ctx iris.Context) {
			r := ctx.Request()
			r.URL.Path = "/api/touritems"
			s.handleTourItemDelete(ctx)
		})
		api.Patch("/{group_id_or_slug}/tours/{tour_id_or_slug}/items/{touritem_id:int}", func(ctx iris.Context) {
			r := ctx.Request()
			r.URL.Path = "/api/touritems"
			s.handleTourItemPatch(ctx)
		})
	}
}

func (s *Server) handleGroupIndex(ctx iris.Context) {
	groups, err := s.GroupService.FindAllGroups(ctx)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find any groups").DetailErr(err))
		return
	}
	// Prepare response
	resp := struct {
		Groups []sextant.Group `json:"groups"`
	}{Groups: groups}
	// Write response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

func (s *Server) handleGroupCreate(ctx iris.Context) {
	var group sextant.Group
	var err error

	if err = ctx.ReadJSON(&group); err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to read body").DetailErr(err))
		return
	}

	if err = s.GroupService.CreateGroup(ctx, &group); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to create group").DetailErr(err))
		return
	}

	resp := struct {
		Status int    `json:"status"`
		Title  string `json:"title"`
	}{201, "Group successfully created"}

	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

func (s *Server) handleGroupView(ctx iris.Context) {
	groupIDorSlug := ctx.Params().Get("group_id_or_slug")
	group, err := s.GroupService.FindGroupByIDorSlug(ctx, groupIDorSlug)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find group").DetailErr(err))
		return
	}
	// Write Response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(group); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

func (s *Server) handleGroupDelete(ctx iris.Context) {
	groupIDorSlug := ctx.Params().Get("group_id_or_slug")
	group, err := s.GroupService.FindGroupByIDorSlug(ctx, groupIDorSlug)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find group").DetailErr(err))
		return
	}
	// Execute
	if err = s.GroupService.DeleteGroup(ctx, *(&group.ID)); err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to delete group").DetailErr(err))
		return
	}
	// Prepare response
	resp := struct {
		Status int    `json:"status"`
		Title  string `json:"title"`
	}{iris.StatusNoContent, "Group successfully deleted"}
	// Write response
	ctx.StatusCode(resp.Status)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}
