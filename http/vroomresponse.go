package http

import (
	"eotl.supply/sextant"
	"github.com/kataras/iris/v12"
)

func (s *Server) registerVroomResponseRoutes(app *iris.Application) {
	api := app.Party("/api/vroom")
	{
		api.Use(iris.Compression)
		api.Post("/solver", s.handleVroomResponseCreate)
	}
}

func (s *Server) handleVroomResponseCreate(ctx iris.Context) {
	var tour sextant.Tour
	var err error

	err = ctx.ReadJSON(&tour)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to read body").DetailErr(err))
		return
	}

	// Prepare response payload
	vroomRequest := s.VroomService.CreateRequest(tour)

	// Prepare response payload
	vroomResponse, err := s.VroomService.Query(vroomRequest)
	if err != nil {
		ctx.StopWithProblem(iris.StatusNotFound, iris.NewProblem().Title("Failed solve vroom request").DetailErr(err))
		return
	}
	geojsonRoutes := s.GeojsonService.CreateFeatureCollection(vroomResponse.GetRoutes())

	resp := struct {
		sextant.VroomResponse            `json:"vroom"`
		sextant.GeojsonFeatureCollection `json:"routes"`
	}{*vroomResponse, *geojsonRoutes}

	// Write Response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}
