package http

import (
	"fmt"

	"eotl.supply/sextant"
	"github.com/kataras/iris/v12"
)

func (s *Server) registerTourItemRoutes(app *iris.Application) {
	api := app.Party("/api/touritems")
	{
		api.Use(iris.Compression)
		api.Post("/", s.handleTourItemCreate)
		api.Delete("/{touritem_id:int}", s.handleTourItemDelete)
		api.Patch("/{touritem_id:int}", s.handleTourItemPatch)
	}
}

func (s *Server) handleTourItemCreate(ctx iris.Context) {
	var group *sextant.Group
	var contact *sextant.Contact
	var tour *sextant.Tour
	var tourItem *sextant.TourItem
	var err error
	// Find Group
	if groupIDorSlug := ctx.Params().Get("group_id_or_slug"); len(groupIDorSlug) > 0 {
		group, err = s.GroupService.FindGroupByIDorSlug(ctx, groupIDorSlug)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find group").DetailErr(err))
			return
		}
	}
	// Find Contact
	if contactID, _ := ctx.Params().GetInt("contact_id"); contactID > 0 {
		if contact, err = s.ContactService.FindContactByID(ctx, contactID); err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find contact").DetailErr(err))
			return
		}
	}
	// Find Tour
	if tourIDorSlug := ctx.Params().Get("tour_id_or_slug"); len(tourIDorSlug) > 0 {
		tour, err = s.TourService.FindTourByIDorSlug(ctx, tourIDorSlug)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find tour").DetailErr(err))
			return
		}
	}
	// Check tour belongs to group
	if group != nil && tour != nil {
		found := false
		for _, g := range tour.Groups {
			if g.ID == group.ID {
				found = true
				break
			}
		}
		if !found {
			err := fmt.Errorf("tour does not belong to group")
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find tour").DetailErr(err))
			return
		}
	}
	// Check if contact is participant of tour
	if contact != nil && tour != nil {
		filter := sextant.ContactAddressFilter{ContactID: &contact.ID}
		contactaddresses, err := s.ContactAddressService.FindContactAddresses(ctx, filter)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find contact address for contact").DetailErr(err))
			return
		}
		if len(contactaddresses) < 1 {
			err := fmt.Errorf("could not find contactaddresses for contact")
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to check if contact is participant of tour").DetailErr(err))
			return
		}
	out:
		for _, caContact := range contactaddresses {
			for _, tourItem := range tour.Riders {
				if caContact.ID == tourItem.ContactAddressID {
					// Contact is participant (rider) of tour
					break out
				}
				// Contact is not participant of tour
				err := fmt.Errorf("contact is not participant of tour")
				ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Failed to delete tour item").DetailErr(err))
				return
			}
		}
	}
	if err = ctx.ReadJSON(&tourItem); err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to read body").DetailErr(err))
		return
	}
	// Check if new tour item belongs to correct tour
	if tour != nil && tour.ID != tourItem.TourID {
		err := fmt.Errorf("touritem does not belong to tour")
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to create tour item").DetailErr(err))
		return
	}
	if tourItem.TourItemtypeID < 1 {
		tit, err := s.TourItemtypeService.FindTourItemtypeByName(ctx, tourItem.TourItemtypeName)
		if err != nil {
			ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to find tour item type").DetailErr(err))
		}
		tourItem.TourItemtypeID = tit.ID
	}

	// Create TourItem
	if err = s.TourItemService.CreateTourItem(ctx, tourItem); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to create tour item").DetailErr(err))
		return
	}

	resp := struct {
		Status int    `json:"status"`
		Title  string `json:"title"`
		Detail string `json:"detail"`
	}{201, "TourItem successfully created", fmt.Sprintf("%d", tourItem.ID)}

	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

func (s *Server) handleTourItemDelete(ctx iris.Context) {
	var group *sextant.Group
	var tour *sextant.Tour
	var contact *sextant.Contact
	var err error
	// Find Group
	if groupIDorSlug := ctx.Params().Get("group_id_or_slug"); len(groupIDorSlug) > 0 {
		group, err = s.GroupService.FindGroupByIDorSlug(ctx, groupIDorSlug)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find group").DetailErr(err))
			return
		}
	}
	// Find Contact
	if contactID, _ := ctx.Params().GetInt("contact_id"); contactID > 0 {
		if contact, err = s.ContactService.FindContactByID(ctx, contactID); err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find contact").DetailErr(err))
			return
		}
	}
	// Find Tour
	if tourIDorSlug := ctx.Params().Get("tour_id_or_slug"); len(tourIDorSlug) > 0 {
		tour, err = s.TourService.FindTourByIDorSlug(ctx, tourIDorSlug)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find tour").DetailErr(err))
			return
		}
	}

	// Check tour belongs to group
	if group != nil && tour != nil {
		found := false
		for _, g := range tour.Groups {
			if g.ID == group.ID {
				found = true
				break
			}
		}
		if !found {
			err := fmt.Errorf("tour does not belong to group")
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find tour").DetailErr(err))
			return
		}
	}
	// Check if contact is participant of tour
	if contact != nil && tour != nil {
		filter := sextant.ContactAddressFilter{ContactID: &contact.ID}
		contactaddresses, err := s.ContactAddressService.FindContactAddresses(ctx, filter)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find contact address for contact").DetailErr(err))
			return
		}
		if len(contactaddresses) < 1 {
			err := fmt.Errorf("could not find contactaddresses for contact")
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to check if contact is participant of tour").DetailErr(err))
			return
		}
	out:
		for _, caContact := range contactaddresses {
			for _, tourItem := range tour.Riders {
				if caContact.ID == tourItem.ContactAddressID {
					// Contact is participant (rider) of tour
					break out
				}
				// Contact is not participant of tour
				err := fmt.Errorf("contact is not participant of tour")
				ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Failed to delete tour item").DetailErr(err))
				return
			}
		}
	}

	// Find TourItem
	tourItemID, err := ctx.Params().GetInt("touritem_id")
	if err != nil {
		ctx.StopWithProblem(iris.StatusNotFound, iris.NewProblem().Title("Unable to find tour item").DetailErr(err))
		return
	}
	tourItem, err := s.TourItemService.FindTourItemByID(ctx, tourItemID)
	if err != nil {
		ctx.StopWithProblem(iris.StatusNotFound, iris.NewProblem().Title("Unable to find tour item").DetailErr(err))
		return
	}
	// Check touritem belongs to tour
	if tour != nil && tourItem.TourID != tour.ID {
		err := fmt.Errorf("tourItem does not belong to tour")
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find tour item").DetailErr(err))
		return
	}
	// Execute
	if err = s.TourItemService.DeleteTourItem(ctx, tourItem.ID); err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to delete tour item").DetailErr(err))
		return
	}
	// Prepare response
	resp := struct {
		Status int    `json:"status"`
		Title  string `json:"title"`
	}{iris.StatusNoContent, "TourItem successfully deleted"}
	// Write response
	ctx.StatusCode(resp.Status)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

func (s *Server) handleTourItemPatch(ctx iris.Context) {
	var group *sextant.Group
	var contact *sextant.Contact
	var tour *sextant.Tour
	var upd sextant.TourItemUpdate
	var err error
	// Find Group
	if groupIDorSlug := ctx.Params().Get("group_id_or_slug"); len(groupIDorSlug) > 0 {
		group, err = s.GroupService.FindGroupByIDorSlug(ctx, groupIDorSlug)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find group").DetailErr(err))
			return
		}
	}
	// Find Contact
	if contactID, _ := ctx.Params().GetInt("contact_id"); contactID > 0 {
		if contact, err = s.ContactService.FindContactByID(ctx, contactID); err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find contact").DetailErr(err))
			return
		}
	}
	// Find Tour
	if tourIDorSlug := ctx.Params().Get("tour_id_or_slug"); len(tourIDorSlug) > 0 {
		tour, err = s.TourService.FindTourByIDorSlug(ctx, tourIDorSlug)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find tour").DetailErr(err))
			return
		}
	}

	// Check tour belongs to group
	if group != nil && tour != nil {
		found := false
		for _, g := range tour.Groups {
			if g.ID == group.ID {
				found = true
				break
			}
		}
		if !found {
			err := fmt.Errorf("tour does not belong to group")
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find tour").DetailErr(err))
			return
		}
	}
	// Check if contact is participant of tour
	if contact != nil && tour != nil {
		filter := sextant.ContactAddressFilter{ContactID: &contact.ID}
		contactaddresses, err := s.ContactAddressService.FindContactAddresses(ctx, filter)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find contact address for contact").DetailErr(err))
			return
		}
		if len(contactaddresses) < 1 {
			err := fmt.Errorf("could not find contactaddresses for contact")
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to check if contact is participant of tour").DetailErr(err))
			return
		}
	out:
		for _, caContact := range contactaddresses {
			for _, tourItem := range tour.Riders {
				if caContact.ID == tourItem.ContactAddressID {
					// Contact is participant (rider) of tour
					break out
				}
				// Contact is not participant of tour
				err := fmt.Errorf("contact is not participant of tour")
				ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Failed to delete tour item").DetailErr(err))
				return
			}
		}
	}

	// Find TourItem
	tourItemID, err := ctx.Params().GetInt("touritem_id")
	if err != nil {
		ctx.StopWithProblem(iris.StatusNotFound, iris.NewProblem().Title("Unable to find tour item").DetailErr(err))
		return
	}

	tourItem, err := s.TourItemService.FindTourItemByID(ctx, tourItemID)
	if err != nil {
		ctx.StopWithProblem(iris.StatusNotFound, iris.NewProblem().Title("Unable to find tour item").DetailErr(err))
		return
	}
	// Check touritem belongs to tour
	if tour != nil && tourItem.TourID != tour.ID {
		err := fmt.Errorf("tourItem does not belong to tour")
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find tour item").DetailErr(err))
		return
	}

	if err = ctx.ReadJSON(&upd); err != nil {
		fmt.Printf("Error: %s", err)
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to read body").DetailErr(err))
		return
	}

	_, err = s.TourItemService.UpdateTourItem(ctx, tourItemID, upd)
	if err != nil {
		ctx.StopWithProblem(iris.StatusNotFound, iris.NewProblem().Title("Unable to update tour item").DetailErr(err))
		return
	}

	// Prepare response
	resp := struct {
		Status int    `json:"status"`
		Title  string `json:"title"`
	}{iris.StatusOK, "TourItem successfully updated"}

	// Write response
	ctx.StatusCode(resp.Status)
	if err = ctx.JSON(resp); err != nil {
		fmt.Println(err)
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}
