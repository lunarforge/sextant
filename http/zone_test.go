package http_test

import (
	"context"
	"io"
	"net/http"
	"testing"

	"eotl.supply/sextant"
	//"github.com/gavv/httpexpect/v2"
)

func TestZoneApi(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Zone db lookup
	s.ZoneService.FindZoneByAbbrFn = func(ctx context.Context, abbr string) (*sextant.Zone, error) {
		return &sextant.Zone{ID: 1, Abbr: abbr}, nil
	}

	t.Run("ViewZone", func(t *testing.T) {
		resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", "/api/zones?id=BER", nil))
		defer resp.Body.Close()
		if err != nil {
			t.Fatal(err)
		}

		if got, want := resp.StatusCode, http.StatusOK; got != want {
			t.Fatalf("StatusCode=%v, want %v", got, want)
		}

		gotBody, err := io.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("Error reading resp.Body: %v", err)
		}

		wantBody, err := readFile("testdata/want/zone.json")
		if err != nil {
			t.Fatalf("Error reading file: %v", err)
		}

		JSONIsEqual(t, string(gotBody), wantBody)
	})
}
