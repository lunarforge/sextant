package http_test

import (
	"io/ioutil"
	"net/http"
	"os"
	"testing"

	"eotl.supply/sextant"
	"github.com/gavv/httpexpect/v2"
)

func TestAddress(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Mock LocationService.FetchLocation
	s.LocationService.FetchLocationFn = func(address string) (*sextant.Location, error) {
		return &sextant.Location{Lat: 1, Lon: 1, Words: "dwarf blood abandon abandon abandon"}, nil
	}

	e := httpexpect.New(t, s.URL())

	t.Run("CreateAddress", func(t *testing.T) {
		input := "Karl Marx Straße 66, 12043"
		if want, err := readFile("testdata/want/address.json"); err != nil {
			t.Fatal(err)
		} else {
			req := e.POST("/api/geocode/address")
			req.WithFormField("address", input)
			resp := req.Expect()
			resp.Status(http.StatusOK)
			JSONIsEqual(t, resp.Body().Raw(), want)
		}
	})

	t.Run("CreateAddresses", func(t *testing.T) {
		// Mock ContactAddressDecoder.DecodeContactAddresses()
		s.ContactAddressDecoder.DecodeContactAddressesFn = func(filepath string) ([]sextant.ContactAddress, error) {
			return []sextant.ContactAddress{
				sextant.ContactAddress{
					Contact: &sextant.Contact{Name: "Friedrich", Email: "f.e@marxism.org"},
					Address: &sextant.Address{Label: "Karl Marx Straße 66, 12043"},
					Details: "First Floor",
				},
				sextant.ContactAddress{
					Contact: &sextant.Contact{Name: "Nestor Mahkno", Email: "nestor@anarchistnews.org"},
					Address: &sextant.Address{Label: "35, Rue de la Bûcherie, Paris, 75005"},
				},
			}, nil
		}

		// Mock LocationService.UpdateLocations()
		s.LocationService.UpdateLocationsFn = func(people ...[]sextant.ContactAddress) []sextant.ContactAddress {
			for _, cas := range people {
				for _, ca := range cas {
					ca.Location = &sextant.Location{Lat: 1, Lon: 1, Words: "dwarf blood abandon abandon abandon"}
				}
			}
			return []sextant.ContactAddress{}
		}

		// Mock FileService.SetFirstLine()
		// returns a filepath to a tmp file containing addresses.csv
		s.FileService.SetFirstLineFn = func(filepath string, header string, action string) (string, error) {
			outputFile, err := os.CreateTemp("", "eotl-csv-")
			if err != nil {
				return "", err
			}
			bytesRead, err := ioutil.ReadFile("testdata/input/addresses.csv")
			if err != nil {
				return "", err
			}
			outputFilepath := outputFile.Name()
			err = ioutil.WriteFile(outputFilepath, bytesRead, 0644)
			if err != nil {
				return "", err
			}
			return outputFilepath, nil
		}

		t.Run("WithValidHeaderInFile", func(t *testing.T) {
			input := "testdata/input/addresses.csv"
			if want, err := readFile("testdata/want/addresses.json"); err != nil {
				t.Fatal(err)
			} else {
				req := e.POST("/api/geocode/addresses")
				req.WithMultipart().WithFile("file", input)
				resp := req.Expect()
				resp.Status(http.StatusOK)
				JSONIsEqual(t, resp.Body().Raw(), want)
			}
		})

		t.Run("CreateAddressesWithHeader", func(t *testing.T) {
			input := "testdata/input/addresses-badheader.csv"
			if want, err := readFile("testdata/want/addresses.json"); err != nil {
				t.Fatal(err)
			} else {
				req := e.POST("/api/geocode/addresses")
				req.WithMultipart().WithFile("file", input).WithFormField("header", "Name,Email,Address,AddressDetails,Phone,Amount,From,To")
				resp := req.Expect()
				resp.Status(http.StatusOK)
				JSONIsEqual(t, resp.Body().Raw(), want)
			}
		})
		t.Run("CreateAddressesWithHeaderPrepend", func(t *testing.T) {
			input := "testdata/input/addresses-noheader.csv"
			if want, err := readFile("testdata/want/addresses.json"); err != nil {
				t.Fatal(err)
			} else {
				req := e.POST("/api/geocode/addresses")
				req.WithMultipart().WithFile("file", input).WithFormField("header", "Name,Email,Address,AddressDetails,Phone,Amount,From,To").WithFormField("prepend", "true")
				resp := req.Expect()
				resp.Status(http.StatusOK)
				JSONIsEqual(t, resp.Body().Raw(), want)
			}
		})
	})
}
