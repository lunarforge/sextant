package http_test

import (
	"context"
	"io"
	"net/http"
	"strconv"
	"strings"
	"testing"

	"eotl.supply/sextant"
	"github.com/gavv/httpexpect/v2"
)

func TestGroupIndex(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Groups db lookup
	s.GroupService.FindAllGroupsFn = func(ctx context.Context) ([]sextant.Group, error) {
		group := sextant.Group{ID: 1, Name: "Tasty Taco", Slug: "tasty-taco"}
		return []sextant.Group{group}, nil
	}

	resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", "/api/groups/", nil))
	if err != nil {
		t.Fatal(err)
	}

	defer resp.Body.Close()

	if got, want := resp.StatusCode, http.StatusOK; got != want {
		t.Fatalf("StatusCode=%v, want %v", got, want)
	}

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Error reading resp.Body: %v", err)
	}

	eb, err := readFile("testdata/want/groups.json")
	if err != nil {
		t.Fatalf("Error reading file: %v", err)
	}

	JSONIsEqual(t, string(b), eb)
}

func TestGroupCreate(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Mock GroupService.CreateGroup
	s.GroupService.CreateGroupFn = func(ctx context.Context, ca *sextant.Group) error {
		return nil
	}

	e := httpexpect.New(t, s.URL())
	input := "testdata/input/group.json"

	req := e.POST("/api/groups")
	req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
	if inputData, err := readFile(input); err != nil {
		t.Fatal(err)
	} else {
		req.WithBytes([]byte(inputData))

		resp := req.Expect()
		resp.Status(http.StatusOK)
		obj := resp.JSON().Object()
		obj.Keys().ContainsOnly("status", "title")
		obj.Value("status").Number().Equal(201)
		obj.Value("title").String().Equal("Group successfully created")
	}
}

func TestGroupView(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Groups db lookup
	s.GroupService.FindGroupByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Group, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		} else {
			slug = "tasty-tacos"
		}
		return &sextant.Group{ID: id, Name: "Tasty Tacos", Slug: slug}, nil
	}

	tcs := []struct {
		name string
		path string
	}{
		{"WithGroupID", "/api/groups/1"},
		{"WithGroupSlug", "/api/groups/tasty-tacos"},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", tc.path, nil))
			if err != nil {
				t.Fatal(err)
			}

			defer resp.Body.Close()

			if got, want := resp.StatusCode, http.StatusOK; got != want {
				t.Fatalf("StatusCode=%v, want %v", got, want)
			}

			b, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("Error reading resp.Body: %v", err)
			}

			eb, err := readFile("testdata/want/group.json")
			if err != nil {
				t.Fatalf("Error reading file: %v", err)
			}

			JSONIsEqual(t, string(b), eb)
		})
	}
}

func TestGroupContactsIndex(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Groups db lookup
	s.GroupService.FindGroupByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Group, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		}
		return &sextant.Group{ID: id, Name: "Tasty Tacos", Slug: slug}, nil
	}
	// Mock ContactAddresses db lookup
	s.ContactAddressService.FindContactAddressesFn = func(ctx context.Context, filter sextant.ContactAddressFilter) ([]sextant.ContactAddress, error) {
		ca := sextant.ContactAddress{Details: "third floor"}
		return []sextant.ContactAddress{ca}, nil
	}
	// Setup testcases
	tcs := []struct {
		name string
		path string
	}{
		{"WithGroupID", "/api/groups/1/contacts"},
		{"WithGroupSlug", "/api/groups/tasty-tacos/contacts"},
	}
	// Execute testcases
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", tc.path, nil))
			if err != nil {
				t.Fatal(err)
			}

			defer resp.Body.Close()

			if got, want := resp.StatusCode, http.StatusOK; got != want {
				t.Fatalf("StatusCode=%v, want %v", got, want)
			}

			b, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("Error reading resp.Body: %v", err)
			}

			eb, err := readFile("testdata/want/contact_addresses.json")
			if err != nil {
				t.Fatalf("Error reading file: %v", err)
			}

			JSONIsEqual(t, string(b), eb)
		})
	}
}

func TestGroupToursIndex(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Groups db lookup
	s.GroupService.FindGroupByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Group, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		}
		return &sextant.Group{ID: id, Name: "Tasty Tacos", Slug: slug}, nil
	}
	// Mock Tours db lookup
	s.TourService.FindToursFn = func(ctx context.Context, filter sextant.TourFilter) ([]sextant.Tour, error) {
		tour := sextant.Tour{
			ID:   1,
			Name: "Northern Tour",
			Slug: "northern-tour",
		}
		return []sextant.Tour{tour}, nil
	}
	// Setup testcases
	tcs := []struct {
		name string
		path string
	}{
		{"WithGroupID", "/api/groups/1/tours"},
		{"WithGroupSlug", "/api/groups/tasty-tacos/tours"},
	}
	// Execute testcases
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", tc.path, nil))
			if err != nil {
				t.Fatal(err)
			}

			defer resp.Body.Close()

			if got, want := resp.StatusCode, http.StatusOK; got != want {
				t.Fatalf("StatusCode=%v, want %v", got, want)
			}
			b, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("Error reading resp.Body: %v", err)
			}

			eb, err := readFile("testdata/want/tours.json")
			if err != nil {
				t.Fatalf("Error reading file: %v", err)
			}

			JSONIsEqual(t, string(b), eb)
		})
	}
}

func TestDeleteGroup(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Groups db lookup
	s.GroupService.FindGroupByIDorSlugFn = func(ctx context.Context, slug string) (*sextant.Group, error) {
		group := sextant.Group{ID: 1, Name: "Tasty Taco", Slug: slug}
		return &group, nil
	}

	// Mock Groups db lookup
	s.GroupService.DeleteGroupFn = func(ctx context.Context, id int) error {
		return nil
	}

	resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "DELETE", "/api/groups/tasty-taco", nil))
	defer resp.Body.Close()

	if err != nil {
		t.Fatal(err)
	} else if got, want := resp.StatusCode, http.StatusNoContent; got != want {
		t.Fatalf("StatusCode=%v, want %v", got, want)
	}
}

func TestGroupTourTouritemCreate(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	group := &sextant.Group{Name: "Tasty Tacos"}

	// Mock Groups db lookup
	s.GroupService.FindGroupByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Group, error) {
		var err error
		if group.ID, err = strconv.Atoi(idOrSlug); err != nil {
			group.ID = 1
			group.Slug = idOrSlug
		} else {
			group.Slug = "tasty-tacos"
		}
		return group, nil
	}

	// Mock Tours db lookup
	s.TourService.FindTourByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Tour, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		} else {
			slug = "northern-tour"
		}
		tour := sextant.Tour{
			ID:         id,
			Name:       "Northern Tour",
			Slug:       slug,
			Deliveries: []sextant.TourItem{},
			Pickups:    []sextant.TourItem{},
			Riders:     []sextant.TourItem{},
			Groups:     []sextant.Group{*group},
		}
		return &tour, nil
	}

	// Mock TourItemtype db lookup
	s.TourItemtypeService.FindTourItemtypeByNameFn = func(ctx context.Context, name string) (*sextant.TourItemtype, error) {
		tit := &sextant.TourItemtype{ID: 1, Name: name}
		return tit, nil
	}

	// Mock TourItemService.CreateTourItem
	s.TourItemService.CreateTourItemFn = func(ctx context.Context, ti *sextant.TourItem) error {
		return nil
	}

	e := httpexpect.New(t, s.URL())
	input := "testdata/input/touritem.json"

	req := e.POST("/api/groups/tasty-tacos/tours/northern-tour/items")
	req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
	if inputData, err := readFile(input); err != nil {
		t.Fatal(err)
	} else {
		req.WithBytes([]byte(inputData))

		resp := req.Expect()
		resp.Status(http.StatusOK)
		obj := resp.JSON().Object()
		obj.Keys().ContainsOnly("status", "title", "detail")
		obj.Value("status").Number().Equal(201)
		obj.Value("title").String().Equal("TourItem successfully created")
	}
}

func TestGroupTourTouritemDelete(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()
	group := &sextant.Group{Name: "Tasty Tacos"}
	var tour *sextant.Tour

	// Mock Groups db lookup
	s.GroupService.FindGroupByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Group, error) {
		var err error
		if group.ID, err = strconv.Atoi(idOrSlug); err != nil {
			group.ID = 1
			group.Slug = idOrSlug
		} else {
			group.Slug = "tasty-tacos"
		}
		return group, nil
	}

	// Mock Tours db lookup
	s.TourService.FindTourByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Tour, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		} else {
			slug = "northern-tour"
		}
		tour = &sextant.Tour{
			ID:         id,
			Name:       "Northern Tour",
			Slug:       slug,
			Deliveries: []sextant.TourItem{},
			Pickups:    []sextant.TourItem{},
			Riders:     []sextant.TourItem{},
			Groups:     []sextant.Group{*group},
		}
		return tour, nil
	}

	// Mock TourItems db lookup
	s.TourItemService.FindTourItemByIDFn = func(ctx context.Context, id int) (*sextant.TourItem, error) {
		return &sextant.TourItem{ID: id, TourID: tour.ID}, nil
	}

	// Mock TourItems db lookup
	s.TourItemService.DeleteTourItemFn = func(ctx context.Context, id int) error {
		return nil
	}

	resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "DELETE", "/api/groups/tasty-tacos/tours/northern-tour/items/99", nil))
	defer resp.Body.Close()

	if err != nil {
		t.Fatal(err)
	} else if got, want := resp.StatusCode, http.StatusNoContent; got != want {
		t.Fatalf("StatusCode=%v, want %v", got, want)
	}
}

func TestGroupTourTouritemPatch(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()
	group := &sextant.Group{Name: "Tasty Tacos"}
	var tour *sextant.Tour

	// Mock Groups db lookup
	s.GroupService.FindGroupByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Group, error) {
		var err error
		if group.ID, err = strconv.Atoi(idOrSlug); err != nil {
			group.ID = 1
			group.Slug = idOrSlug
		} else {
			group.Slug = "tasty-tacos"
		}
		return group, nil
	}

	// Mock Tours db lookup
	s.TourService.FindTourByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Tour, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		} else {
			slug = "northern-tour"
		}
		tour = &sextant.Tour{
			ID:         id,
			Name:       "Northern Tour",
			Slug:       slug,
			Deliveries: []sextant.TourItem{},
			Pickups:    []sextant.TourItem{},
			Riders:     []sextant.TourItem{},
			Groups:     []sextant.Group{*group},
		}
		return tour, nil
	}

	// Mock TourItems db lookup
	s.TourItemService.FindTourItemByIDFn = func(ctx context.Context, id int) (*sextant.TourItem, error) {
		return &sextant.TourItem{ID: id, TourID: tour.ID}, nil
	}

	// Mock TourItems db lookup
	s.TourItemService.UpdateTourItemFn = func(ctx context.Context, id int, upd sextant.TourItemUpdate) (*sextant.TourItem, error) {
		return &sextant.TourItem{ID: id, State: *upd.State}, nil
	}

	body := strings.NewReader("{\"state\":\"new-state\"}")
	resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "PATCH", "/api/groups/tasty-tacos/tours/northern-tour/items/99", body))
	defer resp.Body.Close()

	if err != nil {
		t.Fatal(err)
	} else if got, want := resp.StatusCode, http.StatusOK; got != want {
		t.Fatalf("StatusCode=%v, want %v", got, want)
	}
}
