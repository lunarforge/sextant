package http

import (
	"strconv"

	"eotl.supply/sextant"
	"github.com/kataras/iris/v12"
)

func (s *Server) registerTourRoutes(app *iris.Application) {
	api := app.Party("/api/tours")
	{
		api.Use(iris.Compression)
		api.Get("/{id_or_slug}", s.handleTourView)
		api.Post("/", s.handleTourCreate)
		api.Get("/", s.handleTourIndex)
		api.Delete("/{id_or_slug}", s.handleTourDelete)
	}
}

func (s *Server) handleTourIndex(ctx iris.Context) {
	var group *sextant.Group
	var err error
	var contactIDint int

	groupIdOrSlug := ctx.URLParam("group")
	contactID := ctx.URLParam("contact_id")

	if groupIdOrSlug != "" {
		group, err = s.GroupService.FindGroupByIDorSlug(ctx, groupIdOrSlug)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find group").DetailErr(err))
			return
		}
	}

	filter := sextant.TourFilter{}
	if group != nil {
		filter.GroupID = &group.ID
	}
	if contactID != "" {
		if contactIDint, err = strconv.Atoi(contactID); err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find user").DetailErr(err))
			return
		}
		filter.ContactID = &contactIDint
	}

	tours, err := s.TourService.FindTours(ctx, filter)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find tours").DetailErr(err))
		return
	}
	// Prepare response
	resp := struct {
		Tours []sextant.Tour `json:"tours"`
	}{Tours: tours}
	// Write Response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

func (s *Server) handleTourView(ctx iris.Context) {
	idOrSlug := ctx.Params().Get("id_or_slug")
	tour, err := s.TourService.FindTourByIDorSlug(ctx, idOrSlug)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find tour").DetailErr(err))
		return
	}
	// Write Response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(tour); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

func (s *Server) handleTourCreate(ctx iris.Context) {
	var tour sextant.Tour
	var group *sextant.Group
	var err error

	if err = ctx.ReadJSON(&tour); err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to read body").DetailErr(err))
		return
	}

	// Find Group and append to Tour
	groupIDorSlug := ctx.URLParam("group")
	if groupIDorSlug != "" {
		group, err = s.GroupService.FindGroupByIDorSlug(ctx, groupIDorSlug)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find group").DetailErr(err))
			return
		}
		tour.Groups = append(tour.Groups, *group)
	}

	// Create Tour
	if err = s.TourService.CreateTour(ctx, &tour); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to create tour").DetailErr(err))
		return
	}

	resp := struct {
		Status int    `json:"status"`
		Title  string `json:"title"`
		Detail string `json:"detail"`
	}{201, "Tour successfully created", tour.Slug}

	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

func (s *Server) handleTourDelete(ctx iris.Context) {
	tourIDorSlug := ctx.Params().Get("id_or_slug")
	tour, err := s.TourService.FindTourByIDorSlug(ctx, tourIDorSlug)
	if err != nil {
		ctx.StopWithProblem(iris.StatusNotFound, iris.NewProblem().Title("Unable to find tour").DetailErr(err))
		return
	}
	// Execute
	if err = s.TourService.DeleteTour(ctx, *(&tour.ID)); err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to delete tour").DetailErr(err))
		return
	}
	// Prepare response
	resp := struct {
		Status int    `json:"status"`
		Title  string `json:"title"`
	}{iris.StatusNoContent, "Tour successfully deleted"}
	// Write response
	ctx.StatusCode(resp.Status)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}
