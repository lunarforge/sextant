package http

import (
	"context"
	"eotl.supply/sextant"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/accesslog"

	"fmt"
	"net"
	"time"
)

const DefaultPort = 8080
const MaxRequestBodySize = iris.MB
const ShutdownTimeout = 1 * time.Second

type Server struct {
	BeVerbose    bool
	HttpPort     int
	NominatimUrl string
	VroomUrl     string

	ContactAddressDecoder  sextant.ContactAddressDecoder
	ContactAddressService  sextant.ContactAddressService
	ContactService         sextant.ContactService
	FileService            sextant.FileService
	GeojsonService         sextant.GeojsonService
	GroupService           sextant.GroupService
	GroupMembershipService sextant.GroupMembershipService
	GroupTourItemService   sextant.GroupTourItemService
	LocationService        sextant.LocationService
	TourService            sextant.TourService
	TourItemService        sextant.TourItemService
	TourItemtypeService    sextant.TourItemtypeService
	VroomService           sextant.VroomService
	ZoneService            sextant.ZoneService

	ln  net.Listener
	app *iris.Application
}

func NewServer() *Server {
	s := &Server{}
	s.HttpPort = DefaultPort
	return s
}

// Returns Listener port or HttpPort
func (s *Server) Port() int {
	if s.ln == nil {
		return s.HttpPort
	}
	return s.ln.Addr().(*net.TCPAddr).Port
}

func (s *Server) URL() string {
	scheme, port, domain := "http", s.Port(), "localhost"
	return fmt.Sprintf("%s://%s:%d", scheme, domain, port)
}

func (s *Server) UseRandomPort() {
	s.HttpPort = 0
}

func (s *Server) Open() (err error) {
	// Create and configure iris app
	s.app = iris.New()

	crs := func(ctx iris.Context) {
		ctx.Header("Access-Control-Allow-Origin", "*")
		ctx.Header("Access-Control-Allow-Credentials", "true")
		if ctx.Method() == iris.MethodOptions {
			ctx.Header("Access-Control-Methods",
				"GET, POST, PUT, PATCH, DELETE")

			ctx.Header("Access-Control-Allow-Headers",
				"Access-Control-Allow-Origin,Content-Type")

			ctx.Header("Access-Control-Max-Age", "86400")
			ctx.StatusCode(iris.StatusNoContent)
			return
		}

		ctx.Next()
	}
	s.app.UseRouter(crs)

	if s.BeVerbose {
		ac := s.makeAccessLog()
		defer ac.Close()
		s.app.UseRouter(ac.Handler)
	}

	s.registerAddressRoutes(s.app)
	s.registerContactAddressRoutes(s.app)
	s.registerContactRoutes(s.app)
	s.registerGroupRoutes(s.app)
	s.registerStatusRoutes(s.app)
	s.registerTourRoutes(s.app)
	s.registerTourItemRoutes(s.app)
	s.registerVroomRequestRoutes(s.app)
	s.registerVroomResponseRoutes(s.app)
	s.registerZoneRoutes(s.app)

	// Serve HTML UI
	s.app.HandleDir("/", "../../www/dist")

	if !s.BeVerbose {
		s.app = s.app.Configure(iris.WithoutBanner, iris.WithoutServerError(iris.ErrServerClosed))
	}

	if s.ln, err = net.Listen("tcp4", fmt.Sprintf(":%d", s.Port())); err != nil {
		return err
	}
	// Start Iris
	if s.BeVerbose {
		// NOTE: in verbose mode we need to run this synchronously
		// otherwise logging to stdout is not displayed
		s.app.Run(iris.Listener(s.ln))
	} else {
		go s.app.Run(iris.Listener(s.ln))
	}

	return nil
}

// Close gracefully shuts down the server.
func (s *Server) Close() error {
	ctx, cancel := context.WithTimeout(context.Background(), ShutdownTimeout)
	defer cancel()
	return s.app.Shutdown(ctx)
}

func (s *Server) makeAccessLog() (ac *accesslog.AccessLog) {
	ac = accesslog.New(nil)
	// The default configuration:
	ac.Delim = '|'
	ac.TimeFormat = "01/Jan/2006:13:04:05 -0700"
	ac.Async = false
	ac.IP = true
	ac.BytesReceivedBody = true
	ac.BytesSentBody = true
	ac.BytesReceived = false
	ac.BytesSent = false
	ac.BodyMinify = true
	ac.RequestBody = true
	ac.ResponseBody = false
	ac.KeepMultiLineError = true
	ac.PanicLog = accesslog.LogHandler

	// Format as common log format (CLF) plus "latency" field
	ac.SetFormatter(&accesslog.Template{
		Text: "{{.IP}} - - [{{.Now.Format .TimeFormat}}] \"{{.Method}} {{.Path}} {{.RequestValuesLine}}\" {{.Code}} {{.BytesSentLine}} {{.Latency}}\n",
	})

	return ac
}
