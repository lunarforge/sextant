package http_test

import (
	"context"
	"io"
	"net/http"
	"strconv"
	"testing"

	"eotl.supply/sextant"
	"github.com/gavv/httpexpect/v2"
)

func TestContactView(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock db lookup
	s.ContactService.FindContactByIDFn = func(ctx context.Context, id int) (*sextant.Contact, error) {
		contact := sextant.Contact{ID: id, Name: "Sam"}
		return &contact, nil
	}

	resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", "/api/users/99", nil))
	if err != nil {
		t.Fatal(err)
	}

	defer resp.Body.Close()

	if got, want := resp.StatusCode, http.StatusOK; got != want {
		t.Fatalf("StatusCode=%v, want %v", got, want)
	}

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Error reading resp.Body: %v", err)
	}

	eb, err := readFile("testdata/want/contact.json")
	if err != nil {
		t.Fatalf("Error reading file: %v", err)
	}

	JSONIsEqual(t, string(b), eb)

}

func TestContactToursIndex(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Contacts db lookup
	s.ContactService.FindContactByIDFn = func(ctx context.Context, id int) (*sextant.Contact, error) {
		return &sextant.Contact{ID: id, Name: "Joe"}, nil
	}
	// Mock Tours db lookup
	s.TourService.FindToursFn = func(ctx context.Context, filter sextant.TourFilter) ([]sextant.Tour, error) {
		tour := sextant.Tour{
			ID:   1,
			Name: "Northern Tour",
			Slug: "northern-tour",
		}
		return []sextant.Tour{tour}, nil
	}
	// Setup testcases
	tcs := []struct {
		name string
		path string
	}{
		{"WithContactID", "/api/users/1/tours"},
	}
	// Execute testcases
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", tc.path, nil))
			if err != nil {
				t.Fatal(err)
			}

			defer resp.Body.Close()

			if got, want := resp.StatusCode, http.StatusOK; got != want {
				t.Fatalf("StatusCode=%v, want %v", got, want)
			}

			b, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("Error reading resp.Body: %v", err)
			}

			eb, err := readFile("testdata/want/tours.json")
			if err != nil {
				t.Fatalf("Error reading file: %v", err)
			}

			JSONIsEqual(t, string(b), eb)
		})
	}
}

func TestContactContactsIndex(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Contacts db lookup
	s.ContactService.FindContactByIDFn = func(ctx context.Context, id int) (*sextant.Contact, error) {
		return &sextant.Contact{ID: id, Name: "Joe"}, nil
	}
	// Mock Tours db lookup
	s.TourService.FindToursFn = func(ctx context.Context, filter sextant.TourFilter) ([]sextant.Tour, error) {
		tour := sextant.Tour{ID: 1, Name: "Northern Tour", Slug: "northern-tour"}
		return []sextant.Tour{tour}, nil
	}
	// Mock ContactAddress db lookup
	s.ContactAddressService.FindContactAddressesFn = func(ctx context.Context, filter sextant.ContactAddressFilter) ([]sextant.ContactAddress, error) {
		ca := sextant.ContactAddress{Details: "third floor"}
		return []sextant.ContactAddress{ca}, nil
	}

	// Setup testcases
	tcs := []struct {
		name string
		path string
	}{
		{"WithContactID", "/api/users/1/contacts"},
	}
	// Execute testcases
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", tc.path, nil))
			if err != nil {
				t.Fatal(err)
			}

			defer resp.Body.Close()

			if got, want := resp.StatusCode, http.StatusOK; got != want {
				t.Fatalf("StatusCode=%v, want %v", got, want)
			}

			b, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("Error reading resp.Body: %v", err)
			}

			eb, err := readFile("testdata/want/contact_addresses.json")
			if err != nil {
				t.Fatalf("Error reading file: %v", err)
			}

			JSONIsEqual(t, string(b), eb)

		})
	}
}
func TestContactTourTouritemCreate(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	contact := &sextant.Contact{Name: "Joe"}
	contactaddress := &sextant.ContactAddress{}
	var tour *sextant.Tour

	// Mock Contact db lookup
	s.ContactService.FindContactByIDFn = func(ctx context.Context, id int) (*sextant.Contact, error) {
		contact.ID = id
		return contact, nil
	}

	// Mock ContactAddresses db lookup
	s.ContactAddressService.FindContactAddressesFn = func(ctx context.Context, filter sextant.ContactAddressFilter) ([]sextant.ContactAddress, error) {
		contactaddress.ContactID = *filter.ContactID
		return []sextant.ContactAddress{*contactaddress}, nil
	}

	// Mock Tours db lookup
	s.TourService.FindTourByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Tour, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		} else {
			slug = "northern-tour"
		}
		tour = &sextant.Tour{
			ID:         id,
			Name:       "Northern Tour",
			Slug:       slug,
			Deliveries: []sextant.TourItem{},
			Pickups:    []sextant.TourItem{},
			Riders:     []sextant.TourItem{sextant.TourItem{ContactAddressID: contactaddress.ID}},
		}
		return tour, nil
	}

	// Mock TourItemtype db lookup
	s.TourItemtypeService.FindTourItemtypeByNameFn = func(ctx context.Context, name string) (*sextant.TourItemtype, error) {
		tit := &sextant.TourItemtype{ID: 1, Name: name}
		return tit, nil
	}

	// Mock TourItemService.CreateTourItem
	s.TourItemService.CreateTourItemFn = func(ctx context.Context, ti *sextant.TourItem) error {
		return nil
	}

	e := httpexpect.New(t, s.URL())
	input := "testdata/input/touritem.json"

	req := e.POST("/api/users/1/tours/northern-tour/items")
	req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
	if inputData, err := readFile(input); err != nil {
		t.Fatal(err)
	} else {
		req.WithBytes([]byte(inputData))

		resp := req.Expect()
		resp.Status(http.StatusOK)
		obj := resp.JSON().Object()
		obj.Keys().ContainsOnly("status", "title", "detail")
		obj.Value("status").Number().Equal(201)
		obj.Value("title").String().Equal("TourItem successfully created")
	}
}

func TestContactTourTouritemDelete(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()
	contact := &sextant.Contact{Name: "Joe"}
	contactaddress := &sextant.ContactAddress{}
	var tour *sextant.Tour

	// Mock Contact db lookup
	s.ContactService.FindContactByIDFn = func(ctx context.Context, id int) (*sextant.Contact, error) {
		contact.ID = id
		return contact, nil
	}

	// Mock ContactAddresses db lookup
	s.ContactAddressService.FindContactAddressesFn = func(ctx context.Context, filter sextant.ContactAddressFilter) ([]sextant.ContactAddress, error) {
		contactaddress.ContactID = *filter.ContactID
		return []sextant.ContactAddress{*contactaddress}, nil
	}

	// Mock Tours db lookup
	s.TourService.FindTourByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Tour, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		} else {
			slug = "northern-tour"
		}
		tour = &sextant.Tour{
			ID:         id,
			Name:       "Northern Tour",
			Slug:       slug,
			Deliveries: []sextant.TourItem{},
			Pickups:    []sextant.TourItem{},
			Riders:     []sextant.TourItem{sextant.TourItem{ContactAddressID: contactaddress.ID}},
		}
		return tour, nil
	}

	// Mock TourItems db lookup
	s.TourItemService.FindTourItemByIDFn = func(ctx context.Context, id int) (*sextant.TourItem, error) {
		return &sextant.TourItem{ID: id, TourID: tour.ID}, nil
	}

	// Mock TourItems db lookup
	s.TourItemService.DeleteTourItemFn = func(ctx context.Context, id int) error {
		return nil
	}

	resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "DELETE", "/api/users/1/tours/northern-tour/items/99", nil))
	defer resp.Body.Close()

	if err != nil {
		t.Fatal(err)
	} else if got, want := resp.StatusCode, http.StatusNoContent; got != want {
		t.Fatalf("StatusCode=%v, want %v", got, want)
	}
}
