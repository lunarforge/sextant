package http

import (
	//"eotl.supply/sextant"

	"github.com/kataras/iris/v12"
)

type Status struct {
	Name    string `json:"name"`
	Status  string `json:"status"`
	Version string `json:"version"`
	UI      string `json:"ui"`
}

func (s *Server) registerStatusRoutes(app *iris.Application) {
	api := app.Party("/api")
	{
		api.Use(iris.Compression)
		api.Get("/", s.handleStatusView)
	}
}

func (s *Server) handleStatusView(ctx iris.Context) {
	var status Status
	status.Name = "Sextant API"
	status.Status = "healthy"
	status.Version = "0.1.0"
	status.UI = "0.1.0"

	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(status)
}
