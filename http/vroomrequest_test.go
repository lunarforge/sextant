package http_test

import (
	"net/http"
	"testing"

	"eotl.supply/sextant"
	"github.com/gavv/httpexpect/v2"
)

func TestVroomRequest(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Mock VroomService.CreateRequest()
	s.VroomService.CreateRequestFn = func(tour sextant.Tour) *sextant.VroomRequest {
		return &sextant.VroomRequest{}
	}

	e := httpexpect.New(t, s.URL())

	t.Run("CreateVroomRequest", func(t *testing.T) {
		input := "testdata/input/tour.json"
		want := "testdata/want/vroomrequest.json"

		req := e.POST("/api/vroom")
		req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
		if inputData, err := readFile(input); err != nil {
			t.Fatal(err)
		} else if want, err := readFile(want); err != nil {
			t.Fatal(err)
		} else {
			req.WithBytes([]byte(inputData))
			resp := req.Expect()
			resp.Status(http.StatusOK)
			JSONIsEqual(t, resp.Body().Raw(), want)
		}

		t.Run("WithInvalidJson", func(t *testing.T) {
			input := "{invalid_json}"
			want := "testdata/want/invalid_json_error.json"

			if wantData, err := readFile(want); err != nil {
				t.Fatal(err)
			} else {
				req := e.POST("/api/vroom")
				req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
				req.WithBytes([]byte(input))
				resp := req.Expect()
				resp.Status(http.StatusBadRequest)
				JSONIsEqual(t, resp.Body().Raw(), wantData)
			}
		})
	})
}
