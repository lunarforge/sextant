package http

import (
	"eotl.supply/sextant"
	"github.com/kataras/iris/v12"
)

func (s *Server) registerVroomRequestRoutes(app *iris.Application) {
	api := app.Party("/api/vroom")
	{
		api.Use(iris.Compression)
		api.Post("/", s.handleVroomRequestCreate)
	}
}

func (s *Server) handleVroomRequestCreate(ctx iris.Context) {
	var tour sextant.Tour
	var err error

	err = ctx.ReadJSON(&tour)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to read body").DetailErr(err))
		return
	}

	// Prepare response payload
	vroomRequest := s.VroomService.CreateRequest(tour)

	// Write Response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(vroomRequest); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}
