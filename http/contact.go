package http

import (
	"fmt"
	"strconv"

	"github.com/kataras/iris/v12"
)

func (s *Server) registerContactRoutes(app *iris.Application) {
	api := app.Party("/api/users")
	{
		api.Use(iris.Compression)
		api.Get("/{contact_id}", s.handleContactView)
		api.Get("/{contact_id}/tours", func(ctx iris.Context) {
			r := ctx.Request()
			r.URL.Path = "/api/tours"
			r.URL.RawQuery = fmt.Sprintf("contact_id=%s", ctx.Params().Get("contact_id"))
			s.handleTourIndex(ctx)
		})
		api.Get("/{contact_id}/contacts", func(ctx iris.Context) {
			r := ctx.Request()
			r.URL.Path = "/api/contacts"
			r.URL.RawQuery = fmt.Sprintf("contact_id=%s", ctx.Params().Get("contact_id"))
			s.handleContactAddressIndex(ctx)
		})
		api.Post("/{contact_id:int}/tours/{tour_id_or_slug}/items", func(ctx iris.Context) {
			r := ctx.Request()
			r.URL.Path = "/api/touritems"
			s.handleTourItemCreate(ctx)
		})
		api.Delete("/{contact_id:int}/tours/{tour_id_or_slug}/items/{touritem_id:int}", func(ctx iris.Context) {
			r := ctx.Request()
			r.URL.Path = fmt.Sprintf("/api/touritems/%s", ctx.Params().Get("touritem_id"))
			s.handleTourItemDelete(ctx)
		})
	}
}

func (s *Server) handleContactView(ctx iris.Context) {
	var id int
	var err error
	contactID := ctx.Params().Get("contact_id")
	if id, err = strconv.Atoi(contactID); err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find user").DetailErr(err))
		return
	}
	contact, err := s.ContactService.FindContactByID(ctx, id)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find user").DetailErr(err))
		return
	}
	// Write Response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(contact); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}
