package http

import (
	//"eotl.supply/sextant"

	"github.com/kataras/iris/v12"
)

func (s *Server) registerZoneRoutes(app *iris.Application) {
	api := app.Party("/api/zones")
	{
		api.Use(iris.Compression)
		api.Get("/", s.handleZoneView)
	}
}

func (s *Server) handleZoneView(ctx iris.Context) {
	abbr := ctx.URLParam("id")
	zone, err := s.ZoneService.FindZoneByAbbr(ctx, abbr)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to find zone").DetailErr(err))
		return
	}

	// Prepare response
	ctx.StatusCode(iris.StatusOK)
	fc := zone.ToFeatureCollection()
	if err = ctx.JSON(fc); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}
