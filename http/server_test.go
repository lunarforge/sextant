package http_test

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"testing"

	sextanthttp "eotl.supply/sextant/http"
	"eotl.supply/sextant/mock"
	"github.com/stretchr/testify/assert"
)

type Server struct {
	*sextanthttp.Server

	// Mock Services
	ContactAddressDecoder  mock.ContactAddressDecoder
	ContactAddressService  mock.ContactAddressService
	ContactService         mock.ContactService
	FileService            mock.FileService
	GeojsonService         mock.GeojsonService
	GroupService           mock.GroupService
	GroupMembershipService mock.GroupMembershipService
	GroupTourItemService   mock.GroupTourItemService
	LocationService        mock.LocationService
	TourService            mock.TourService
	TourItemService        mock.TourItemService
	TourItemtypeService    mock.TourItemtypeService
	VroomService           mock.VroomService
	ZoneService            mock.ZoneService
}

// MustOpenServer is a test helper function for starting a new test HTTP server.
// Fail on error.
func MustOpenServer(tb testing.TB) *Server {
	tb.Helper()

	s := &Server{Server: sextanthttp.NewServer()}
	s.Server.UseRandomPort()

	// Assign mocks to actual server services
	s.Server.ContactAddressDecoder = &s.ContactAddressDecoder
	s.Server.ContactAddressService = &s.ContactAddressService
	s.Server.ContactService = &s.ContactService
	s.Server.FileService = &s.FileService
	s.Server.GeojsonService = &s.GeojsonService
	s.Server.GroupService = &s.GroupService
	s.Server.GroupMembershipService = &s.GroupMembershipService
	s.Server.GroupTourItemService = &s.GroupTourItemService
	s.Server.LocationService = &s.LocationService
	s.Server.TourService = &s.TourService
	s.Server.TourItemService = &s.TourItemService
	s.Server.TourItemtypeService = &s.TourItemtypeService
	s.Server.VroomService = &s.VroomService
	s.Server.ZoneService = &s.ZoneService

	if err := s.Open(); err != nil {
		tb.Fatal(err)
	}
	return s
}

// MustCloseServer is a test helper function for shutting down the server.
// Fail on error.
func MustCloseServer(tb testing.TB, s *Server) {
	tb.Helper()
	if err := s.Close(); err != nil {
		tb.Fatal(err)
	}
}

// MustNewRequest creates a new HTTP request using the server's base URL and
func (s *Server) MustNewRequest(tb testing.TB, ctx context.Context, method, url string, body io.Reader) *http.Request {
	tb.Helper()
	// Create new net/http request with server's base URL.
	r, err := http.NewRequest(method, s.URL()+url, body)
	if err != nil {
		tb.Fatal(err)
	}
	return r
}

func readFile(path string) (string, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

// JSONIsEqual ensures a response body and a loaded testdata JSON file are
// equal. We use json.Compact to avoid any inconsistencies on both sides.
func JSONIsEqual(t *testing.T, body, want string) {
	bodyCompact := &bytes.Buffer{}
	if err := json.Compact(bodyCompact, []byte(body)); err != nil {
		t.Fatal(err)
	}

	wantCompact := &bytes.Buffer{}
	if err := json.Compact(wantCompact, []byte(want)); err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, bodyCompact, wantCompact)
}
