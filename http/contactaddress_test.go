package http_test

import (
	"context"
	"io"
	"net/http"
	"strconv"
	"testing"

	"eotl.supply/sextant"
	"github.com/gavv/httpexpect/v2"
)

func TestContactAddressIndex(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Group db lookup
	s.GroupService.FindGroupByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Group, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		}
		return &sextant.Group{ID: id, Slug: slug}, nil
	}
	// Mock Contact db lookup
	s.ContactService.FindContactByIDFn = func(ctx context.Context, id int) (*sextant.Contact, error) {
		return &sextant.Contact{ID: id, Name: "Joe"}, nil
	}
	// Mock Tour db lookup
	s.TourService.FindToursFn = func(ctx context.Context, filter sextant.TourFilter) ([]sextant.Tour, error) {
		return []sextant.Tour{sextant.Tour{ID: 1, Name: "Northern Tour"}}, nil
	}
	// Mock Zone db lookup
	s.ZoneService.FindZoneByAbbrFn = func(ctx context.Context, abbr string) (*sextant.Zone, error) {
		return &sextant.Zone{Abbr: abbr}, nil
	}
	// Mock ContactAddresses db lookup
	s.ContactAddressService.FindContactAddressesFn = func(ctx context.Context, filter sextant.ContactAddressFilter) ([]sextant.ContactAddress, error) {
		ca := sextant.ContactAddress{Details: "third floor"}
		return []sextant.ContactAddress{ca}, nil
	}

	tcs := []struct {
		name string
		path string
	}{
		{"WithGroupID", "/api/contacts?group=99"},
		{"WithGroupSlug", "/api/contacts?group=superfood"},
		{"WithZoneAbbr", "/api/contacts?zone=NKN"},
		{"WithContactID", "/api/contacts?contact_id=1"},
	}
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", tc.path, nil))
			if err != nil {
				t.Fatal(err)
			}

			defer resp.Body.Close()

			if got, want := resp.StatusCode, http.StatusOK; got != want {
				t.Fatalf("StatusCode=%v, want %v", got, want)
			}

			b, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("Error reading resp.Body: %v", err)
			}

			eb, err := readFile("testdata/want/contact_addresses.json")
			if err != nil {
				t.Fatalf("Error reading file: %v", err)
			}

			JSONIsEqual(t, string(b), eb)
		})
	}
}

func TestCreateContactAddresses(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	//ctx := context.Background()

	// Mock LocationService.FetchLocation
	s.LocationService.FetchLocationFn = func(address string) (*sextant.Location, error) {
		return &sextant.Location{Lat: 1, Lon: 1, Words: "dwarf blood abandon abandon abandon"}, nil
	}

	// Mock ContactAddressService.FindOrCreateContactAddress
	s.ContactAddressService.FindOrCreateContactAddressFn = func(ctx context.Context, ca *sextant.ContactAddress) error {
		return nil
	}

	e := httpexpect.New(t, s.URL())
	input := "testdata/input/contactaddresses.json"

	req := e.POST("/api/contacts")
	req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
	if inputData, err := readFile(input); err != nil {
		t.Fatal(err)
	} else {
		req.WithBytes([]byte(inputData))

		resp := req.Expect()
		resp.Status(http.StatusOK)
		obj := resp.JSON().Object()
		obj.Keys().ContainsOnly("status", "title")
		obj.Value("status").Number().Equal(201)
		obj.Value("title").String().Equal("Contacts added successfully")
	}

	t.Run("WithGroup", func(t *testing.T) {
		// Mock GroupService.FindGroupByName()
		s.GroupService.FindGroupByNameFn = func(ctx context.Context, name string) (*sextant.Group, error) {
			return &sextant.Group{Name: name}, nil
		}
		// Mock GroupMembershipService.FindOrCreateGroupMembership()
		s.GroupMembershipService.FindOrCreateGroupMembershipFn = func(ctx context.Context, ca *sextant.GroupMembership) error {
			return nil
		}

		input := "testdata/input/contactaddresses-withgroup.json"

		req := e.POST("/api/contacts")
		req.WithHeader("Content-Type", "application/x-www-form-urlencoded")

		if inputData, err := readFile(input); err != nil {
			t.Fatal(err)
		} else {
			req.WithBytes([]byte(inputData))

			resp := req.Expect()
			resp.Status(http.StatusOK)
		}
	})
}
