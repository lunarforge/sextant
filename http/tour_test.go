package http_test

import (
	"context"
	"io"
	"net/http"
	"strconv"
	"testing"

	"eotl.supply/sextant"
	"github.com/gavv/httpexpect/v2"
)

func TestFindAllTours(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Tours db lookup
	s.TourService.FindToursFn = func(ctx context.Context, filter sextant.TourFilter) ([]sextant.Tour, error) {
		tour := sextant.Tour{ID: 1, Name: "Northern Tour", Slug: "northern-tour"}
		return []sextant.Tour{tour}, nil
	}

	resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", "/api/tours/", nil))
	if err != nil {
		t.Fatal(err)
	}

	defer resp.Body.Close()

	if got, want := resp.StatusCode, http.StatusOK; got != want {
		t.Fatalf("StatusCode=%v, want %v", got, want)
	}

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Error reading resp.Body: %v", err)
	}

	eb, err := readFile("testdata/want/tours.json")
	if err != nil {
		t.Fatalf("Error reading file: %v", err)
	}

	JSONIsEqual(t, string(b), eb)
}

func TestTourCreate(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)

	// Mock TourService.CreateTour
	s.TourService.CreateTourFn = func(ctx context.Context, ca *sextant.Tour) error {
		return nil
	}

	e := httpexpect.New(t, s.URL())
	input := "testdata/input/tour.json"

	req := e.POST("/api/tours")
	req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
	if inputData, err := readFile(input); err != nil {
		t.Fatal(err)
	} else {
		req.WithBytes([]byte(inputData))

		resp := req.Expect()
		resp.Status(http.StatusOK)
		obj := resp.JSON().Object()
		obj.Keys().ContainsOnly("status", "title", "detail")
		obj.Value("status").Number().Equal(201)
		obj.Value("title").String().Equal("Tour successfully created")
	}

	t.Run("WithGroup", func(t *testing.T) {
		// Mock Group db lookup
		s.GroupService.FindGroupByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Group, error) {
			return &sextant.Group{Name: "Tasty Tacos"}, nil
		}
		// Mock GroupTourItem db creation
		s.GroupTourItemService.FindOrCreateGroupTourItemFn = func(ctx context.Context, item *sextant.GroupTourItem) error {
			return nil
		}

		req := e.POST("/api/tours")
		req.WithQuery("group", "tasty-tacos")
		//req.WithForm("group", "tasty-tacos")
		//req.WithForm(map[string]interface{}{"group": "tasty-tacos"})
		req.WithHeader("Content-Type", "application/x-www-form-urlencoded")
		if inputData, err := readFile(input); err != nil {
			t.Fatal(err)
		} else {
			req.WithBytes([]byte(inputData))

			resp := req.Expect()
			resp.Status(http.StatusOK)
			obj := resp.JSON().Object()
			obj.Keys().ContainsOnly("status", "title", "detail")
			obj.Value("status").Number().Equal(201)
			obj.Value("title").String().Equal("Tour successfully created")
		}
	})
}

func TestTourView(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Tours db lookup
	s.TourService.FindTourByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Tour, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		} else {
			slug = "northern-tour"
		}
		tour := sextant.Tour{
			ID:         id,
			Name:       "Northern Tour",
			Slug:       slug,
			Deliveries: []sextant.TourItem{},
			Pickups:    []sextant.TourItem{},
			Riders:     []sextant.TourItem{},
		}
		return &tour, nil
	}

	tcs := []struct {
		name string
		path string
	}{
		{"WithGroupID", "/api/tours/1"},
		{"WithGroupSlug", "/api/tours/northern-tour"},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "GET", tc.path, nil))
			defer resp.Body.Close()
			if err != nil {
				t.Fatal(err)
			}

			if got, want := resp.StatusCode, http.StatusOK; got != want {
				t.Fatalf("StatusCode=%v, want %v", got, want)
			}

			b, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("Error reading resp.Body: %v", err)
			}

			eb, err := readFile("testdata/want/tour.json")
			if err != nil {
				t.Fatalf("Error reading file: %v", err)
			}

			JSONIsEqual(t, string(b), eb)
		})
	}
}

func TestTourDelete(t *testing.T) {
	s := MustOpenServer(t)
	defer MustCloseServer(t, s)
	ctx := context.Background()

	// Mock Tours db lookup
	s.TourService.FindTourByIDorSlugFn = func(ctx context.Context, idOrSlug string) (*sextant.Tour, error) {
		var id int
		var slug string
		var err error
		if id, err = strconv.Atoi(idOrSlug); err != nil {
			id = 1
			slug = idOrSlug
		} else {
			slug = "northern-tour"
		}
		tour := sextant.Tour{ID: id, Name: "Northern Tour", Slug: slug}
		return &tour, nil
	}

	// Mock Tours db lookup
	s.TourService.DeleteTourFn = func(ctx context.Context, id int) error {
		return nil
	}

	resp, err := http.DefaultClient.Do(s.MustNewRequest(t, ctx, "DELETE", "/api/tours/tasty-taco", nil))
	defer resp.Body.Close()

	if err != nil {
		t.Fatal(err)
	} else if got, want := resp.StatusCode, http.StatusNoContent; got != want {
		t.Fatalf("StatusCode=%v, want %v", got, want)
	}
}
