package http

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"eotl.supply/sextant"
	"github.com/kataras/iris/v12"
)

func (s *Server) registerAddressRoutes(app *iris.Application) {
	addressAPI := app.Party("/api/geocode/address")
	{
		addressAPI.Use(iris.Compression)
		addressAPI.Post("/", s.handleAddressCreate)
	}
	addressesAPI := app.Party("/api/geocode/addresses")
	{
		addressesAPI.Use(iris.Compression)
		addressesAPI.Post("/", s.handleAddressesCreate)
	}
}

func (s *Server) handleAddressCreate(ctx iris.Context) {
	var label string
	var err error

	// Read address label from request
	label = ctx.PostValue("address")
	if len(label) == 0 {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Mandatory field 'address' missing in post request"))
		return
	}

	// Prepare response payload
	address := sextant.Address{Label: label}
	address.Location, err = s.LocationService.FetchLocation(address.Label)

	if err != nil {
		ctx.StopWithProblem(iris.StatusNotFound, iris.NewProblem().Title("Failed to fetch location").DetailErr(err))
		return
	}

	// Write response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(address); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}

// NOTE: returns ContactAddresses in payload, breaking REST
func (s *Server) handleAddressesCreate(ctx iris.Context) {
	ctx.SetMaxRequestBodySize(MaxRequestBodySize)

	// Return first uploaded file
	_, fileHeader, err := ctx.FormFile("file")
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Could not return upload file 'file'").DetailErr(err))
		return
	}

	// Create temporary folder for upload
	tmpDir, err := ioutil.TempDir("", "upload.webapp.eotl.supply")
	if err != nil {
		ctx.StopWithError(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to create temporary directory").DetailErr(err))
		return
	}
	defer os.RemoveAll(tmpDir)

	// Save uploaded file
	csvFile := filepath.Join(tmpDir, fileHeader.Filename)
	ctx.SaveFormFile(fileHeader, csvFile)

	// Update CSV header
	csvHeader := ctx.FormValue("header")
	csvPrepend := ctx.FormValue("prepend")
	if csvHeader != "" {
		action := "replace"
		if csvPrepend == "true" {
			action = "prepend"
		}
		filepath, err := s.FileService.SetFirstLine(csvFile, csvHeader, action)
		if err != nil {
			ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to parse CSV").DetailErr(err))
			return
		}
		defer os.Remove(filepath)
		csvFile = filepath
	}

	// Import CSV
	contacts, err := s.ContactAddressDecoder.DecodeContactAddresses(csvFile)
	if err != nil {
		ctx.StopWithProblem(iris.StatusBadRequest, iris.NewProblem().Title("Unable to parse CSV").DetailErr(err))
		return
	}

	// Prepare payload
	s.LocationService.UpdateLocations(contacts)

	// Assemble response
	resp := struct {
		Success []sextant.ContactAddress `json:"addresses"`
		Failed  []sextant.ContactAddress `json:"-"`
	}{
		Success: []sextant.ContactAddress{},
		Failed:  []sextant.ContactAddress{},
	}
	for _, contact := range contacts {
		if contact.Location == nil {
			resp.Failed = append(resp.Failed, contact)
			continue
		}
		resp.Success = append(resp.Success, contact)
	}

	// Write Response
	ctx.StatusCode(iris.StatusOK)
	if err = ctx.JSON(resp); err != nil {
		ctx.StopWithProblem(iris.StatusInternalServerError, iris.NewProblem().Title("Failed to write JSON").DetailErr(err))
		return
	}
}
