package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"os/user"
	"path/filepath"
	"strings"

	"eotl.supply/sextant/csv"
	"eotl.supply/sextant/http"
	"eotl.supply/sextant/inmem"
	"eotl.supply/sextant/sqlite"
	"github.com/pelletier/go-toml"
)

const (
	DefaultConfigPath = "~/.sextant.conf"
	DefaultDSN        = "~/.sextant.db"
)

func main() {
	// Setup signal handlers.
	ctx, cancel := context.WithCancel(context.Background())
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() { <-c; cancel() }()

	m := NewMain()

	// Parse command line flags & load configuration.
	if err := m.ParseFlags(os.Args[1:]); err == flag.ErrHelp {
		os.Exit(1)
	} else if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	// Execute program.
	if err := m.Run(ctx); err != nil {
		fmt.Fprintln(os.Stderr, err)
		m.Close()
		//wtf.ReportError(ctx, err)
		os.Exit(1)
	}

	// Wait for CTRL-C.
	<-ctx.Done()

	// Clean up program.
	if err := m.Close(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

type Main struct {
	ConfigPath string
	Config     Config
	HttpServer *http.Server
	DB         *sqlite.DB
}

func NewMain() *Main {
	return &Main{
		Config:     DefaultConfig(),
		HttpServer: http.NewServer(),
		DB:         sqlite.NewDB(""),
	}
}

func (m *Main) ParseFlags(args []string) error {
	fs := flag.NewFlagSet("sextantd", flag.ContinueOnError)
	fs.StringVar(&m.ConfigPath, "config", DefaultConfigPath, "config path")
	beVerbosePtr := fs.Bool("v", false, "Display verbose output")
	if err := fs.Parse(args); err != nil {
		return err
	}
	// The expand() function is here to automatically expand "~" to the user's
	// home directory. This is a common task as configuration files are typing
	// under the home directory during local development.
	configPath, err := expand(m.ConfigPath)
	if err != nil {
		return err
	}

	// Read our TOML formatted configuration file.
	config, err := ReadConfigFile(configPath)
	if os.IsNotExist(err) {
		return fmt.Errorf("config file not found: %s", m.ConfigPath)
	} else if err != nil {
		return err
	}
	m.Config = config
	if m.Config.BeVerbose == false && *beVerbosePtr == true {
		m.Config.BeVerbose = *beVerbosePtr
	}

	return nil
}

func (m *Main) Run(ctx context.Context) (err error) {
	m.HttpServer.BeVerbose = m.Config.BeVerbose
	m.HttpServer.HttpPort = m.Config.Port
	m.HttpServer.NominatimUrl = m.Config.NominatimUrl
	m.HttpServer.VroomUrl = m.Config.VroomUrl

	// Open DB
	if m.DB.DSN, err = expandDSN(m.Config.DSN); err != nil {
		return fmt.Errorf("cannot expand dsn: %w", err)
	}
	if err = m.DB.Open(); err != nil {
		return fmt.Errorf("cannot open db: %w", err)
	}

	// Intitialize services
	m.HttpServer.ContactAddressDecoder = csv.NewContactAddressDecoder(false)
	m.HttpServer.ContactAddressService = sqlite.NewContactAddressService(m.DB)
	m.HttpServer.ContactService = sqlite.NewContactService(m.DB)
	m.HttpServer.FileService = inmem.NewFileService(false)
	m.HttpServer.GeojsonService = inmem.NewGeojsonService()
	m.HttpServer.GroupService = sqlite.NewGroupService(m.DB)
	m.HttpServer.GroupMembershipService = sqlite.NewGroupMembershipService(m.DB)
	m.HttpServer.LocationService = inmem.NewLocationService(nil, m.HttpServer.NominatimUrl, nil, false)
	m.HttpServer.TourService = sqlite.NewTourService(m.DB)
	m.HttpServer.TourItemService = sqlite.NewTourItemService(m.DB)
	m.HttpServer.TourItemtypeService = sqlite.NewTourItemtypeService(m.DB)
	m.HttpServer.VroomService = inmem.NewVroomService(nil, m.HttpServer.VroomUrl, false)
	m.HttpServer.ZoneService = sqlite.NewZoneService(m.DB)

	// Run HttpServer
	if err = m.HttpServer.Open(); err != nil {
		return err
	}

	return nil
}

// Close gracefully stops the program.
func (m *Main) Close() error {
	if m.HttpServer != nil {
		if err := m.HttpServer.Close(); err != nil {
			return err
		}
	}
	if m.DB != nil {
		if err := m.DB.Close(); err != nil {
			return err
		}
	}
	return nil
}

type Config struct {
	BeVerbose    bool   `toml:"be_verbose"`
	DSN          string `toml:"dsn"`
	NominatimUrl string `toml:"nominatim_url"`
	Port         int
	VroomUrl     string `toml:"vroom_url"`
}

func DefaultConfig() Config {
	var config Config
	config.DSN = DefaultDSN
	config.Port = 8000
	return config
}

// ReadConfigFile unmarshals config from filename. Expands path if needed.
func ReadConfigFile(filename string) (Config, error) {
	var err error
	var defaultConfigPath string
	config := DefaultConfig()
	if filename, err = expand(filename); err != nil {
		return config, err
	}
	if defaultConfigPath, err = expand(DefaultConfigPath); err != nil {
		return config, err
	}
	// Read & deserialize configuration.
	if buf, err := ioutil.ReadFile(filename); os.IsNotExist(err) {
		// If DefaultConfigPath (file) is missing do not fail
		// and use DefaultConfig
		if filename == defaultConfigPath {
			return config, nil
		}
		return config, fmt.Errorf("config file not found: %s", filename)
	} else if err != nil {
		return config, err
	} else if err := toml.Unmarshal(buf, &config); err != nil {
		return config, err
	}
	return config, nil
}

// expand returns path using tilde expansion. This means that a file path that
// begins with the "~" will be expanded to prefix the user's home directory.
func expand(path string) (string, error) {
	// Ignore if path has no leading tilde.
	if path != "~" && !strings.HasPrefix(path, "~"+string(os.PathSeparator)) {
		return path, nil
	}

	// Fetch the current user to determine the home path.
	u, err := user.Current()
	if err != nil {
		return path, err
	} else if u.HomeDir == "" {
		return path, fmt.Errorf("home directory unset")
	}

	if path == "~" {
		return u.HomeDir, nil
	}
	return filepath.Join(u.HomeDir, strings.TrimPrefix(path, "~"+string(os.PathSeparator))), nil
}

func expandDSN(dsn string) (string, error) {
	if dsn == ":memory:" {
		return dsn, nil
	}
	return expand(dsn)
}
