package main

import (
	"context"
	"eotl.supply/sextant"
	"eotl.supply/sextant/sqlite"
	"errors"
	"flag"
	"fmt"
)

type GroupCreateCommand struct {
	ConfigPath string
	DB         *sqlite.DB
}

// Reads contacts from file input (csv or yaml)
func (c *GroupCreateCommand) Run(args []string) (err error) {
	fs := flag.NewFlagSet("sextant-group-create", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	showHelp := fs.Bool("h", false, "show help")
	fs.Usage = func() {}
	if err = fs.Parse(args); err != nil {
		return err
	}
	// Validate flags
	if *showHelp {
		c.usage()
		return nil
	}
	if len(fs.Args()) == 0 {
		return errors.New("Missing required groupname.")
	}
	name := fs.Args()[0]

	// Persist data
	if c.DB.DSN, err = expandDSN(config.DSN); err != nil {
		return fmt.Errorf("cannot expand dsn: %w", err)
	}
	if err := c.DB.Open(); err != nil {
		return fmt.Errorf("cannot open db: %w", err)
	}
	defer c.DB.Close()
	ctx := context.Background()
	svc := sqlite.NewGroupService(c.DB)
	group := sextant.Group{Name: name}
	if err := svc.CreateGroup(ctx, &group); err != nil {
		return err
	}
	return nil
}

// usage print usage information for the command to STDOUT.
func (c *GroupCreateCommand) usage() {
	fmt.Println(`
Create a new group.

Usage:

	sextant group create [-h] group_name
`[1:])
}

func NewGroupCreateCommand() *GroupCreateCommand {
	return &GroupCreateCommand{
		DB: sqlite.NewDB(""),
	}
}
