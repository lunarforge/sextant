package main

import (
	"context"
	"eotl.supply/sextant/sqlite"
	"errors"
	"flag"
	"fmt"
)

type GroupDeleteCommand struct {
	ConfigPath string
	DB         *sqlite.DB
}

// Reads contacts from file input (csv or yaml)
func (c *GroupDeleteCommand) Run(args []string) (err error) {
	fs := flag.NewFlagSet("sextant-group-list", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	name := fs.String("name", "", "group name")
	id := fs.Int("id", -1, "group id")
	showHelp := fs.Bool("h", false, "show help")
	fs.Usage = func() {}
	if err = fs.Parse(args); err != nil {
		return err
	}
	// Validate flags
	if *showHelp {
		c.usage()
		return nil
	}
	if *name == "" && *id < 0 {
		return errors.New("Arguments missing")
	}

	// Delete from db
	if c.DB.DSN, err = expandDSN(config.DSN); err != nil {
		return fmt.Errorf("cannot expand dsn: %w", err)
	}
	if err := c.DB.Open(); err != nil {
		return fmt.Errorf("cannot open db: %w", err)
	}
	defer c.DB.Close()
	ctx := context.Background()
	svc := sqlite.NewGroupService(c.DB)

	if *name != "" {
		group, err := svc.FindGroupByName(ctx, *name)
		if err != nil {
			return err
		}
		id = &group.ID
	}

	if err := svc.DeleteGroup(ctx, *id); err != nil {
		return err
	}
	return nil
}

// usage print usage information for the command to STDOUT.
func (c *GroupDeleteCommand) usage() {
	fmt.Println(`
Delete a group.

Usage:

	sextant group delete [-h] -name group_name | -id id

Arguments:

	-name group_name
		The name of the group to delete. Required unless -id is set.
	-id group_id
		The id of the group to delete. Required unless -name is set.
`[1:])
}

func NewGroupDeleteCommand() *GroupDeleteCommand {
	return &GroupDeleteCommand{
		DB: sqlite.NewDB(""),
	}
}
