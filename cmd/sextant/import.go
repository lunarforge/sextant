package main

import (
	"context"
	"eotl.supply/sextant"
	"eotl.supply/sextant/csv"
	"eotl.supply/sextant/inmem"
	"eotl.supply/sextant/sqlite"
	"eotl.supply/sextant/yaml"
	"errors"
	"flag"
	"fmt"
	"os"
)

type ImportCommand struct {
	ConfigPath string
	DB         *sqlite.DB
}

// Reads contacts from file input (csv or yaml)
func (c *ImportCommand) Run(args []string) (err error) {
	fs := flag.NewFlagSet("sextant-import", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	deliveriesFile := fs.String("d", "", "A csv file of perons which describe the deliveries. Required.")
	var csvHeader string
	fs.StringVar(&csvHeader, "H", "", "String to replace deliveries csv file header with")
	fs.StringVar(&csvHeader, "csv-header", "", "String to replace deliveries csv file header with")
	var csvPrepend bool
	fs.BoolVar(&csvPrepend, "P", false, "Prepend deliveries csv file header")
	fs.BoolVar(&csvPrepend, "csv-prepend", false, "Prepend deliveries csv file header")
	pickupsFile := fs.String("p", "", "A yaml file of contacts which describe the pickups")
	ridersFile := fs.String("r", "", "A yaml file of contacts that describe the riders")
	groupName := fs.String("g", "", "An group name to add contact addresses to")
	addressLabel := fs.String("a", "", "A single address string to import")
	url := fs.String("u", config.NominatimUrl, "Nominatim instance url")

	showHelp := fs.Bool("h", false, "show help")
	fs.Usage = func() {}
	if err = fs.Parse(args); err != nil {
		return err
	}

	// Validate flags
	if *showHelp {
		c.usage(config)
		return nil
	}
	if *deliveriesFile == "" && *addressLabel == "" {
		return errors.New("Missing required argument")
	} else if *deliveriesFile != "" && *addressLabel != "" {
		return errors.New("Arguments are mutual exclusive")
	}

	// Prepare Database
	if c.DB.DSN, err = expandDSN(config.DSN); err != nil {
		return fmt.Errorf("cannot expand dsn: %w", err)
	}
	if err := c.DB.Open(); err != nil {
		return fmt.Errorf("cannot open db: %w", err)
	}
	defer c.DB.Close()
	ctx := context.Background()

	// Find Group
	var group *sextant.Group
	if *groupName != "" {
		groupService := sqlite.NewGroupService(c.DB)
		group, err = groupService.FindGroupByName(ctx, *groupName)
		if err != nil {
			return err
		}
	}

	contactAddresses := []sextant.ContactAddress{}

	// Handle import single address
	if *addressLabel != "" {
		contactAddress := sextant.ContactAddress{
			Contact: &sextant.Contact{},
			Address: &sextant.Address{Label: *addressLabel},
		}
		contactAddresses = append(contactAddresses, contactAddress)
	}

	// Handle import from file
	if *deliveriesFile != "" {
		// Update deliveries CSV Header
		if csvHeader != "" {
			fileService := inmem.NewFileService(false)
			action := "replace"
			if csvPrepend {
				action = "prepend"
			}
			if filepath, err := fileService.SetFirstLine(*deliveriesFile, csvHeader, action); err != nil {
				return err
			} else {
				defer os.Remove(filepath)
				deliveriesFile = &filepath
			}
		}
		// Import from csv file
		csvDecoder := csv.NewContactAddressDecoder(true)
		if items, err := csvDecoder.DecodeContactAddresses(*deliveriesFile); err != nil {
			return err
		} else {
			for _, item := range items {
				contactAddresses = append(contactAddresses, item)
			}
		}
		if *pickupsFile != "" {
			yamlDecoder := yaml.NewContactAddressDecoder(true)
			if items, err := yamlDecoder.DecodeContactAddresses(*pickupsFile); err != nil {
				return err
			} else {
				for _, item := range items {
					contactAddresses = append(contactAddresses, item)
				}
			}
		}
		if *ridersFile != "" {
			yamlDecoder := yaml.NewContactAddressDecoder(true)
			if items, err := yamlDecoder.DecodeContactAddresses(*ridersFile); err != nil {
				return err
			} else {
				for _, item := range items {
					contactAddresses = append(contactAddresses, item)
				}
			}
		}
	}

	// Update locations on contact addresses
	locationService := inmem.NewLocationService(nil, *url, nil, true)
	failedContacts := locationService.UpdateLocations(contactAddresses)
	// Persist addresses not found to file
	if len(failedContacts) > 0 {
		fileService := inmem.NewFileService(true)
		if _, err := fileService.ToJSON("address_notfound.json", failedContacts, "."); err != nil {
			return err
		}
	}

	// Persist contacts addresses
	contactAddressService := sqlite.NewContactAddressService(c.DB)
	var caPersisted []sextant.ContactAddress
	for _, contactAddress := range contactAddresses {
		// Skip contact without Location
		if contactAddress.Location == nil {
			continue
		} else if err := contactAddressService.FindOrCreateContactAddress(ctx, &contactAddress); err != nil {
			return err
		}
		caPersisted = append(caPersisted, contactAddress)
	}

	// Add contacts to group
	if group != nil {
		groupMembershipService := sqlite.NewGroupMembershipService(c.DB)
		for _, contactAddress := range caPersisted {
			membership := &sextant.GroupMembership{GroupID: group.ID, ContactAddressID: contactAddress.ID}
			if err := groupMembershipService.FindOrCreateGroupMembership(ctx, membership); err != nil {
				//fmt.Printf("ContactAddress: %#v\nGroup: %#v\n#GroupMembership: %#v", contactAddress, group, membership)
				return err
			}
		}
	}

	return nil
}

// usage print usage information for the command to STDOUT.
func (c *ImportCommand) usage(config Config) {
	fmt.Println(`
Import, locate and persist contacts, addresses and locations

Usage:

	sextant import [-h] [-g group_name] [-u url] [-H header_string [-P]] 
	               [-p pickups_file] [-r riders_file] -d deliveries_file

	sextant import [-h] [-g group_name] [-u url] -a address_string

Arguments:

	-a address_string
		Import a single address string
	-d file
		Import from deliveries csv file
	-g group_name
		The group name to add contact addresses to
	-H header_string
	--csv-header header_string
		The string replaces first line in deliveries csv file
	-P 
	--csv-prepend
		Given '-H string' is set, enabling this flag will prepend 
		the string to deliveries csv file
	-p pickups_file
		Import from pickups yaml file
	-r riders_file
		Import from riders yaml file
	-u url
		to Nominatim server (default: ` + config.NominatimUrl + `\)

	For a detailed specification of input files
	please see README document.
`[1:])
}

func NewImportCommand() *ImportCommand {
	return &ImportCommand{
		DB: sqlite.NewDB(""),
	}
}
