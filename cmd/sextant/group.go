package main

import (
	"flag"
	"fmt"
	"strings"
)

// GroupCommand represents a collection of group-related subcommands.
type GroupCommand struct{}

// Run executes the command which delegates to other subcommands.
func (c *GroupCommand) Run(args []string) error {
	// Shift off the subcommand name, if available.
	var cmd string
	if len(args) > 0 && !strings.HasPrefix(args[0], "-") {
		cmd, args = args[0], args[1:]
	}

	// Delegete to the appropriate subcommand.
	switch cmd {
	case "create":
		return (NewGroupCreateCommand()).Run(args)
	case "list":
		return (NewGroupListCommand()).Run(args)
	case "delete":
		return (NewGroupDeleteCommand()).Run(args)
	case "", "-h", "help":
		c.usage()
		return flag.ErrHelp
	default:
		return fmt.Errorf("sextant group %s: unknown command", cmd)
	}
}

// usage prints the subcommand usage to STDOUT.
func (c *GroupCommand) usage() {
	fmt.Println(`
Manage sextant groups

Usage:

	sextant group <command> [arguments]

The commands are:

	list        all available groups
	create      a new group
	delete      a group by id or name
`[1:])
}
