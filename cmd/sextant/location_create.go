package main

import (
	"eotl.supply/sextant"
	"eotl.supply/sextant/inmem"
	"errors"
	"flag"
	"fmt"
)

type LocationCreateCommand struct {
	ConfigPath string
}

func (c *LocationCreateCommand) Run(args []string) error {
	fs := flag.NewFlagSet("sextant-locate", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	url := fs.String("u", config.NominatimUrl, "Nominatim instance url")
	address := fs.String("a", "", "an address string")
	fromWords := fs.String("w", "", "a string of where39 words")
	showHelp := fs.Bool("h", false, "show help")

	if err := fs.Parse(args); err != nil {
		return err
	}

	if *showHelp {
		c.usage(config)
		return nil
	}

	if *address != "" && *fromWords != "" {
		return errors.New("Multipe exclusive arguments set")
	}

	if *address != "" {
		locationService := inmem.NewLocationService(nil, *url, nil, false)
		location_ptr, err := locationService.FetchLocation(*address)
		if err != nil {
			return err
		}
		encoder := inmem.JsonEncoder{}
		output, err := encoder.EncodeToJson(*location_ptr)
		if err != nil {
			return err
		}
		fmt.Println(string(output))
		return nil
	}

	if *fromWords != "" {
		locationWordService := inmem.NewLocationWordService()
		location_ptr, err := locationWordService.FromWords(*fromWords)
		if err != nil {
			return err
		}
		encoder := inmem.JsonEncoder{}
		output, err := encoder.EncodeToJson(*location_ptr)
		if err != nil {
			return err
		}
		fmt.Println(string(output))
		return nil
	}

	inputData, err := GetInputData(fs)
	if err != nil {
		return err
	}

	encoder := inmem.JsonEncoder{}
	sextantStruct, err := encoder.DecodeFromJson(inputData)
	if err != nil {
		return err
	}
	tour := sextantStruct.(sextant.Tour)
	locationService := inmem.NewLocationService(nil, *url, nil, true)
	failedContacts := locationService.UpdateLocations(tour.ContactAddresses())

	if len(failedContacts) > 0 {
		fileService := inmem.NewFileService(true)
		_, err := fileService.ToJSON("address_notfound.json", failedContacts, ".")
		if err != nil {
			return err
		}
	}

	output, err := encoder.EncodeToJson(tour)
	if err != nil {
		return err
	}

	fmt.Println(string(output))
	return nil
}

// usage print usage information for the command to STDOUT.
func (c *LocationCreateCommand) usage(config Config) {
	fmt.Println(`
Fetches geo locations for contacts in sextant.Tour
or a single address string.

Usage:

	sextant locate [-h] [-c config_file] [-u url] -a address_string | -w word_string | file

Arguments:

	The file should be a sextant.Tour encoded with sextant.inmem.JsonEncoder.
	If file is "-" or absent the command reads from the standard input.

	-c config_file
		Path to config file (default: ` + DefaultConfigPath + `)
	-a address_string
		Specifies the address compatible with nominatim API e.g. "Times Square, New York". 
		If this flag is used -w flag and file argument are ignored.
	-u url
		The url to nominatim server (default: ` + config.NominatimUrl + `\)
	-w word_string
		A string of where39 words.
		If this flag is used file argument are ignored.

Output:
		
	With 'file':       sextant.Tour
	with -a or -w:     sextant.Location

	encoded with sextant.inmem.JsonEncoder.

	If addresses in sextant.Tour fail to be located they are
	written to 'address_notfound.json'.
`[1:])
}
