package main

import (
	"context"
	"eotl.supply/sextant/geojson"
	"eotl.supply/sextant/inmem"
	"eotl.supply/sextant/sqlite"
	"errors"
	"flag"
	"fmt"
	"regexp"
)

type ZoneCreateCommand struct {
	ConfigPath string
	DB         *sqlite.DB
}

// Reads contacts from file input (csv or yaml)
func (c *ZoneCreateCommand) Run(args []string) (err error) {
	fs := flag.NewFlagSet("sextant-zone-create", flag.ContinueOnError)
	attachConfigFlags(fs, &c.ConfigPath)
	config, err := ReadConfigFile(c.ConfigPath)
	if err != nil {
		return err
	}
	zonesFile := fs.String("f", "", "A json file describing the zones. Required.")
	nameKey := fs.String("n", "", "Use 'nameKey' to extract zone 'name' value.")
	parentZoneID := fs.Int("p", -1, "Create zone(s) as child of zone with this db ID.")
	showHelp := fs.Bool("h", false, "show help")
	fs.Usage = func() {}
	if err = fs.Parse(args); err != nil {
		return err
	}
	// Validate flags
	if *showHelp {
		c.usage()
		return nil
	}
	if *zonesFile == "" {
		return errors.New("Missing required argument")
	}

	// Decode Zones from file
	zoneDecoder := geojson.NewZoneDecoder(true)
	zones, err := zoneDecoder.DecodeZones(*zonesFile, *nameKey)
	if err != nil {
		return err
	}
	// Set zone.Abbr value
	svc := inmem.NewZoneService()
	if _, err := svc.SetZoneAbbr(zones); err != nil {
		return err
	}
	// Set parent Zone
	if *parentZoneID > 0 {
		for i, _ := range zones {
			zones[i].ParentID = parentZoneID
		}
	}

	// Persist data
	if c.DB.DSN, err = expandDSN(config.DSN); err != nil {
		return fmt.Errorf("cannot expand dsn: %w", err)
	}
	if err := c.DB.Open(); err != nil {
		return fmt.Errorf("cannot open db: %w", err)
	}
	defer c.DB.Close()
	ctx := context.Background()
	svcSqlite := sqlite.NewZoneService(c.DB)
	for _, zone := range zones {
		if err := svcSqlite.CreateZone(ctx, &zone); err != nil {
			if m, _ := regexp.MatchString(".*UNIQUE constraint failed.*", err.Error()); m {
				fmt.Printf("Record already exists: %s\n", zone.Name)
			} else {
				return err
			}
		}
	}
	return nil
}

// usage print usage information for the command to STDOUT.
func (c *ZoneCreateCommand) usage() {
	fmt.Println(`
Create a new zone.

Usage:

	sextant zone create [-h] [-n nameKey] [-p parentZoneID] -f zones.json

Arguments:

	-n nameKey
		Use "namekey" to read zone name (default: name)
	-p parentZoneID 
		Zone with this table ID will be parent of created zone(s)
`[1:])
}

func NewZoneCreateCommand() *ZoneCreateCommand {
	return &ZoneCreateCommand{
		DB: sqlite.NewDB(""),
	}
}
