package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"eotl.supply/sextant/inmem"
	"github.com/pelletier/go-toml"
)

const (
	DefaultConfigPath      = "~/.sextant.conf"
	DefaultDSN             = "~/.sextant.db"
	DefaultSsrAuthFilepath = "auth.ssr"
)

func main() {
	if err := Run(os.Args[1:]); err == flag.ErrHelp {
		os.Exit(1)
	} else if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func Run(args []string) error {
	// Shift off subcommand from the argument list, if available.
	var cmd string
	if len(args) > 0 {
		cmd, args = args[0], args[1:]
	}

	// Delegate subcommands to their own Run() methods.
	switch cmd {
	case "solve":
		return (&VroomResponseCreateCommand{}).Run(args)
	case "locate":
		return (&LocationCreateCommand{}).Run(args)
	case "geojson":
		return (&GeojsonCreateCommand{}).Run(args)
	case "tour":
		return (&TourCreateCommand{}).Run(args)
	case "import":
		return (NewImportCommand()).Run(args)
	case "group":
		return (&GroupCommand{}).Run(args)
	case "zone":
		return (&ZoneCommand{}).Run(args)
	case "auth":
		return (&AuthCommand{}).Run(args)
	case "", "-h", "help":
		usage()
		return flag.ErrHelp
	default:
		return fmt.Errorf("sextant %s: unknown command", cmd)
	}
}

func usage() {
	fmt.Println(`
sextant is a tool which does various geolocational tasks

Usage:

	sextant <command> [arguments]

The commands are:
	
	auth     manage access to sextantd
	import   load, locate and persist contacts, addresses, locations 
	locate   geo locations
	geojson  convert to geojson
	group    manage groups
	tour     create a tour of delivieries, pickups, riders
	solve    a tour using Vroom
	zone     manage zones 

`[1:])
}

// Config represents a configuration file common to all subcommands.
type Config struct {
	NominatimUrl    string `toml:"nominatim_url"`
	VroomUrl        string `toml:"vroom_url"`
	DSN             string `toml:"dsn"`
	SsrAuthFilepath string `toml:"ssrauth_filepath"`
}

func DefaultConfig() Config {
	return Config{
		NominatimUrl:    inmem.NominatimDefaultUrl,
		VroomUrl:        inmem.VroomDefaultUrl,
		DSN:             DefaultDSN,
		SsrAuthFilepath: DefaultSsrAuthFilepath,
	}
}

// ReadConfigFile unmarshals config from filename. Expands path if needed.
func ReadConfigFile(filename string) (Config, error) {
	var err error
	var defaultConfigPath string
	config := DefaultConfig()
	if filename, err = expand(filename); err != nil {
		return config, err
	}
	if defaultConfigPath, err = expand(DefaultConfigPath); err != nil {
		return config, err
	}
	// Read & deserialize configuration.
	if buf, err := ioutil.ReadFile(filename); os.IsNotExist(err) {
		// If DefaultConfigPath (file) is missing do not fail
		// and use DefaultConfig
		if filename == defaultConfigPath {
			return config, nil
		}
		return config, fmt.Errorf("config file not found: %s", filename)
	} else if err != nil {
		return config, err
	} else if err := toml.Unmarshal(buf, &config); err != nil {
		return config, err
	}
	return config, nil
}

// attachConfigFlags adds a common "-config" flag to a flag set.
func attachConfigFlags(fs *flag.FlagSet, p *string) {
	fs.StringVar(p, "c", DefaultConfigPath, "config path")
}

// Expand filenpath, if necessary. This means substituting a "~" prefix
// with the user's home directory, if available.
func expand(filename string) (string, error) {
	if prefix := "~" + string(os.PathSeparator); strings.HasPrefix(filename, prefix) {
		u, err := user.Current()
		if err != nil {
			return filename, err
		} else if u.HomeDir == "" {
			return filename, fmt.Errorf("home directory unset")
		}
		filename = filepath.Join(u.HomeDir, strings.TrimPrefix(filename, prefix))
		return filename, nil
	}
	return filename, nil
}

// expandDSN expands a datasource name. Ignores in-memory databases.
func expandDSN(dsn string) (string, error) {
	if dsn == ":memory:" {
		return dsn, nil
	}
	return expand(dsn)
}
