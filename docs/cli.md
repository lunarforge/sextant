---
title: CLI
titleFull: Sextant CLI
kind: documenation
leafs: ["sextant", "cli"]
---

The following subcommands are available:

```
sextant is a tool which does various geolocational tasks

Usage:

	sextant <command> [arguments]

The commands are:
	
	import   load, locate and persist contacts, addresses, locations 
	tour     create a tour of delivieries, pickups, riders
	locate   geo locations
	solve    a tour using Vroom
	geojson  convert to geojson
	group    manage groups
  zone     manage zones
```

Apart from `import` the sub commands support shell pipes.
The expected input data format is json with the structure of
`sextant.inmem.JsonEnvelope`. 

Example:

```
$ sextant tour -d deliveries.csv -r riders.yaml | sextant locate | sextant solve | sextant geojson
```

### Import

```
Import, locate and persist contacts, addresses and locations

Usage:

	sextant import [-h] [-H string [-P]] [-p file] [-r file] [-g string] -d file

Arguments:

	-d file
		Import from deliveries csv file
	-H string
	--csv-header string
		The string replaces first line in deliveries csv file
	-P 
	--csv-prepend
		Given '-H string' is set, enabling this flag will prepend 
		the string to deliveries csv file
	-p file
		Import from pickups yaml file
	-r file
		Import from riders yaml file
	-u url
		to Nominatim server (default: http://localhost/nominatim)
	-g string 
		The group name to add contact addresses to

	For a detailed specification of input files
	please see README document.
```

The specification of input formats of CSV of a `deliveries.csv` file which is
ensure imported deliveries CSV file has a header row where
each value can be one of the following:

```
Name,Email,Address,AddressDetails,Phone,Amount,From,To
``` 

- All columns except `Address` are optional
- The columns can be in any order
- The values in columns "From" and "To" are to be written in the format "01-Dec-21 15:00" (i.e day-month-year hour:minute).
- Missing "From/To" columns or values have the effect of no time contraints.
- A missing "Amount" value/column defaults to value 1.

A note on internal side effects:

For each address the importer tries to find the geo
coordinates and stores them in a location table.
It also checks and persists which zone a location
is member of.

### Tour

```
Imports deliveries and optionally pickups and riders.
Returns a sextant.Tour as json.

Usage:

	sextant tour [-h] [-H header_string [-P]] [-p pickups_file] [-r riders_file] -d deliveries_file

Arguments:

	-d deliveries_file
		Load deliveries from csv file
	-H header_string
	--csv-header header_string
		The string replaces first line in deliveries csv file
	-P 
	--csv-prepend
		Given '-H string' is set, enabling this flag will prepend 
		the string to deliveries csv file
	-p pickups_file
		Load pickups from yaml file
	-r riders_file
		Load riders from yaml file

	For a detailed specification of input files
	please see README document.
```

During tour creation _Address_ data is parsed and potentially altered 
so it becomes meaningful input for `sextant locate`, e.g.

```
"Wallstraße 5, apt. 2.5.2, 10179 Berlin" => 
Address{Label: "Wallstraße 5, 10179 Berlin", Details: "apt. 2.5.2"}
```

For a specification of input formats see further below.

Limitation: currently only one pickup location is supported
(the first one found in 'pickups.yaml').


### Locate

```
Fetches geo locations for contacts in sextant.Tour
or a single address string.

Usage:

	sextant locate [-h] [-c config_file] [-u url] -a address_string | -w word_string | file

Arguments:

	The file should be a sextant.Tour encoded with sextant.inmem.JsonEncoder.
	If file is "-" or absent the command reads from the standard input.

	-c config_file
		Path to config file (default: ~/.sextant.conf)
	-a address_string
		Specifies the address compatible with nominatim API e.g. "Times Square, New York". 
		If this flag is used -w flag and file argument are ignored.
	-u url
		The url to nominatim server (default: http://localhost/nominatim)
	-w word_string
		A string of where39 words.
		If this flag is used file argument are ignored.

Output:
		
	With 'file':       sextant.Tour
	with -a or -w:     sextant.Location

	encoded with sextant.inmem.JsonEncoder.

	If addresses in sextant.Tour fail to be located they are
	written to 'address_notfound.json'.
```


### Solve

```
Solves a tour finding optimal routes

Usage:

	sextant solve [-hw] [-c config_file] [-u url] file 

Arguments:

	The file should be a sextant.Tour encoded with sextant.inmem.JsonEncoder.
	If file is "-" or absent the command reads from the standard input.

	-c config_file
		Path to config file (default: ~/.sextant.conf)
	-u url
		Specifies url to Vroom solver (default: http://localhost:3000)
	-w
		write sextant.VroomRequest to vroom_request.json
```

The file 'vroom_request.json' may be used as input with
Vroom solver web UI. 

At present time (Feb 2022) this UI is limited: It can only 
solve a request and successfully render the result 
if the request describes jobs, not shipments. 
(see [VROOM FrontEnd Issue 56](https://github.com/VROOM-Project/vroom-frontend/issues/56))

In other words, if you want to use the Vroom solver web UI, 
the `sextant.Tour` passed to `sextant solve` must contain 
_Deliveries_ and _Riders_ only, not _Pickups_.



`pickups.yaml` - the following yaml structure applies to pickups.yaml

```
---
1: 
 name: SuperStore
 email: email@somewhere.com
 address: Somestreet 11, berlin 12045
 address_details: 3rd floor
 phone: 123456789
 from: 12-Dec-21 09:00
 to: 12-Dec-21 12:00
```

- All keys except `address` are optional
- Missing "from/to" keys or values have the effect of no time contraints.
- The current implementation uses the first pickup place only.
 - If no pickup entry is found no shipments will be rendered in vroom request output.


`riders.yaml` - same structure as pickups.yaml

```
---
1: 
 name: Crazy Rider
 email:  email@nowhere.com
 address: Someway 3, berlin 12045
 address_details: In the backyard
 phone: 123456789
 amount: 20
 from: 12-Dec-21 11:00 
 to: 12-Dec-21 15:00
```

- All keys except `address` are optional
- A missing "amount" key or value defaults to value 1.
- Missing "from/to" key or values have the effect of no time contraints.


### Geojson

Convers contact data with locations to GeoJSON format

```
Converts to geojson format

Usage:

	sextant geojson [-h] file

Arguments:

	The file should be sextant.Tour or sextant.VroomResponse encoded as sextant.JsonEnvelope.
	If file is "-" or absent the command reads from the standard input.

Output:

  For sextant.Tour: it converts tour.Delivieries to geojson
  For sextant.VroomResponse: it converts vroomResponse.GetRoutes() to geojson
```

### Group

Managing `groups` of contacts

```
Manage sextant groups

Usage:

	sextant group <command> [arguments]

The commands are:

	list        all available groups
	create      a new group
	delete      a group by id or name
```

See `sextant group <command> -h` for more details.


### Zone

Manging `zones` used within sextant

```
Manage sextant zones

Usage:

	sextant zone <command> [arguments]

The commands are:

	list        all available zones
	create      a new zone
	delete      a zone by id
```
		
See `sextant zone <command> -h` for more details.

A note on internal side effects:

The database persists which zone a location is member of. 
These zone memberships are created when a new zone
or location is created. The reason for this mechanism is to
enable an efficient query such as 
_'show all contacts/addresses/locations within zone'_.
