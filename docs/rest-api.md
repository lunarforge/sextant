---
title: Rest API
titleFull: Sextant REST API
kind: documenation
leafs: ["sextant", "cli"]
---

Is a HTTP daemon with an API for location / routing related endpoints.

- Use of `id_` refers to object type (`group_id` or `tours_id`)
- Auth `Yes` requires [SSRs](/eotl/simple-signed-requests) to grant access
- Level default key

```
| Method | Route                                   | Auth | Level |
| ------ | --------------------------------------- | ---- | ------|
| GET    | /api/contacts                           | Yes  | Admin |
| POST   | /api/contacts                           | Yes  | User  |
| POST   | /api/geocode/address                    | No   | None  | 
| POST   | /api/geocode/addresses                  | No   | None  |
| GET    | /api/groups                             | No   | User  |
| POST   | /api/groups                             | Yes  | Admin |
| DELETE | /api/groups/{id_or_slug}                | Yes  | Admin |
| GET    | /api/groups/{id_or_slug}/contacts       | Yes  | User  |
| GET    | /api/groups/{id_or_slug}/tours          | Yes  | User  |
| POST   | /api/groups/{id_or_slug}/tours          | Yes  | User  |
| POST   | /api/groups/{id_or_slug}/tours/{id_or_slug}/items           | Yes  | User  |
| DELETE | /api/groups/{id_or_slug}/tours/{id_or_slug}/items/{item_id} | Yes  | User  |
| PATCH  | /api/groups/{id_or_slug}/tours/{id_or_slug}/items/{item_id} | Yes  | User  |
| POST   | /api/tours                              | Yes  | Admin |
| GET    | /api/tours/{id_or_slug}                 | Yes  | Admin |
| DELETE | /api/tours/{id_or_slug}                 | Yes  | Admin |
| POST   | /api/touritems                 		     | Yes  | Admin |
| DELETE | /api/touritems/{id}         		         | Yes  | Admin |
| PATCH  | /api/touritems/{id}         		         | Yes  | Admin |
| GET    | /api/users/{contact_id}/                | Yes  | User  |
| GET    | /api/users/{contact_id}/contacts        | Yes  | User  |
| GET    | /api/users/{contact_id}/tours           | Yes  | User  |
| POST   | /api/vroom                              | No   | None  |
| POST   | /api/vroom/solver                       | No   | None  |
| GET    | /api/zones                              | No   | All   |
```

##  Contacts

Find contact addresses that are part of a contacts' (users') 
tours.

```
$ curl http://localhost:8080/api/contacts?contact_id=1
```

Find contact addresses that belong to a certain group

```
$ curl http://localhost:8080/api/contacts?group=superfood
```

Find contact addresses that have a string "q" in contact name or address label (not case sensitive)

```
$ curl http://localhost:8080/api/contacts?q=Ma
```

Find contact addresses that belong to a certain zone

```
$ curl http://localhost:8080/api/contacts?zone=NKN
```

Add one or more contacts from a POST request

```
$ cat contacts.json 
{
    "group": "comrades",
    "contacts": [{
        "name": "Robert Zimmerman",
        "address": "Tucholskystrasse 32, Berlin, DE",
        "phone": "+49234567890"
    }]
}
$ curl -d @contacts.json http://localhost:8080/api/contacts
```

## Geocoding

Lookup a single address passed as string to obtain latitute & longitude values

```
$ curl -d "address=35, Rue de la Bûcherie,Paris,75005" http://localhost:8080/api/geocode/address
```

Perform geocoding lookup on multiple addresses stored in a CSV file

```
$ curl -F file=@testdata/input/addresses.csv http://localhost:8080/api/geocode/addresses
```

An example of `addresses.csv` file

```
Name,Email,Address,AddressDetails,Phone,Amount,From,To
Robert Zimmerman,justbob@,"Tucholskystrasse 32, Berlin, DE","",+49234567890,,,
```

## Groups

List all groups

```
$ curl http://localhost:8080/api/groups
```

Create a group

```
$ curl -d @testdata/input/group.json http://localhost:8008/api/groups
```

View a group by id or slug

```
$ curl http://localhost:8080/api/groups/1
$ curl http://localhost:8080/api/groups/tasty-tacos
```

Delete a group by id or slug

```
$ curl -X "DELETE" http://localhost:8080/api/groups/1
$ curl -X "DELETE" http://localhost:8080/api/groups/tasty-tacos
```

List contacts that are part of group

```
$ curl http://localhost:8080/api/groups/1/contacts
$ curl http://localhost:8080/api/groups/tasty-tacos/contacts
```

List tours that are belong to a group

```
$ curl http://localhost:8080/api/groups/1/tours
$ curl http://localhost:8080/api/groups/tasty-tacos/tours
```

Create a tour from json file that belongs to a group

```
$ curl -d @testdata/input/tour.json http://localhost:8080/api/groups/tasty-tacos/tours
```

## Tours

Create a tour from json file

```
$ curl -d @testdata/input/tour.json http://localhost:8080/api/tours
```

- Date value is string that is RFC3339 formatted, e.g. `2022-05-22T00:00:00.00Z`


List all tours

```
$ curl http://localhost:8080/api/tours
```

View a tour by id or slug

```
$ curl http://localhost:8080/api/tours/1
$ curl http://localhost:8080/api/tours/northern-route
```

Delete a tour by id or slug

```
$ curl -X "DELETE" http://localhost:8080/api/tours/1
$ curl -X "DELETE" http://localhost:8080/api/tours/northern-route
```

## TourItems

View a touritem by id

```
$ curl http://localhost:8080/api/touritems/1
```

Delete a touritem by id

```
$ curl -X "DELETE" http://localhost:8080/api/touritems/1
```

Update a touritem by id

```
$ curl --data "{\"state\":\"success\"}" -X "PATCH" http://localhost:8080/api/touritems/1
```

It is also possible to interact via this namespace:

`/api/groups/{id_or_slug}/tours/{id_or_slug}/items/{id}`

The code does validate the group-tour-item relationship
expressed in the path to avoid abuse.

## Users

Any contact in the system may also be a user.

View a user

```
$ curl http://localhost:8080/api/users/1
```

NOTE: With current implementation this returns only the users 
contact data but not the addresses that belong to it.

List the tours a user is part of

```
$ curl http://localhost:8080/api/users/1/tours
```

## Vroom

Generate JSON file which can be submitted to Vroom routing optimization engine

```
$ curl -d @deliveriesRidersPickups.json http://localhost:8080/api/vroom
```

To solve a VroomRequest json file with the Vroom routing optimization engine

```
$ curl -d @vroom_request.json http://localhost:8080/api/vroom/solver
```

## Zones

Display a given zone and sub-zones

```
$ curl http://localhost:8080/api/zones?id=BER
```
