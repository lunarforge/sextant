package sextant

import (
	"context"
	"github.com/paulmach/orb"
	"github.com/paulmach/orb/geojson"
	"github.com/paulmach/orb/planar"
	"time"
)

type Zone struct {
	ID        int          `json:"-" yaml:"-"`
	ParentID  *int         `json:"-" yaml:"-"`
	Abbr      string       `json:"abbr" yaml:"-"`
	Name      string       `json:"name" yaml:"-"`
	Geometry  orb.Geometry `json:"geometry,omitempty" yaml:"-"`
	CreatedAt time.Time    `json:"-" yaml:"-"`
	UpdatedAt time.Time    `json:"-" yaml:"-"`
	Children  []*Zone      `json:"sub-zones,omitempty" yaml:"-"`
}

func (z *Zone) Validate() error {
	return nil
}

func (z *Zone) Contains(l Location) bool {
	point := orb.Point{l.Lon, l.Lat}
	geometry := z.Geometry

	switch geometry.(type) {
	case orb.MultiPolygon:
		mp := geometry.(orb.MultiPolygon)
		return planar.MultiPolygonContains(mp, point)
	case orb.Polygon:
		p := geometry.(orb.Polygon)
		return planar.PolygonContains(p, point)
	default:
		return false
	}
}

// NOTE: for simplicity it was opted to not
// mix this up with sextant.GeojsonService
func (z *Zone) ToFeatureCollection() geojson.FeatureCollection {
	fc := geojson.NewFeatureCollection()
	f := geojson.NewFeature(z.Geometry)
	f.Properties["name"] = z.Name
	f.Properties["abbr"] = z.Abbr
	if len(z.Children) > 0 {
		f.Properties["relation"] = "parent"
	}
	fc.Append(f)
	// Add chileZone features
	for _, childZone := range z.Children {
		f := geojson.NewFeature(childZone.Geometry)
		f.Properties["name"] = childZone.Name
		f.Properties["abbr"] = childZone.Abbr
		f.Properties["relation"] = "child"
		fc.Append(f)
	}
	return *fc
}

type ZoneFilter struct {
	ID       *int    `json:"id"`
	ParentID *int    `json:"-"`
	Abbr     *string `json:"-"`
	Offset   int     `json:"offset"`
	Limit    int     `json:"limit"`
}

type ZoneService interface {
	CreateZone(ctx context.Context, zone *Zone) error

	DeleteZone(ctx context.Context, id int) error

	FindZoneByAbbr(ctx context.Context, abbr string) (*Zone, error)

	FindZoneByID(ctx context.Context, id int) (*Zone, error)

	FindZones(ctx context.Context, filter ZoneFilter) ([]*Zone, int, error)

	SetZoneAbbr(zones []Zone) ([]Zone, error)
}

type ZoneDecoder interface {
	DecodeZones(filepath string, nameKey string) ([]Zone, error)
}
