package sextant

import (
	"context"
	"time"
)

var ValidTourItemStates = [...]string{"created", "assigned", "delivery", "success", "failed", "partial"}

type TourItem struct {
	ID               int `json:"id"`
	TourID           int `json:"tour_id"`
	ContactAddressID int `json:"contact_address_id"`
	*ContactAddress
	TourItemtypeID   int       `json:"tour_itemtype_id"`
	TourItemtypeName string    `json:"type,omitempty"`
	State            string    `json:"state"`
	CreatedAt        time.Time `json:"-"`
	UpdatedAt        time.Time `json:"-"`
}

type TourItemUpdate struct {
	State *string `json:"state"`
}

type TourItemFilter struct {
	ID               *int
	TourID           *int
	ContactAddressID *int
	TourItemtypeID   *int
	Offset           int
	Limit            int
}

func (tm *TourItem) Validate() error {
	if tm.State == "" {
		return Errorf(EINVALID, "TourItem state missing")
	}

	for _, state := range ValidTourItemStates {
		if tm.State == state {
			return nil
		}
	}

	return Errorf(EINVALID, "TourItem state invalid")
}

type TourItemService interface {
	CreateTourItem(ctx context.Context, tm *TourItem) error

	DeleteTourItem(ctx context.Context, id int) error

	FindTourItemsByTourID(ctx context.Context, tourID int) ([]TourItem, error)

	FindTourItemByID(ctx context.Context, id int) (*TourItem, error)

	// Updates the state field of a touritem.
	UpdateTourItem(ctx context.Context, id int, upd TourItemUpdate) (*TourItem, error)
}
