#!/bin/sh

set -e

BIN_PATH=~/.local/bin/
WWW_PATH="$(pwd)/www/dist"

# https://stackoverflow.com/questions/600079/how-do-i-clone-a-subdirectory-only-of-a-git-repository/13738951#13738951
git_sparse_clone() (
  rurl="$1" localdir="$2" && shift 2

  mkdir -p "$localdir"
  cd "$localdir"

  git init
  git remote add -f origin "$rurl"

  git sparse-checkout init

  # Set dirs to checkout
  git sparse-checkout set "data/json/zones"

  git pull --depth=1 origin main
)

build_binaries() {
  if ! (which go > /dev/null) then
    echo "Prerequisite 'go' not found, please install: https://go.dev"
    exit 1
  fi

  echo "Building sextant"
  cd $(pwd)/cmd/sextant/
  go build -o sextant *.go

  echo "Building sextantd"
  cd ../sextantd/
  go build

  if [ "$1" = "" ]
  then
    mkdir -p $BIN_PATH
    COPY_PATH=$BIN_PATH
  else
    COPY_PATH=$1
  fi
  echo "Copying binaries to $COPY_PATH"
  cd ../../
  cp cmd/sextant/sextant $COPY_PATH
  cp cmd/sextantd/sextantd $COPY_PATH

  echo "Done!!"
}

pull_bootstrap_theme() {
  check_basic_prequisites
  # To hardcode replace with: pkg_version="0.1.5"
  pkg_version=$(curl -s https://registry.npmjs.org/@eotl/theme-bootstrap/latest | jq -r '.version')
  compiled_theme_url="https://registry.npmjs.org/@eotl/theme-bootstrap/-/theme-bootstrap-${pkg_version}.tgz"
  
  printf "\n%s\n" "Pulling @eotl/theme-bootstrap ${pkg_version}"

	tmpdir="$(mktemp -d -t 'sextant.XXXXXX')"
	trap 'rm -rf -- "$tmpdir"' EXIT
	tmpfile="$tmpdir/theme-eotl.tar.gz"

  echo "Pulling the theme compiled file..."
  curl -s "$compiled_theme_url" > "$tmpfile"

  echo "Extracting theme files into ${WWW_PATH} ..."
	mkdir -p "$WWW_PATH"
	cd "$WWW_PATH"
	if test -x "$(which pax 2> /dev/null)"
	then
		pax -r -z -f "$tmpfile" -s '_package/dist/__'
	else
		tar -xf "$tmpfile" --strip-components 2 package/dist
	fi

  # Due to Apache2 aliasing of /icons to /usr/share/apache2/icons/
  echo "Moving icons/ directory"
  rm -rf "$WWW_PATH"/images/icons 
  mv "$WWW_PATH"/icons "$WWW_PATH"/images/

  echo "Cleaning index.html"
  rm "$WWW_PATH"/index.html
	cd - >> /dev/null

  printf "\n%s\n" "Done, frontend theme downloaded and extracted."
}


pull_ui() {
  check_basic_prequisites
  # To hardcode replace with: pkg_version="0.1.4"
  pkg_version="$(curl -s https://registry.npmjs.org/@eotl/sextant/latest | jq -r '.version')"
  compiled_ui_url="https://registry.npmjs.org/@eotl/sextant/-/sextant-${pkg_version}.tgz"
  
  printf "\n%s\n" "Pulling @eotl/sextant ${pkg_version}"
	tmpdir="$(mktemp -d -t 'sextant.XXXXXX')"
	trap 'rm -rf -- "$tmpdir"' EXIT
	tmpfile="$tmpdir/ui-eotl.tar.gz"

  echo "Pulling the theme compiled file..."
  curl -s "$compiled_ui_url" > "$tmpfile"

  echo "Extracting theme files into ${WWW_PATH} ..."
	mkdir -p "$WWW_PATH"
	cd "$WWW_PATH"
	if test -x "$(which pax 2> /dev/null)"
	then
		pax -r -z -f "$tmpfile" -s '_package/dist/__'
	else
		tar -xf "$tmpfile" --strip-components 2 package/dist
	fi

  printf "\n%s\n" "Done, sextant downloaded and extracted."
	cd - >> /dev/null
}

install_data() {
    DATA_PATH="${HOME}/.local/share/eotl/core/"
    ZONES_PATH="${HOME}/.local/share/eotl/core/data/json/zones/"

    if [ ! -d "${DATA_PATH}" ]
    then
        echo "Cloning Zones data from: ${1}"
        git_sparse_clone "${1}" "${DATA_PATH}"
        sleep 1
    else
        echo "EOTL Data already exists: ${DATA_PATH}"
        sleep 2
    fi

    if [ ! -d "${ZONES_PATH}" ]
    then
        echo "Zones directory does exist, failing..."
        exit 1
    fi

    echo "Installing Zones data into Sextant"
    sleep 1
    sextant zone create -f "${ZONES_PATH}berlin.geojson" -n GEN 
    sextant zone create -f "${ZONES_PATH}berlin-districts.geojson" -p 1
    sextant zone create -f "${ZONES_PATH}berlin-district-kreuzberg.geojson" -p 9
    sextant zone create -f "${ZONES_PATH}berlin-district-neukoelln.geojson" -p 3 
}

create_defaults() {
    CONF_PATH="${HOME}/.sextant.conf"

    if [ ! -f "${CONF_PATH}" ]
    then
        echo "Copying default config to: ${CONF_PATH}"
        cp sextant.conf $CONF_PATH 
    else
        echo "Existing config found, ignoring"
    fi
    exit 1

    echo "Creating default groups: riders, suppliers, customers..."
    sleep 1
    sextant group create contacts
    sextant group create riders
    sextant group create suppliers
    sextant group create customers        
}

create_testing() {
    check_basic_prequisites
    echo "Installing test data: customers, riders, suppliers..."
    sleep 1

    curl -d @www/testing/customers.json http://localhost:8081/api/contacts
    curl -d @www/testing/riders.json http://localhost:8081/api/contacts
    curl -d @www/testing/suppliers.json http://localhost:8081/api/contacts
}

check_basic_prequisites() {
  if ! (which curl > /dev/null) then
    echo "Prerequisite 'curl' not found, please install"
    exit 1
  fi

  if ! (which jq > /dev/null) then
    echo "Prerequisite 'jq' not found, please install"
    exit 1
  fi
}

# Script start
if [ $# -le 0 ]
then
	cat <<-EOF
Usage: ${0} binaries|theme|ui|defaults|testing"

Options:

  binaries                          Build sextant & sextand binaries
  theme                             Pulls @eotl/bootstrap-eotl from NPM
  ui                                Pulls @eotl/sextant from NPM
  data http://host.org/path/repo/   Clone and import Zones data
  defaults path/to/data/            Creates suggested default Groups and Zones
  testing                           Writes testing data (Contacts, etc...)
	EOF
  exit 1
elif [ "$1" = "binaries" ]; then
    build_binaries $2
elif [ "$1" = "theme" ]; then
    echo "Pull Theme into ${WWW_PATH}"
    echo "If present, pre-existing theme files will be overwritten."
	    printf "\n\n%s\n" "Press any key to continue"
    read -r _
    pull_bootstrap_theme
elif [ "$1" = "ui" ]; then
    pull_ui
elif [ "$1" = "data" ]; then
    if [ "$2" = "" ]
    then
        install_data "https://codeberg.org/eotl/core"
    else
        install_data "${2}"
    fi
elif [ "$1" = "defaults" ]; then
    create_defaults
elif [ "$1" = "testing" ]; then
    create_testing
else
    echo "Usage: ${0} binaries|theme|ui|data|defaults|testing"
    exit 1
fi

exit 0
