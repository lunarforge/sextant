package sextant

type GeojsonFeature struct {
	Type       string            `json:"type"`
	Geometry   GeojsonGeometry   `json:"geometry"`
	Properties map[string]string `json:"properties"`
}

type GeojsonGeometry struct {
	Type        string      `json:"type"`
	Coordinates interface{} `json:"coordinates"`
}

type GeojsonFeatureCollection struct {
	Type     string            `json:"type"`
	Features []*GeojsonFeature `json:"features"`
}

type GeojsonService interface {
	CreateFeature(data interface{}) *GeojsonFeature
	CreateFeatureCollection(data interface{}) *GeojsonFeatureCollection
}
