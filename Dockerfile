FROM golang:1.21-alpine AS build

COPY . /sextant

WORKDIR /sextant

ENV CGO_ENABLED=1

RUN apk add --no-cache \
  gcc \
  musl-dev

RUN ./install.sh binaries /sextant

FROM alpine:3.19.1

RUN adduser --system sextant

USER sextant

COPY --from=build /sextant/sextant /usr/bin/sextant
COPY --from=build /sextant/sextantd /usr/bin/sextantd

CMD ["/usr/bin/sextantd", "-v"]
