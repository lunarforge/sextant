const messages = {
    welcome: 'Willkommen zu Sextant',
    tour: {
        build: "Tour Builder", 
        buildMessage: "Fügen Sie eines oder alle der folgenden Elemente hinzu:",
        buildAdd1: "Gespeicherte Orte und Kontakte",
        buildAdd2: "Neue Adressen und Koordinaten",
        buildAdd3: "CSV von Adressen hochladen"
    }
}

export default messages;
