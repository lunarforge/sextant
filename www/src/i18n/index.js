import _ from 'lodash'
import Vue from 'vue'
import VueI18n from 'vue-i18n'

import enCore from '@eotl/core/locales/en'
import deCore from '@eotl/core/locales/de'
import enSextant from './locales/en.js'
import deSextant from './locales/de.js'

const en = _.merge(enCore, enSextant)
const de = _.merge(deCore, enSextant)

Vue.use(VueI18n)

const messages = {
  en,
  de,
}

const i18n = new VueI18n({
  locale: "en",
  messages,
  silentFallbackWarn: true
})

export default i18n;
