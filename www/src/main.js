import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './i18n'
import jQuery from 'jquery'
import bootstrap from 'bootstrap'

global.jQuery = jQuery
global .$ = jQuery

Vue.config.devtools = true
Vue.config.productionTip = false;

const mapGetters = Vuex.mapGetters
const mapActions = Vuex.mapActions

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
  data: {
      title: 'Sextant',
  },
  created() {
      console.log('created Sextant app')
  },
  computed: mapGetters([
      'evenOrOdd'
  ]),
  methods: mapActions([
      'increment',
      'decrement',
      'incrementIfOdd',
      'incrementAsync'
  ])
}).$mount('#sextant')
