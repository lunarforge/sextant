import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const DEFAULT_TITLE = 'Sextant'

const routes = [{
        path: '/',
        component: () => import('@/views/Mapper.vue'),
        meta: { title: 'Mapper' }
    },{
        path: '/places',
        component: () => import('@/views/Places.vue'),
        meta: { title: 'Places' }
    },{
        path: '/tours',
        component: () => import('@/views/Tours.vue'),
        meta: { title: 'Tours'}
    },{
        path: '/tour/:id',
        component: () => import('@/views/Tour.vue'),
        meta: { title: 'Tour'}
    },{
        path: '/zones',
        component: () => import('@/views/Zones.vue'),
        meta: { title: 'Zones'}
    }
]

const router = new VueRouter({
    // mode: 'history', need mod rewrite in server
    routes: routes
})

router.afterEach((to) => {
  Vue.nextTick(() => {
    document.title = to.meta.title + ' : ' + DEFAULT_TITLE || DEFAULT_TITLE
  })
})

export default router;
