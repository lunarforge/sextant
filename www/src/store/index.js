import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex)

const mutations = {
  increment (state) {
    state.count++
  },
  decrement (state) {
    state.count--
  },
  merge(state, newState) {
    for (let key in newState) {
        if (key in state) {
            state[key] = newState[key];
        }
    }
  }
}

const actions = {
  increment: ({ commit }) => commit('increment'),
  decrement: ({ commit }) => commit('decrement'),
  incrementIfOdd ({ commit, state }) {
    if ((state.count + 1) % 2 === 0) {
      commit('increment')
    }
  },
  incrementAsync ({ commit }) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        commit('increment')
        resolve()
      }, 1000)
    })
  },
  updateTours({ commit }, tours) {
    commit('merge', { tours })
  },
  alertMsg({ commit }, alert) {
    commit('merge', { alert })
  },
  async fetchGroups({ commit, state }) {
    console.log('Store fetch GROUPS')
    const response = await fetch(state.config.url_api + '/groups')
    if (!response.ok) {
      console.error('fetchGroups: could not fetch groups in store! status: ', response.status)
      return
    }
    const data = await response.json()
    commit('merge', data )
  }
}

// getters are functions.
const getters = {
  evenOrOdd: state => state.count % 2 === 0 ? 'even' : 'odd'
}

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default new Vuex.Store({
  state: {
    config: sextantConfig,
    riders: [],
    tours: [],
    groups: [],
    alert: {
        show: false,
        style: 'primary',
        title: 'Hello',
        message: 'This is just a default message, nothing to se here!'
    }
  },
  getters,
  actions,
  mutations
})

