const path = require('path'); 

module.exports = {
  lintOnSave: false,
  runtimeCompiler: true,
  filenameHashing: false,
  productionSourceMap: true,
  devServer: {
    public: '0.0.0.0',
    sockPath: '/sockjs-node'
  },
  configureWebpack: {
    resolve: {
       symlinks: false,
       alias: {
         'vue$': 'vue/dist/vue.common.js'
       }
    },
    optimization: {
      minimize: true,
      splitChunks: {
        chunks: 'all',
        minSize: 25000,
        maxSize: 0,
        minChunks: 1,
        maxAsyncRequests: 30,
        maxInitialRequests: 30,
        automaticNameDelimiter: '~',
        enforceSizeThreshold: 75000,
        cacheGroups: {
          default: false,
          bundle: {
            name: 'libs',
            chunks: 'all',
            minChunks: 1,
            reuseExistingChunk: false,
          },
          app: {
            chunks: 'all',
            name: 'sextant',
            minChunks: 1,
            priority: -20,
            reuseExistingChunk: true
          }
        }
      }
    }
  },
  chainWebpack: config => {
    config.plugins.delete('preload')
    config.plugins.delete('prefetch')

    config.plugin("copy").tap(args => {
      args[0].push({
        from: path.resolve(__dirname, "node_modules/leaflet/dist/leaflet.css"),
        to: "css",
        toType: "dir",
      },{
        from: path.resolve(__dirname, "node_modules/leaflet/dist/images"),
        to: "css/images",
        toType: "dir",
      },{
        from: path.resolve(__dirname, "node_modules/@eotl/theme-bootstrap/dist/css"),
        to: "css",
        toType: "dir",
        ignore: [".DS_Store"]
      },{
        from: path.resolve(__dirname, "node_modules/@eotl/theme-bootstrap/dist/fonts"),
        to: "fonts",
        toType: "dir"
      },{
        from: path.resolve(__dirname, "node_modules/@eotl/theme-bootstrap/dist/js"),
        to: "js",
        toType: "dir"
      },{
        from: path.resolve(__dirname, "node_modules/@eotl/icons/dist"),
        to: "images/icons",
        toType: "dir",
        ignore: [".gitignore"]
      });
      return args;
    });
  }
}
